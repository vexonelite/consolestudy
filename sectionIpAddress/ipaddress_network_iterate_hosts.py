
# Iterating over the network yields addresses,
# but not all of them are valid for hosts.
# For example, the base address of the network and
# the broadcast address are both included.
# To find the addresses that can be used by regular hosts on the network,
# use the ``hosts()`` method, which produces a generator.

# Comparing the output of this example with the previous example shows that
# the host addresses do not include the first values produced
# when iterating over the entire network.


import ipaddress
from collections import deque

NETWORKS = [
    '10.9.0.0/24',
    'fdfd:87b5:b475:5e3e::/64',
]

for n in NETWORKS:
    net = ipaddress.ip_network(n)
    print('{!r}'.format(net))
    for i, ip in zip(range(3), net.hosts()):
        print(ip)
    print()
    # type of net.hosts(): <class 'generator'>
    #print('type of net.hosts(): {}'.format(type(net.hosts())))
    # ipaddress = net.hosts()[net.hosts().length - 1]
    # print(ipaddress)

    # this only works out for the IPv4Network
    # dd = deque(net.hosts(), maxlen=1)
    # last_element = dd.pop()
    # print('last item: {}'.format(last_element))
