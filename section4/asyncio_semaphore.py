import asyncio
from random import randint
import socket
import sys

# https://stackoverflow.com/questions/48483348/how-to-limit-concurrency-with-python-asyncio

sem = asyncio.Semaphore(3)


async def download(code):
    wait_time = randint(2, 5)
    print('downloading {} will take {} second(s)'.format(code, wait_time))
    await asyncio.sleep(wait_time)  # I/O, context will switch to main function
    print('downloaded {}'.format(code))


async def safe_download(i):
    async with sem:  # semaphore limits num of simultaneous downloads
        return await download(i)


async def main():
    tasks = [
        asyncio.ensure_future(safe_download(i))  # creating task starts coroutine
        for i in range(15)
    ]
    await asyncio.gather(*tasks)  # await moment all downloads done


if __name__ ==  '__main__':
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(main())
    finally:
        loop.run_until_complete(loop.shutdown_asyncgens())
        loop.close()

##


class IeSocketConfig(object):

    def __init__(self, ip_address: str, port_number: int, tag: str):
        self.ip_address = ip_address
        self.port_number = port_number
        self.tag = tag
        # end of __init__

    def __str__(self):
        return 'IeSocketConfig[ ip_address: {}, port_number: {}, tag: {} ]'\
            .format(self.ip_address, self.port_number, self.tag)
        # end of __str__

    def to_server_address(self):
        return self.ip_address, self.port_number


async def socket_connect_and_send(socket_config: IeSocketConfig, ):

    # Create a TCP/IP socket
    the_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    server_address = socket_config.to_server_address()
    print('connecting to {} port {}'.format(*server_address))
    the_socket.connect(server_address)

    try:

        # Send data
        message = b'This is the message.  It will be repeated.'
        print('sending {!r}'.format(message))
        sock.sendall(message)

        # Look for the response
        amount_received = 0
        amount_expected = len(message)

        while amount_received < amount_expected:
            data = sock.recv(16)
            amount_received += len(data)
            print('received {!r}'.format(data))

    finally:
        print('closing socket')
        sock.close()
