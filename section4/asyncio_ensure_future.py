import asyncio
from asyncio import Future, Task

# # Creating Tasks from Coroutines

# The ``ensure_future()`` function returns a ``Task`` tied to the execution of a coroutine.
# That ``Task`` instance can then be passed to other code,
# which can wait for it without knowing how the original coroutine was constructed or called.

# Note that the coroutine given to ``ensure_future()`` is not started
# until something uses ``await`` to allow it to be executed!!.


async def wrapped():
    """
    a Coroutine wrapped in a task
    """
    print('wrapped Coroutine - sleep 10 seconds')
    await asyncio.sleep(10)
    return 'result'


async def inner(task: Future):
    print('inner: starting')
    print('inner: waiting for {!r}'.format(task))
    result_of_task = await task
    print('inner: task returned {!r}'.format(result_of_task))


async def starter():
    """
    a starter Coroutine that waits the result of task to be produced
    """
    print('starter: creating task')
    task = asyncio.ensure_future(wrapped())
    print('starter: waiting for inner')
    await inner(task)
    print('starter: inner returned')


event_loop = asyncio.get_event_loop()
try:
    print('entering event loop')
    result = event_loop.run_until_complete(starter())
finally:
    event_loop.close()
    print('closing event loop')
