import paho.mqtt.client as mqtt
import uuid
from abc import ABC  # ,ABCMeta
from datetime import datetime
import time
import traceback


# [Sending and Receiving Messages with MQTT](https://www.ev3dev.org/docs/tutorials/sending-and-receiving-messages-with-mqtt/)
class IeMqttConnectionDelegate(ABC):
    # abstract
    def on_connect(self, client, userdata, flags, rc):
        raise NotImplementedError("on_connect() must be overridden.")


class IeMqttDisconnectionDelegate(ABC):
    # abstract
    def on_disconnect(self, client, userdata, rc):
        raise NotImplementedError("on_disconnect() must be overridden.")


class IeMqttMessageDelegate(ABC):
    # abstract
    def on_message(self, client, userdata, msg):
        raise NotImplementedError("on_message() must be overridden.")


# https://docs.python.org/3/library/uuid.html
# https://www.programiz.com/python-programming/datetime/timestamp-datetime
class RandomStringViaUuidAndTime:
    @staticmethod
    def generate():
        now = datetime.now()
        timestamp: float = datetime.timestamp(now)
        timestamp_int: int = int(timestamp)
        print("timestamp: {}, type of timestamp: {}".format(timestamp, type(timestamp)))
        return uuid.uuid4().__str__() + "_" + timestamp_int.__str__()


class IeMqttConfig:

    def __init__(self,
                 identifier: str = RandomStringViaUuidAndTime().generate(),
                 host: str = "localhost",
                 port: int = 1883,
                 keepalive: int = 60,
                 clean_session: bool = True,
                 transport: str = "tcp",
                 protocol=mqtt.MQTTv311):
        self.identifier = identifier
        self.host = host
        self.port = port
        self.keepalive = keepalive
        self.clean_session = clean_session
        self.transport = transport
        self.protocol = protocol
        # end of __init__

    def __str__(self):
        return 'IeMqttConfig[ identifier: {}, host: {}, port: {} ]'.format(self.identifier, self.host, self.port)
        # end of __str__


class IeMqttRepository(IeMqttConnectionDelegate, IeMqttMessageDelegate, IeMqttDisconnectionDelegate):

    def __init__(self, config: IeMqttConfig):
        self.config = config
        self.client: mqtt = mqtt.Client(
            client_id=config.identifier,
            clean_session=config.clean_session,
            transport=config.transport,
            protocol=config.protocol)
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.on_disconnect = self.on_disconnect

    def start_loop(self):
        self.client.loop_start()

    def stop_loop(self):
        self.client.loop_forever()

    def loop_forever(self):
        self.client.loop_forever()

    def connect_to_broker(self):
        self.client.connect(host=self.config.host, port=self.config.port, keepalive=self.config.keepalive)

    def disconnect_from_broker(self):
        self.client.disconnect()

    def subscribe(self, topic_filer: str):
        self.client.subscribe(topic=topic_filer)

    def publish(self, topic: str, payload: str):
        self.client.publish(topic, payload=payload)

    # override IeMqttConnectionDelegate
    def on_connect(self, client: mqtt.Client, userdata: dict, flags, rc):
        print('on_connect')
        if client:
            print('type of client: {}'.format(type(client)))
        if userdata:
            print('type of userdata: {}'.format(type(userdata)))
        if flags:
            print('type of flags: {}'.format(type(flags)))
        if rc:
            print('type of rc: {}'.format(type(rc)))

    # override IeMqttMessageDelegate
    def on_message(self, client: mqtt.Client, userdata: dict, msg: mqtt.MQTTMessage):

        # if client:
        #     print('type of client: {}'.format(type(client)))
        # if userdata:
        #     print('type of userdata: {}'.format(type(userdata)))
        if msg:
            try:
                text_message = msg.payload.decode()
                print('on_message: {}'.format(text_message))
            except Exception as cause:
                print('error on msg.payload.decode()', traceback.print_tb(cause.__traceback__))
                # print('error on msg.payload.decode(): {}'.format(cause.__cause__))
        else:
            print('on_message - msg is None!')

    # override IeMqttDisconnectionDelegate
    def on_disconnect(self, client: mqtt.Client, userdata: dict, rc):
        print('on_disconnect')


if __name__ == '__main__':
    mqtt_config = IeMqttConfig()
    print(mqtt_config)
    repository = IeMqttRepository(mqtt_config)
    repository.connect_to_broker()
    repository.subscribe("test/topic")
    repository.start_loop()
    while True:
        time.sleep(15)
        repository.publish(topic="test/topic", payload=uuid.uuid4().__str__())

    # respository.client_loop_forever()
    # time.sleep(15)
    # respository.client_stop_loop()
    # respository.disconnect_from_broker()
