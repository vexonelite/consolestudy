import asyncio
from asyncio import Future
# A ``Future`` can also be used with the ``await`` keyword, as in this example.


def mark_done(future: Future, result: str):
    """
    Severed as a Callable in which we involve the ``set_result()`` of ``Future``
    to let the ``Future`` object to hold the ``result`` that can be retrieved
    in the near future.
    :param future:
    :param result:
    :return:
    """
    print('setting future result to {!r}'.format(result))
    future.set_result(result)


async def main(loop):
    all_done = asyncio.Future()

    print('scheduling mark_done')
    loop.call_soon(mark_done, all_done, 'the result')

    result2 = await all_done
    print('returned result: {!r}'.format(result2))


event_loop = asyncio.get_event_loop()
try:
    print('entering event loop')
    event_loop.run_until_complete(main(event_loop))
finally:
    event_loop.close()
    print('closing event loop')
