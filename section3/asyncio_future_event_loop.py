import asyncio
from asyncio import Future

# A ``Future`` represents the result of work that has not been completed yet.
# The event loop can watch for a ``Future`` object’s state to indicate that it is done,
# allowing one part of an application to wait for another part to finish some work.


# # Waiting for a Future
# A ``Future`` acts like a coroutine,
# so any techniques useful for waiting for a coroutine
# can also be used to wait for the future to be ``marked done``.
# This example passes the future to the event loop’s run_until_complete() method.

# The state of the ``Future`` changes to done when ``set_result()`` is called,
# and the ``Future`` instance retains the result given to the method for retrieval later.


def mark_done(future: Future, result: str):
    """
    Severed as a Callable in which we involve the ``set_result()`` of ``Future``
    to let the ``Future`` object to hold the ``result`` that can be retrieved
    in the near future.
    :param future:
    :param result:
    :return:
    """
    print('setting future result to {!r}'.format(result))
    future.set_result(result)


event_loop = asyncio.get_event_loop()
try:
    # A Future object
    all_done = asyncio.Future()

    print('scheduling mark_done')
    event_loop.call_soon(mark_done, all_done, 'the result')

    print('entering event loop')
    result2 = event_loop.run_until_complete(all_done)
    print('returned result: {!r}'.format(result2))
finally:
    event_loop.close()
    print('closing event loop')

print('future result: {!r}'.format(all_done.result()))
