import asyncio

# https://gist.github.com/justdoit0823/24d7a43a4fa519a8da6adac0a74e1684
def test_asyncio_coroutine(seconds):

    async def foo_coroutine(n):
        await asyncio.sleep(n)
        print('sleep', n, 'seconds')

    loop = asyncio.get_event_loop()
    print('start at', loop.time())
    loop.run_until_complete(foo_coroutine(seconds))
    print('finish at', loop.time())


if __name__ == '__main__':
    test_asyncio_coroutine(3)