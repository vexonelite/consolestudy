from abc import ABC
from time import sleep
from typing import Generic, List, Dict
from sectionJson.chain_operations import T, R, IeApiResponse, IeRunnableDelegate


class IeAbsFifoQueueTask(Generic[T, R], ABC):
    def __init__(self, task_list: List[T]):
        self.queued_task_list: List[T] = []
        self.result_map: Dict[T, IeApiResponse[R]] = {}
        self.queued_task_list.extend(task_list)

    """
    added in 2020/08/05
    """
    def on_next(self):
        """
        Does not override the on_next()
        """
        queued_task_list_length = len(self.queued_task_list)
        print('IeAbsFifoQueueTask - on_next - queued_task_list.size: {}, result_map.size: {}'.format(
            queued_task_list_length, len(self.result_map)))
        if queued_task_list_length > 0:
            next_task: T = self.queued_task_list.pop(0)
            if next_task:
                print('IeAbsFifoQueueTask - on_next -> next_task is valid -> execute_individual_task()')
                self.execute_individual_task(next_task)
            else:
                print('IeAbsFifoQueueTask - on_next -> next_task is None -> on_next()')
                self.on_next()

        else:
            print('IeAbsFifoQueueTask - on_next -> queued_task_list is empty -> on_complete()')
            self.on_complete()

    def execute_individual_task(self, next_task: T):
        """
        """
        raise NotImplementedError("execute_individual_task() must be overridden.")

    def on_complete(self):
        """
        """
        raise NotImplementedError("on_complete() must be overridden.")


class SimpleTestTaskModel:
    def __init__(self, name: str):
        self.name = name

    # override
    def __str__(self):
        name_str: str = ''
        if self.name:
            name_str += self.name
        return 'SimpleTestTaskModel { name: ' + name_str + ' }'


class SimpleFifoQueueTask(IeAbsFifoQueueTask):

    def __init__(self, task_list: List[SimpleTestTaskModel]):
        super().__init__(task_list)

    def execute_individual_task(self, next_task: SimpleTestTaskModel):
        print('SimpleFifoQueueTask - execute_individual_task')
        sleep(1.5)
        print('SimpleFifoQueueTask - execute_individual_task -> {} -> on_next()'.format(next_task))
        self.on_next()

    def on_complete(self):
        print('SimpleFifoQueueTask - on_complete')


class TestSimpleFifoQueueTask(IeRunnableDelegate):
    def run(self):
        task_list: List[SimpleTestTaskModel] = [
            SimpleTestTaskModel('foo'),
            SimpleTestTaskModel('qoo'),
            SimpleTestTaskModel('zoo'),
            SimpleTestTaskModel('woo'),
            SimpleTestTaskModel('goo'),
        ]
        SimpleFifoQueueTask(task_list).on_next()


if __name__ == '__main__':
    TestSimpleFifoQueueTask().run()
