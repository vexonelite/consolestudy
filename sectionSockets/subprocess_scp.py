# google: ``python secure copy with pem file``
# google: ``paramiko scp pem file``

# [How to copy a file to a remote server in Python using SCP or SSH?](https://stackoverflow.com/questions/68335/how-to-copy-a-file-to-a-remote-server-in-python-using-scp-or-ssh)
# [Best way to do file transfer via SCP using python and a .pem file [duplicate]](https://stackoverflow.com/questions/39573434/best-way-to-do-file-transfer-via-scp-using-python-and-a-pem-file)
# [How to call an external command?](https://stackoverflow.com/questions/89228/how-to-call-an-external-command)
# https://docs.python.org/3/library/subprocess.html
# https://pymotw.com/3/subprocess/index.html

# [Client] http://docs.paramiko.org/en/2.0/api/client.html
# [SFTP](http://docs.paramiko.org/en/2.0/api/sftp.html)
# [paramiko_example.py](https://gist.github.com/batok/2352501)


import multiprocessing
import subprocess
import traceback
from sectionJson.chain_operations import IeCallableDelegate, T
from subprocess import call
import threading
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor, as_completed
from sectionStructs.dongle_binary_file import (
    root_folder_path, command_scp, param_using_pem_file, pem_file, remote_bin_data_folder,
    remote_source_prefix, remote_source_suffix, all_bin_files, local_temp_folder
)


path_to_pem_file = root_folder_path + pem_file
dongle_ip_address = '192.168.86.225'


class ScpDongleBinFilesTask(IeCallableDelegate):

    def __init__(self, ip_address: str, dest_folder: str):
        self.command = command_scp + param_using_pem_file + path_to_pem_file + ' ' + remote_source_prefix + \
                   ip_address + remote_source_suffix + remote_bin_data_folder + all_bin_files + \
                   ' ' + root_folder_path + dest_folder

    # override
    def call(self) -> T:
        try:
            call(self.command.split())
            return True
        except Exception as cause:
            print('error on ScpDongleBinFilesTask#call()', traceback.print_tb(cause.__traceback__))
            return False


def test_get_bin_files_via_scp():
    # scp -i /home/elite_lin/Dongle_dapp/Dongle.pem  root@192.168.86.225:/bin/Data/*.bin /home/elite_lin/Dongle_dapp/temp_bins
    ScpDongleBinFilesTask(dongle_ip_address, local_temp_folder).call()


if __name__ == '__main__':
    test_get_bin_files_via_scp()


