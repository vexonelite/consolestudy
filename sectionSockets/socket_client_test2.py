import asyncio

from sectionJson.dongle_commands import IeDongleCommand, IeEmptyDongleCommandData
from sectionJson.exceptions import IeCustomException
from .socket_protocol_clients import (
    IeSocketOperationDelegate, ie_socket_protocol_client_factory
)
from sectionSockets.socket_client_definition import IeSocketConfig


class TestSocketCallback(IeSocketOperationDelegate):
    # override IeSocketReadDelegate
    def data_received(self, socket_config: IeSocketConfig, data: bytes):
        try:
            json_string = data.decode()
            print('data_received - {}, response: {}'.format(socket_config, json_string))
        except Exception as cause:
            print('error on paring data: {}'.format(cause))
        # end of data_received()

    # override IeSocketReadDelegate
    def on_error(self, socket_config: IeSocketConfig, cause: IeCustomException):
        print('on_error - {}, cause: {}'.format(socket_config, cause))
        # end of data_received()


async def main():
    client_completed = asyncio.Future()
    # the_socket_config = IeSocketConfig(ip_address='10.0.100.1', port_number=8888, tag='dongle_command_test')
    the_socket_config = IeSocketConfig(ip_address='192.168.86.225', port_number=8888, tag='dongle_command_test')
    socket_protocol_client = await ie_socket_protocol_client_factory(
        client_completed, the_socket_config)
    socket_protocol_client.set_socket_operation_callback(TestSocketCallback())
    print('get system info')
    system_info = IeDongleCommand(114, IeEmptyDongleCommandData())
    socket_protocol_client.send_command(system_info)

    try:
        await client_completed
    finally:
        socket_protocol_client.disconnect_from_server()

try:
    asyncio.run(main())
except KeyboardInterrupt:
    pass
except Exception as error:
    print('error on socket client test: {}'.format(error))