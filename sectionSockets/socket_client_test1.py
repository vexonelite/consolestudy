import asyncio

from sectionJson.dongle_commands import IeDongleCommand, IeEmptyDongleCommandData
from sectionJson.exceptions import IeCustomException
from .socket_protocol_clients import (
    IeSocketOperationDelegate, IeSocketProtocolClient,
    IeSocketProtocolClientCreationDelegate, create_socket_protocol_client
)
from sectionSockets.socket_client_definition import IeSocketConfig


class TestSocketCallback(IeSocketOperationDelegate, IeSocketProtocolClientCreationDelegate):

    def __init__(self):
        self.socket_protocol_client = None
        # end of __init__

    def on_created(self, socket_protocol_client: IeSocketProtocolClient):
        print('on_created')
        self.socket_protocol_client = socket_protocol_client
        self.socket_protocol_client.set_socket_operation_callback(self)
        print('get system info')
        system_info = IeDongleCommand(114, IeEmptyDongleCommandData())
        self.socket_protocol_client.send_command(system_info)
        # end of on_created()

    # override IeSocketReadDelegate
    def data_received(self, socket_config: IeSocketConfig, data: bytes):
        try:
            json_string = data.decode()
            print('data_received - {}, response: {}'.format(socket_config, json_string))
        except Exception as cause:
            print('error on paring data: {}'.format(cause))
        # end of data_received()

    # override IeSocketReadDelegate
    def on_error(self, socket_config: IeSocketConfig, cause: IeCustomException):
        print('on_error - {}, cause: {}'.format(socket_config, cause))
        # end of data_received()


test_socket_callback = TestSocketCallback()
# the_socket_config = IeSocketConfig(ip_address='10.0.100.1', port_number=8888, tag='dongle_command_test')
the_socket_config = IeSocketConfig(ip_address='192.168.86.225', port_number=8888, tag='dongle_command_test')
try:
    asyncio.run(
        create_socket_protocol_client(the_socket_config, test_socket_callback))
except KeyboardInterrupt:
    pass
except Exception as error:
    print('error on socket client test: {}'.format(error))
