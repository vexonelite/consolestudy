
# https://pymotw.com/3/socket/tcp.html#echo-client
# https://pymotw.com/3/selectors/index.html

import json
import time
import selectors
import socket
import traceback
from sectionJson.chain_operations import IeAsyncioTask, IeApiResult
from sectionJson.dongle_commands import IeDongleCommand, IeEmptyDongleCommandData, IeProgramInfoDongleCommandData
from sectionJson.exceptions import (
    IeCustomException, socket_error_connection, socket_error_selector_register_events,
    socket_error_read, socket_error_write
)
from typing import Optional
from sectionJson.dongle_models import IeSimpleGroupId


class IeSocketConfig(object):

    def __init__(self,
                 ip_address: str,
                 port_number: int,
                 tag: str,
                 connection_timeout: int = 4,
                 buffer_size: int = 1024):
        self.ip_address = ip_address
        self.port_number = port_number
        self.tag = tag
        self.connection_timeout = connection_timeout
        self.buffer_size = buffer_size
        # end of __init__

    def __str__(self):
        return 'IeSocketConfig[ ip_address: {}, port_number: {}, tag: {} ]'\
            .format(self.ip_address, self.port_number, self.tag)
        # end of __str__

    def to_remote_address(self, ) -> tuple:
        """
        :return: (ip address, port)
        """
        return self.ip_address, self.port_number


class IeBaseSocketClient:

    def __init__(self, socket_config: IeSocketConfig):
        self.socket_config = socket_config
        self.socket = None

    def create_new_socket(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print('IeBaseSocketClient#create_new_socket')

    async def connect_to_remote(self):
        try:
            await self.connect_on_demand()
            # Connecting is a blocking operation, so call ``setblocking()`` after it returns.
            print('IeBaseSocketClient#connect_to_remote - has connected to {}'.format(self.socket_config.ip_address))
            self.socket.setblocking(False)
        except Exception as cause:
            print('error on IeBaseSocketClient#connect_to_remote(): {}'.format(cause))
            raise IeCustomException(socket_error_connection, 'error on IeBaseSocketClient#connect_to_remote()', cause)

    async def connect_on_demand(self):
        raise NotImplementedError("connect_on_demand() must be overridden.")

    async def disconnect_from_remote(self):
        if self.socket:
            try:
                self.socket.close()
                self.socket = None
                print('IeBaseSocketClient#disconnect_from_remote() [socket]')
            except Exception as cause:
                print('error on IeBaseSocketClient#disconnect_from_remote() [socket]',
                      traceback.print_tb(cause.__traceback__))

    async def private_send_out_data(self, out_data: bytes):
        self.socket.sendall(out_data)
        print('IeBaseSocketClient#private_send_out_data - sending {!r} with length: {}'.format(out_data, len(out_data)))

    # async def read_incoming_data(self) -> Optional[bytes]:
        # try:
        #     read_byte_array = bytearray()
        #     keep_reading = True
        #     while keep_reading:
        #         bytes_received = 0
        #         read_data = await self.read_chunk_of_data(self.socket_config.buffer_size)
        #         if read_data:
        #             # A readable client socket has data
        #             # print('  received {!r}'.format(read_data))
        #             bytes_received += len(read_data)
        #             print('IeSocketClient#read_incoming_data - received {} bytes'.format(len(read_data)))
        #             print('IeSocketClient#read_incoming_data - received data: {!r}'.format(read_data))
        #             if bytes_received > 0:
        #                 read_byte_array += read_data
        #                 print('IeSocketClient#read_incoming_data - read_byte_array.length: {}'
        #                       .format(len(read_byte_array)))
        #                 keep_reading = bytes_received == self.socket_config.buffer_size
        #     return read_byte_array
        # except Exception as cause:
        #     print('error on IeSocketClient#read_incoming_data()')
        #     raise IeCustomException(socket_error_read, 'error on IeNioSocket#read_incoming_data()', cause)
    async def private_read_incoming_data(self, subject) -> bytes:
        read_byte_array = bytearray()
        keep_reading = True
        while keep_reading:
            bytes_received = 0
            read_data = subject.recv(self.socket_config.buffer_size)
            if read_data:
                bytes_received += len(read_data)
                print('IeSocketClient#private_read_incoming_data - received {} bytes'.format(len(read_data)))
                print('IeSocketClient#private_read_incoming_data - received data: {!r}'.format(read_data))
                if bytes_received > 0:
                    read_byte_array += read_data
                    print('IeSocketClient#private_read_incoming_data - read_byte_array.length: {}'
                          .format(len(read_byte_array)))
                    keep_reading = bytes_received == self.socket_config.buffer_size
        return read_byte_array

    async def send_out_data(self, out_data: bytes):
        raise NotImplementedError("send_out_data() must be overridden.")

    async def read_incoming_data(self) -> bytes:
        raise NotImplementedError("read_incoming_data() must be overridden.")


class IeSocketClient(IeBaseSocketClient):

    def __init__(self, socket_config: IeSocketConfig):
        super().__init__(socket_config)

    # override
    async def connect_on_demand(self):
        print('IeSocketClient#connect_on_demand {}'.format(self.socket_config.ip_address))
        self.socket.connect(self.socket_config.to_remote_address())

    # override
    async def send_out_data(self, out_data: bytes):
        try:
            return await self.private_send_out_data(out_data)
        except Exception as cause:
            print('error on IeSocketClient#send_out_data()')
            raise IeCustomException(socket_error_write, 'error on IeNioSocket#send_out_data()', cause)

    # override
    async def read_incoming_data(self) -> bytes:
        try:
            return await self.private_read_incoming_data(self.socket)
        except Exception as cause:
            print('error on IeSocketClient#read_incoming_data()')
            raise IeCustomException(socket_error_read, 'error on IeNioSocket#read_incoming_data()', cause)


class IeNioSocketClient(IeBaseSocketClient):

    def __init__(self, socket_config: IeSocketConfig):
        super().__init__(socket_config)
        self.selector = None

    def create_new_socket(self):
        super().create_new_socket()
        self.selector = selectors.DefaultSelector()

    # override
    async def connect_on_demand(self):
        print('IeNioSocketClient#connect_on_demand {}'.format(self.socket_config.ip_address))
        self.socket.connect_ex(self.socket_config.to_remote_address())

    async def disconnect_from_remote(self):
        await super().disconnect_from_remote()
        if self.selector:
            try:
                self.selector.close()
                self.selector = None
                print('IeNioSocketClient#disconnect_from_remote() [selector]')
            except Exception as cause:
                print('error on IeNioSocketClient#disconnect_from_remote() [selector]',
                      traceback.print_tb(cause.__traceback__))

    def selector_register_read_write_events(self):
        try:
            # Set up the selector to watch for when the socket is ready
            # to send data as well as when there is data to read.
            self.selector.register(
                self.socket,
                selectors.EVENT_READ | selectors.EVENT_WRITE,
                data=None
            )
        except Exception as cause:
            print('error on IeNioSocketClient#selector_register_read_write_events()')
            raise IeCustomException(socket_error_selector_register_events,
                                    'error on IeNioSocketClient#selector_register_read_write_events()', cause)

    async def get_selected_events(self) -> tuple:
        can_send = False
        read_connection = None
        try:
            for s_key, s_mask in self.selector.select(timeout=10):
                if s_mask & selectors.EVENT_WRITE:
                    print('IeNioSocketClient#get_selected_events - EVENT_WRITE')
                    can_send = True
                if s_mask & selectors.EVENT_READ:
                    print('IeNioSocketClient#get_selected_events - EVENT_READ')
                    read_connection = s_key.fileobj
        except Exception as cause:
            print('error on IeNioSocketClient#get_selected_events()', cause)
        return can_send, read_connection

    # async def send_out_data(self, out_data: bytes):
    #     for s_key, s_mask in self.selector.select(timeout=1):
    #         s_connection = s_key.fileobj
    #         s_client_address = s_connection.getpeername()
    #         print('IeNioSocket#send_out_data - client({})'.format(s_client_address))
    #
    #         if s_mask & selectors.EVENT_WRITE:
    #             bytes_sent = 0
    #             print('IeNioSocket#send_out_data - ready to write')
    #             print('IeNioSocket#send_out_data - sending {!r} with length: {}'
    #                   .format(out_data, len(out_data)))
    #             bytes_sent += len(out_data)
    #             self.socket.sendall(out_data)
    # override
    async def send_out_data(self, out_data: bytes):
        event_tuple = await self.get_selected_events()
        can_send = event_tuple[0]
        if can_send:
            try:
                return await self.private_send_out_data(out_data)
            except Exception as cause:
                print('error on IeNioSocketClient#send_out_data()')
                raise IeCustomException(socket_error_write, 'error on IeNioSocketClient#send_out_data()', cause)
        else:
            print('IeNioSocketClient#send_out_data() - can not send currently')

# async def read_incoming_data(self) -> Optional[bytes]:
    #     for s_key, s_mask in self.selector.select(timeout=1):
    #         s_connection = s_key.fileobj
    #         s_client_address = s_connection.getpeername()
    #         print('IeNioSocket#read_incoming_data - client({})'.format(s_client_address))
    #
    #         if s_mask & selectors.EVENT_READ:
    #             print('IeNioSocket#read_incoming_data - ready to read')
    #             read_byte_array = bytearray(0)
    #             keep_reading = True
    #             while keep_reading:
    #                 bytes_received = 0
    #                 read_data = s_connection.recv(1024)
    #                 if read_data:
    #                     # A readable client socket has data
    #                     # print('  received {!r}'.format(read_data))
    #                     bytes_received += len(read_data)
    #                     print('IeNioSocket#read_incoming_data - received {!r} with length: {}'
    #                           .format(read_data, len(read_data)))
    #                     read_byte_array.append(read_data)
    #                     print('IeNioSocket#read_incoming_data - read_byte_array.length: {}'
    #                           .format(len(read_byte_array)))
    #                 keep_reading = bytes_received > 0
    #             return read_byte_array
    #         else:
    #             return None

    # override
    async def read_incoming_data(self) -> Optional[bytes]:
        event_tuple = await self.get_selected_events()
        s_connection = event_tuple[1]
        if s_connection:
            try:
                return await self.private_read_incoming_data(s_connection)
            except Exception as cause:
                print('error on IeNioSocketClient#read_incoming_data()')
                raise IeCustomException(socket_error_read, 'error on IeNioSocketClient#read_incoming_data()', cause)
        else:
            print('IeNioSocketClient#read_incoming_data - s_connection is null!!')
            return None


class SimpleSocket1Task(IeAsyncioTask):
    async def execute_asyncio_task(self) -> str:
        socket_config = IeSocketConfig(ip_address='192.168.86.236', port_number=8888, tag='dongle_command_test')
        the_socket = IeSocketClient(socket_config)
        the_socket.create_new_socket()
        await the_socket.connect_to_remote()
        system_info = IeDongleCommand(114, IeEmptyDongleCommandData())
        json_str: str = system_info.to_json()
        print('json_str: {}'.format(json_str))
        json_bytes = json_str.encode()
        print('json_str to bytes: {}'.format(json_bytes))
        await the_socket.send_out_data(json_bytes)
        time.sleep(0.5)
        received_bytes = await the_socket.read_incoming_data()
        if received_bytes:
            json_str = received_bytes.decode()
        else:
            json_str = ''
        await the_socket.disconnect_from_remote()
        return json_str


class SimpleSocket2Task(IeAsyncioTask):
    async def execute_asyncio_task(self) -> str:
        # STA
        # socket_config = IeSocketConfig(ip_address='192.168.86.236', port_number=8888, tag='dongle_command_test')
        # AP
        socket_config = IeSocketConfig(ip_address='10.0.100.1', port_number=8888, tag='dongle_command_test')
        nio_socket = IeNioSocketClient(socket_config)
        nio_socket.create_new_socket()
        await nio_socket.connect_to_remote()
        nio_socket.selector_register_read_write_events()
        # nio_socket.selector_register_read_event()
        system_info = IeDongleCommand(114, IeEmptyDongleCommandData())
        json_str: str = system_info.to_json()
        print('json_str: {}'.format(json_str))
        json_bytes = json_str.encode()
        print('json_str to bytes: {}'.format(json_bytes))
        await nio_socket.send_out_data(json_bytes)
        time.sleep(0.5)
        received_bytes = await nio_socket.read_incoming_data()
        if received_bytes:
            json_str = received_bytes.decode()
        else:
            json_str = ''

        group_info = IeDongleCommand(102, IeSimpleGroupId(26))
        await nio_socket.send_out_data(group_info.to_json().encode())
        time.sleep(0.5)
        group_info_bytes = await nio_socket.read_incoming_data()
        if group_info_bytes:
            group_info_json_str = group_info_bytes.decode()
            print('group_info_json_str: {}'.format(group_info_json_str))

        program_info = IeDongleCommand(106, IeProgramInfoDongleCommandData(1, 1))
        await nio_socket.send_out_data(program_info.to_json().encode())
        time.sleep(0.5)
        program_info_bytes = await nio_socket.read_incoming_data()
        if program_info_bytes:
            program_info_json_str = program_info_bytes.decode()
            print('program_info_json_str: {}'.format(program_info_json_str))

        await nio_socket.disconnect_from_remote()

        return json_str


class SimpleSocketCallback(IeApiResult):
    def on_result_available(self, success: str, error: IeCustomException):
        if success:
            print('SimpleSocketCallback#success: {}'.format(success))
        elif error:
            print('SimpleSocketCallback#error!', traceback.print_tb(error.__traceback__))


if __name__ == '__main__':
    # SimpleSocket1Task().execute_task(SimpleSocketCallback())
    SimpleSocket2Task().execute_task(SimpleSocketCallback())
