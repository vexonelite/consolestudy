import asyncio
import logging
import sys

from abc import ABCMeta
from asyncio import Future, Transport

from sectionJson.dongle_commands import IeDongleCommand
from sectionJson.exceptions import IeCustomException, socket_error_common, socket_error_write, socket_error_disconnection
from sectionJson.json_utils import object_to_json
from sectionSockets.socket_client_definition import IeSocketConfig


logging.basicConfig(
    level=logging.DEBUG,
    format='%(name)s: %(message)s',
    stream=sys.stderr,
)
log = logging.getLogger('main')


class IeSocketOperationDelegate(metaclass=ABCMeta):
    def data_received(self, socket_config: IeSocketConfig, data: bytes):
        """
        return bytes
        """
        raise NotImplementedError("data_received() must be overridden.")

    def on_error(self, socket_config: IeSocketConfig, cause: IeCustomException):
        """
        return IeCustomException
        """
        raise NotImplementedError("on_error() must be overridden.")


class IeSocketProtocolClient(asyncio.Protocol):

    def __init__(self, future: Future, socket_config: IeSocketConfig):
        super().__init__()
        self.transport = None
        self.address = None
        self.log = logging.getLogger('IeSocketProtocolClient')
        self.future = future
        self.socket_config = socket_config
        self.socket_operation_callback = None
        # end of __init__

    def is_connected(self) -> bool:
        if self.transport is None:
            print('transport is null!')
            return False

        if self.transport.is_closing():
            print('transport is closed!')
            return False

        return True
        # end of is_connected()

    def disconnect_from_server(self):
        if not self.is_connected():
            return

        self.transport.close()
        print('involve transport.close()!')

        if self.future.done():
            print('future has been done!')
            return

        self.future.set_result(True)
        print('involve transport.close()!')
        # end of disconnect_to_server()

    def set_socket_operation_callback(self, socket_operation_callback: IeSocketOperationDelegate):
        self.socket_operation_callback = socket_operation_callback
        # end of set_socket_operation_callback()

    def send_command(self, command: IeDongleCommand):
        if not self.is_connected():
            return
        try:
            json_str: str = object_to_json(command)
            print('json_str: {}'.format(json_str))
            # json_str.encode() --> convert into bytes-like object
            self.transport.write(json_str.encode())
            self.log.debug('sending {!r}'.format(json_str.encode()))
            # sending eof will make server to close the socket connection
            #if self.transport.can_write_eof():
            #    self.transport.write_eof()

        except Exception as cause:
            print('error on send_command: {}'.format(cause))
            raise IeCustomException(socket_error_write, 'error on send_command', cause)
        # end of send_command()

    # override asyncio.BaseProtocol
    def connection_made(self, transport: Transport):
        self.transport = transport
        self.address = transport.get_extra_info('peername')
        self.log.debug(
            'connecting to {} port {}'.format(*self.address)
        )
        # end of connection_made()

    # override asyncio.BaseProtocol
    def connection_lost(self, exc):
        self.log.debug('server closed connection')

        if exc and self.socket_operation_callback \
                and isinstance(self.socket_operation_callback, IeSocketOperationDelegate):
            self.socket_operation_callback.on_error(
                self.socket_config,
                IeCustomException(socket_error_disconnection, 'server closed connection', exc))

        self.disconnect_from_server()
        # end of connection_lost()

    # override asyncio.Protocol
    def data_received(self, data: bytes):
        print('type of data {}'.format(type(data)))
        self.log.debug('received {!r}'.format(data))
        if self.socket_operation_callback and isinstance(self.socket_operation_callback, IeSocketOperationDelegate):
            self.socket_operation_callback.data_received(self.socket_config, data)
        # end of data_received()

    # when an end-of-file marker is received
    # the local transport object is closed and
    # the future object is marked as done by setting a result.
    #
    # override asyncio.Protocol
    def eof_received(self):
        self.log.debug('received EOF')
        # end of eof_received()


class IeSocketProtocolClientCreationDelegate(metaclass=ABCMeta):
    def on_created(self, socket_protocol_client: IeSocketProtocolClient):
        """
        return bytes
        """
        raise NotImplementedError("on_created() must be overridden.")


async def create_socket_protocol_client(
        socket_config: IeSocketConfig,
        created_callback: IeSocketProtocolClientCreationDelegate):
    # Get a reference to the event loop as we plan to use low-level APIs.
    event_loop = asyncio.get_running_loop()

    client_completed = asyncio.Future()
    transport, protocol = await event_loop.create_connection(
        lambda: IeSocketProtocolClient(client_completed, socket_config), socket_config.ip_address, socket_config.port_number)
    print('type of transport: {}'.format(type(transport)))
    print('type of protocol: {}'.format(type(protocol)))

    if created_callback:
        if isinstance(protocol, IeSocketProtocolClient):
            created_callback.on_created(protocol)
            try:
                await client_completed
            finally:
                transport.close()
        else:
            message = 'protocol object is not an instance of IeSocketProtocolClient: {}'.format(type(protocol))
            print(message)
            client_completed.set_result('haha')
    else:
        print('created_callback is null!!')
        client_completed.set_result('haha')


async def ie_socket_protocol_client_factory(
        client_completed: Future, socket_config: IeSocketConfig) -> IeSocketProtocolClient:
    # Get a reference to the event loop as we plan to use low-level APIs.
    event_loop = asyncio.get_running_loop()

    transport, protocol = await event_loop.create_connection(
        lambda: IeSocketProtocolClient(client_completed, socket_config),
        socket_config.ip_address, socket_config.port_number)
    print('type of transport: {}'.format(type(transport)))
    print('type of protocol: {}'.format(type(protocol)))

    if isinstance(protocol, IeSocketProtocolClient):
        return protocol
    else:
        message = 'protocol object is not an instance of IeSocketProtocolClient: {}'.format(type(protocol))
        raise IeCustomException(socket_error_common, message, None)
