
import time
from timeit import timeit
from threading import current_thread
import random
import string

# https://docs.python.org/3/library/concurrent.futures.html
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from urllib.request import urlopen
from sectionJson.chain_operations import IeConversionDelegate, T


# https://stackoverflow.com/questions/1966750/measure-time-of-a-function-with-arguments-in-python
def calculate_time_taken(fun, *args):
    """
    :param fun: a callable or a function
    :param args: the arguments subject to the function fun
    :return:
    """
    print('type of function: {}, args: {}'.format(type(fun), *args))
    start = time.time()
    ret = fun(*args)
    end = time.time()
    return ret, end-start


def download1(d_url: str, base: int):
    print('open connection for url: {} on thread: {}'.format(d_url, current_thread().getName()))
    start = time.time() - base
    try:
        resp = urlopen(d_url)
    except Exception as cause:
        print('ERROR: %s' % cause)
    stop = time.time() - base
    return start, stop


def download2(d_url: str):
    try:
        print('open connection for url: {} on thread: {}'.format(d_url, current_thread().getName()))
        resp = urlopen(d_url)
    except Exception as cause:
        print('ERROR: %s' % cause)


class LoadUrlCallable(IeConversionDelegate):
    def do_convert(self, input_data: str) -> tuple:
        print('open connection for url: {} on thread: {}'.format(input_data, current_thread().getName()))
        start = time.time()
        try:
            print('open connection for url: {} on thread: {}'.format(input_data, current_thread().getName()))
            resp = urlopen(input_data)
        except Exception as cause:
            print('ERROR: %s' % cause)
        stop = time.time()
        return start, stop


def io_heavy(text, base: int):
    start = time.time() - base
    f = open('output.txt', 'wt', encoding='utf-8')
    # text could be either str or bytes
    f.write(text)
    f.close()
    stop = time.time() - base
    return start, stop


def multithreading(func, args, workers):
    begin_time = time.time()
    with ThreadPoolExecutor(max_workers=workers) as executor:
        # type:  <class 'generator'>
        tmp_result = executor.map(func, args, [begin_time for i in range(len(args))])
        print('[ThreadPoolExecutor] type of tmp_result: {}, tmp_result: {}'.format(type(tmp_result), tmp_result))
        for item in tmp_result:
            print(item)
    return list(tmp_result)


def multiprocessing(func, args, workers):
    begin_time = time.time()
    with ProcessPoolExecutor(max_workers=workers) as executor:
        # type:  <class 'generator'>
        tmp_result = executor.map(func, args, [begin_time for i in range(len(args))])
        print('[ProcessPoolExecutor] type of tmp_result: {}, tmp_result: {}'.format(type(tmp_result), tmp_result))
    return list(tmp_result)


URL = 'http://scholar.princeton.edu/sites/default/files/oversize_pdf_test_0.pdf'
N = 3
urls = [URL for i in range(N)]

# Serial
# %timeit -n 1 [download(url, 1) for url in urls]
# taken_time = timeit('download(url, 1)', '[for url in urls]', number=1)

#
# for url in urls:
#     taken_time = calculate_time_taken(download2, url)
#     print(taken_time)

# std example:
# taken_time = timeit('"-".join(str(n) for n in range(100))', number=30)

# multithreading(download1, urls, 1)

# does not work out
load_url_callable = LoadUrlCallable
multithreading(load_url_callable.do_convert, urls, 1)

#
# Multithreading
#
# visualize_runtimes(multithreading(download, urls, 1), "Single Thread")
#
# visualize_runtimes(multithreading(download, urls, 2),MULTITHREADING_TITLE)
#
# visualize_runtimes(multiprocessing(download, urls, 1), "Single Process")
#
# visualize_runtimes(multiprocessing(download, urls, 2), MULTIPROCESSING_TITLE)
#
# N=12
# TEXT = ''.join(random.choice(string.ascii_lowercase) for i in range(10**7*5))


##
