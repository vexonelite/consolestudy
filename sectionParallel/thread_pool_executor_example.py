# https://docs.python.org/3/library/concurrent.futures.html

import threading
from concurrent.futures import ThreadPoolExecutor, as_completed
from urllib.request import urlopen


URLS = ['http://www.foxnews.com/',
        'http://www.cnn.com/',
        'http://europe.wsj.com/',
        'http://www.bbc.co.uk/',
        'http://some-made-up-domain.com/']


# Retrieve a single page and report the URL and contents
def load_url_callable(url, timeout):
    with urlopen(url, timeout=timeout) as conn:
        print('open connection for url: {} on thread: {}'.format(url, threading.current_thread().getName()))
        return conn.read()


# We can use a with statement to ensure threads are cleaned up promptly
with ThreadPoolExecutor(max_workers=5) as executor:
    # Start the load operations and mark each future with its URL
    future_to_url = {executor.submit(load_url_callable, url, 60): url for url in URLS}
    print('type of future_to_url: {}'.format(type(future_to_url)))
    for future in as_completed(future_to_url):
        print('type of future_item: {}'.format(type(future)))
        url = future_to_url[future]
        try:
            data = future.result()
        except Exception as exc:
            print('%r generated an exception: %s' % (url, exc))
        else:
            print('%r page is %d bytes' % (url, len(data)))
