# https://www.digitalocean.com/community/tutorials/how-to-use-threadpoolexecutor-in-python-3

import threading
from concurrent.futures import ThreadPoolExecutor, as_completed
import requests


wiki_page_urls = [
    "https://en.wikipedia.org/wiki/Ocean",
    "https://en.wikipedia.org/wiki/Island",
    "https://en.wikipedia.org/wiki/this_page_does_not_exist",
    "https://en.wikipedia.org/wiki/Shark",
]


def get_wiki_page_existence(wiki_page_url, timeout=10):
    print('open connection for url: {} on thread: {}'.format(wiki_page_url, threading.current_thread().getName()))
    response = requests.get(url=wiki_page_url, timeout=timeout)

    page_status = "unknown"
    if response.status_code == 200:
        page_status = "exists"
    elif response.status_code == 404:
        page_status = "does not exist"

    return wiki_page_url + " - " + page_status


# We can use a with statement to ensure threads are cleaned up promptly
with ThreadPoolExecutor(max_workers=5) as executor:
    futures = []
    for url in wiki_page_urls:
        future_object = executor.submit(get_wiki_page_existence, wiki_page_url=url, timeout=0.00001)
        futures.append(future_object)
        print('add future_object for url: {}'.format(url))
    for future_object in as_completed(futures):
        try:
            print('get result from future_object: {}'.format(future_object.result()))
        except requests.ConnectTimeout:
            print("ConnectTimeout.")
