# https://docs.python.org/3/library/concurrent.futures.html
# https://www.digitalocean.com/community/tutorials/how-to-use-threadpoolexecutor-in-python-3

import requests
import threading
from abc import ABC
from concurrent.futures import ThreadPoolExecutor, as_completed
from urllib.request import urlopen
from sectionJson.chain_operations import IeCallableDelegate, IeConversionDelegate, T

URLS = ['http://www.foxnews.com/',
        'http://www.cnn.com/',
        'http://europe.wsj.com/',
        'http://www.bbc.co.uk/',
        'http://some-made-up-domain.com/']


# Retrieve a single page and report the URL and contents
def load_url_callable(url, timeout):
    with urlopen(url, timeout=timeout) as conn:
        print('open connection for url: {} on thread: {}'.format(url, threading.current_thread().getName()))
        return conn.read()


class BaseLoadUrl(ABC):

    def __init__(self, given_url: str, timeout):
        self.url = given_url
        self.timeout = timeout

    def load_url(self) -> str:
        print('open connection for url: {} on thread: {}'.format(self.url, threading.current_thread().getName()))
        response = requests.get(url=self.url, timeout=self.timeout)

        page_status = "unknown"
        if response.status_code == 200:
            page_status = "exists"
        elif response.status_code == 404:
            page_status = "does not exist"

        return self.url + " - " + page_status


class LoadUrlCallable(BaseLoadUrl, IeCallableDelegate):

    def __init__(self, given_url: str, timeout):
        super().__init__(given_url, timeout)

    def call(self) -> str:
        return self.load_url()


class LoadUrlConverter(IeConversionDelegate):

    def do_convert(self, input_data: tuple) -> T:
        print('open connection for url: {} on thread: {}'.format(input_data[0], threading.current_thread().getName()))
        response = requests.get(url=input_data[0], timeout=input_data[1])

        page_status = "unknown"
        if response.status_code == 200:
            page_status = "exists"
        elif response.status_code == 404:
            page_status = "does not exist"

        return input_data[0] + " - " + page_status


def test1():
    converter = LoadUrlConverter()
    # We can use a with statement to ensure threads are cleaned up promptly
    with ThreadPoolExecutor(max_workers=5) as executor:
        futures = []
        for url in URLS:
            future_object = executor.submit(converter.do_convert, (url, 1.0))
            futures.append(future_object)
            print('add future_object for url: {}'.format(url))
        for future_object in as_completed(futures):
            try:
                print('get result from future_object: {}'.format(future_object.result()))
            except requests.ConnectTimeout:
                print("ConnectTimeout.")


def test2():
    # We can use a with statement to ensure threads are cleaned up promptly
    with ThreadPoolExecutor(max_workers=5) as executor:
        futures = []
        for url in URLS:
            url_callable = LoadUrlCallable(url, 1.0)
            # --> call() got an unexpected keyword argument 'args'
            # future_object = executor.submit(url_callable.call, args=None)
            future_object = executor.submit(url_callable.call)
            futures.append(future_object)
            print('add future_object for url: {}'.format(url))
        for future_object in as_completed(futures):
            try:
                print('get result from future_object: {}'.format(future_object.result()))
            except requests.ConnectTimeout:
                print("ConnectTimeout.")


if __name__ == '__main__':
    test1()
