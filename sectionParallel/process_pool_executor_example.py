# https://docs.python.org/3/library/concurrent.futures.html

import math
import multiprocessing
from concurrent.futures import ProcessPoolExecutor


PRIMES = [
    112272535095293,
    112582705942171,
    112272535095293,
    115280095190773,
    115797848077099,
    1099726899285419]


def is_prime(number: int):
    name = multiprocessing.current_process().name
    print('is_prime on process: {}'.format(name))

    if number < 2:
        return False
    if number == 2:
        return True
    if number % 2 == 0:
        return False

    sqrt_n = int(math.floor(math.sqrt(number)))
    for i in range(3, sqrt_n + 1, 2):
        if number % i == 0:
            return False
    return True


def main():
    with ProcessPoolExecutor() as executor:
        for number, prime in zip(PRIMES, executor.map(is_prime, PRIMES)):
            print('%d is prime: %s' % (number, prime))


if __name__ == '__main__':
    main()

