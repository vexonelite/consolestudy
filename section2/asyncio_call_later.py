import asyncio

# # Scheduling a Callback with a Delay

# To postpone a callback until some time in the future, use ``call_later()``.
# The first argument is the delay in seconds and the second argument is the callback.

# In this example, the same callback function is scheduled
# for several different times with different arguments.
# The final instance, using ``call_soon()``,
# results in the callback being invoked with the argument 3
# before any of the time-scheduled instances,
# showing that “soon” usually implies a minimal delay.

# I think the behavior is similar to Handler in the Android,
# where I can involve ``post()`` or ``postDelay()`` with
# a ``Runnable`` or ``Callable`` Object


def callback(n):
    print('callback {} invoked'.format(n))


async def main(loop):
    print('registering callbacks')
    loop.call_later(0.2, callback, 1)
    loop.call_later(0.1, callback, 2)
    loop.call_soon(callback, 3)

    await asyncio.sleep(0.4)


event_loop = asyncio.get_event_loop()
try:
    print('entering event loop')
    event_loop.run_until_complete(main(event_loop))
finally:
    event_loop.close()
    print('closing event loop')
