import math


class TouchHelper(object):
    @classmethod
    def touch_point_to_angle(cls, touch_x: float, touch_y: float, center_x: float, center_y: float) -> float:
        angle_in_radian: float = math.atan((touch_y - center_y) / (touch_x - center_x))
        angle_in_degree: float = math.degrees(angle_in_radian)
        return TouchHelper.angle_with_quadrant(angle_in_degree, touch_x, touch_y, center_x, center_y)

    @classmethod
    def angle_with_quadrant(
            cls, angle_in_degree: float, touch_x: float, touch_y: float, center_x: float, center_y: float) -> float:
        """
         *      90
         *      |
         *    2 |  1
         *      |
         *  0-------180
         *      |
         *    3 |   4
         *      |
         *     270
         *
         * angle to Quadrant
         *
        :param angle_in_degree:
        :param touch_x:
        :param touch_y:
        :param center_x:
        :param center_y:
        :return:
        """
        # 180
        if touch_x > center_x and touch_y == center_y:
            return float(180)
        # 90
        elif touch_x == center_x and touch_y < center_y:
            return float(90)
        # 0 / 360
        elif touch_x < center_x and touch_y == center_y:
            return float(0)
        # 270
        elif touch_x == center_x and touch_y > center_y:
            return float(270)
        # third
        elif touch_x < center_x and touch_y > center_y:
            return float(360) + angle_in_degree
        # first
        elif touch_x > center_x and touch_y < center_y:
            return float(180) + angle_in_degree
        # fourth
        elif touch_x > center_x and touch_y > center_y:
            return float(180) + angle_in_degree
        # second
        # elif (touch_x < center_x and touch_y < center_y):
        else:
            return angle_in_degree

    @classmethod
    def get_circle_coordinate(cls, angle_in_degree: float, radius: float, center_x: float, center_y: float) -> tuple:
        angle_in_radians: float = math.radians(angle_in_degree)
        circle_x: float = center_x + (radius * math.cos(angle_in_radians))
        circle_y: float = center_y - (radius * math.sin(angle_in_radians))
        return circle_x, circle_y

    @classmethod
    def to_android_angle(cls, angle_in_degree: float) -> int:
        angle_in_degree_int: int = round(angle_in_degree)
        t_angle: int = 180 - angle_in_degree_int
        if t_angle < 0:
            return 360 + t_angle
        else:
            return t_angle


if __name__ == '__main__':
    width = 828
    height = 828
    touch_point1 = (184.0, 145.0)
    touch_point2 = (579.0, 183.0)
    touch_point3 = (166.0, 664.0)
    touch_point4 = (538.0, 684.0)
    touch_points = [touch_point1, touch_point2, touch_point3, touch_point4]
    v_center_x = width / 2
    v_center_y = height / 2
    density: float = 3.0
    radius_in_dp = 107
    v_radius = density * radius_in_dp
    print('density: {}, radius: {}'.format(density, v_radius))

    rect = [126, 168, 954, 996]
    v_center_x2 = rect[0] + (width / 2)
    v_center_y2 = rect[1] + (height / 2)
    print('v_center_x2: {}, v_center_y2: {}'.format(v_center_x2, v_center_y2))

    for touch_point in touch_points:
        angle = TouchHelper.touch_point_to_angle(touch_point[0], touch_point[1], v_center_x, v_center_y)
        angle_int = round(angle)
        angle_x = TouchHelper.to_android_angle(angle_int)
        print('angle: {}, angle_x: {}'.format(angle, angle_x))
        circle_coordinate = TouchHelper.get_circle_coordinate(angle_x, v_radius, v_center_x2, v_center_y2)
        print('circle_coordinate_x: {}, circle_coordinate_y: {}'.format(circle_coordinate[0], circle_coordinate[1]))
