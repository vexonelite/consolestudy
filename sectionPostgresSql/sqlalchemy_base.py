from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# 'USER': 'project1user',
# 'PASSWORD': 'aA123456',
# 'Database': 'sql_alchemy_study',
# 'HOST': 'localhost',
# 'PORT': '5432'
SQL_Alchemy_Engine = create_engine('postgresql://project1user:aA123456@localhost:5432/sql_alchemy_study')

SQL_Alchemy_Session = sessionmaker(bind=SQL_Alchemy_Engine)

SQL_Alchemy_Base = declarative_base()
