# 1 - imports
from datetime import date

from sectionPostgresSql.sqlalchemy_base import SQL_Alchemy_Base, SQL_Alchemy_Engine, SQL_Alchemy_Session
from sectionPostgresSql.sqlalchemy_models import Movie, Actor, Stuntman, ContactDetails

# 2 - generate database schema
# Question - can we run the statement each time?
# after the testing, the SQLAlchemy will do it for us
# Raise anther question: how we can do when we define new tables and need to do migration?
SQL_Alchemy_Base.metadata.create_all(SQL_Alchemy_Engine)

# 3 - create a new session
sql_alchemy_session = SQL_Alchemy_Session()

# # 4 - create movies
# bourne_identity = Movie("The Bourne Identity", date(2002, 10, 11))
# furious_7 = Movie("Furious 7", date(2015, 4, 2))
# pain_and_gain = Movie("Pain & Gain", date(2013, 8, 23))
#
# # 5 - creates actors
# matt_damon = Actor("Matt Damon", date(1970, 10, 8))
# dwayne_johnson = Actor("Dwayne Johnson", date(1972, 5, 2))
# mark_wahlberg = Actor("Mark Wahlberg", date(1971, 6, 5))
#
# # 6 - add actors to movies (build the relation)
# bourne_identity.actors = [matt_damon]
# furious_7.actors = [dwayne_johnson]
# pain_and_gain.actors = [dwayne_johnson, mark_wahlberg]
#
# # 7 - add contact details to actors
# matt_contact = ContactDetails("415 555 2671", "Burbank, CA", matt_damon)
# dwayne_contact = ContactDetails("423 555 5623", "Glendale, CA", dwayne_johnson)
# dwayne_contact_2 = ContactDetails("421 444 2323", "West Hollywood, CA", dwayne_johnson)
# mark_contact = ContactDetails("421 333 9428", "Glendale, CA", mark_wahlberg)
#
# # 8 - create stuntmen
# matt_stuntman = Stuntman("John Doe", True, matt_damon)
# dwayne_stuntman = Stuntman("John Roe", True, dwayne_johnson)
# mark_stuntman = Stuntman("Richard Roe", True, mark_wahlberg)
#
# # 9 - persists data
# sql_alchemy_session.add(bourne_identity)
# sql_alchemy_session.add(furious_7)
# sql_alchemy_session.add(pain_and_gain)
#
# sql_alchemy_session.add(matt_contact)
# sql_alchemy_session.add(dwayne_contact)
# sql_alchemy_session.add(dwayne_contact_2)
# sql_alchemy_session.add(mark_contact)
#
# sql_alchemy_session.add(matt_stuntman)
# sql_alchemy_session.add(dwayne_stuntman)
# sql_alchemy_session.add(mark_stuntman)

# 10 - commit and close session
# sql_alchemy_session.commit()

#--------------------------------------------------------------------

# 3 - extract all movies
movies = sql_alchemy_session.query(Movie).all()
# 4 - print movies' details
print('\n### All movies:')
for movie in movies:
    print(f'{movie.title} was released on {movie.release_date}')
print('')

# 5 - get movies after 15-01-01
movies = sql_alchemy_session.query(Movie) \
    .filter(Movie.release_date > date(2015, 1, 1)) \
    .all()

print('### Recent movies:')
for movie in movies:
    print(f'{movie.title} was released after 2015')
print('')

# 6 - movies that Dwayne Johnson participated
the_rock_movies = sql_alchemy_session.query(Movie) \
    .join(Actor, Movie.actors) \
    .filter(Actor.name == 'Dwayne Johnson') \
    .all()

print('### Dwayne Johnson movies:')
for movie in the_rock_movies:
    print(f'The Rock starred in {movie.title}')
print('')

# 7 - get actors that have house in Glendale
glendale_stars = sql_alchemy_session.query(Actor) \
    .join(ContactDetails) \
    .filter(ContactDetails.address.ilike('%glendale%')) \
    .all()

print('### Actors that live in Glendale:')
for actor in glendale_stars:
    print(f'{actor.name} has a house in Glendale')
print('')

sql_alchemy_session.close()
