
import base64
import hashlib
import psycopg2
from psycopg2 import Error as psy_error
from psycopg2.extensions import connection as psy_connection, cursor as psy_cursor
import traceback
from typing import Any, List
from sectionJson.chain_operations import IeAsyncioTask, IeApiResult, IeApiResponse
from sectionJson.exceptions import (
    IeCustomException, database_error_common, database_error_connection, database_error_disconnection,
    database_error_no_connection, database_error_query, database_query_no_result, unknown_error
)
from sectionPostgresSql.sso_db_models import (
    IeDatabaseConfig, SsoAuthUser, SsoBackupBinFiles, SsoBackupBinFile,
    SsoGroupProgramInfo, SsoBackupBinFilesToGroupProgramInfo
)


sso_db_config = IeDatabaseConfig(
    user='project1user',
    password='aA123456',
    host='localhost',
    port='5432',
    database='gfi_sso_db1',
    tag='db_connection_test')


class IeDatabaseClient:

    def __init__(self, db_config: IeDatabaseConfig):
        self.db_config = db_config
        self.connection = None
        # self.cursor = None

    async def connect_to_database(self):
        try:
            # psycopg2.extensions.connection
            self.connection = psycopg2.connect(
                user=self.db_config.user,
                password=self.db_config.password,
                host=self.db_config.host,
                port=self.db_config.port,
                database=self.db_config.database)
            # print('IeDatabaseClient#connect_to_database - type of connection: {}'.format(type(self.connection)))
            # Print PostgreSQL Connection properties
            print('IeDatabaseClient#connect_to_database: {}\n'.format(self.connection.get_dsn_parameters()))
        except (Exception, psy_error) as cause:
            print('error on IeDatabaseClient#connect_to_remote(): {}'.format(cause))
            raise IeCustomException(database_error_connection, 'error on IeDatabaseClient#connect_to_database()', cause)

    async def close_connection(self):
        if self.connection is None:
            return
        try:
            self.connection.cursor().close()
            self.connection.close()
            self.connection = None
            print('IeDatabaseClient#close_connection - has disconnected from database!')
        except (Exception, psy_error) as cause:
            print('error on IeDatabaseClient#close_connection()', traceback.print_tb(cause.__traceback__))

    def get_db_connection(self) -> psy_connection:
        return self.connection

    def get_cursor_from_db_connection(self) -> psy_cursor:
        if self.connection is None:
            raise IeCustomException(database_error_no_connection,
                                    'IeDatabaseClient#get_cursor_from_db_connection(): No Db Connection!!',
                                    None)
        # psycopg2.extensions.cursor
        return self.connection.cursor()

    async def do_query(self) -> str:
        cursor = self.get_cursor_from_db_connection()
        # print('IeDatabaseClient#do_query - type of cursor: {}'.format(type(cursor)))

        # Print PostgreSQL version
        cursor.execute("SELECT version();")
        record = cursor.fetchone()
        record2 = cursor.fetchall()
        print('IeDatabaseClient#do_query - You are connected to: {}\n'.format(record))
        return record

###


class SimpleDbConnectionTask(IeAsyncioTask):
    async def execute_asyncio_task(self) -> str:
        database_client = IeDatabaseClient(sso_db_config)
        try:
            await database_client.connect_to_database()
            result = await database_client.do_query()
            return result
        except Exception as cause:
            if isinstance(cause, IeCustomException):
                raise cause
            else:
                raise IeCustomException(database_error_common, 'error on SimpleDbConnectionTask#execute_asyncio_task()', cause)
        finally:
            await database_client.close_connection()


class SimpleDbConnectionCallback(IeApiResult):
    def on_result_available(self, success: str, error: IeCustomException):
        if success:
            print('SimpleDbConnectionCallback#success: {}'.format(success))
        elif error:
            print('SimpleDbConnectionCallback#error!', traceback.print_tb(error.__traceback__))

###


class SsoUserQueryTask(IeAsyncioTask):
    # override
    async def execute_asyncio_task(self) -> IeApiResponse[List[SsoAuthUser]]:
        database_client = IeDatabaseClient(sso_db_config)
        try:
            await database_client.connect_to_database()
            cursor = database_client.get_cursor_from_db_connection()
            # result = await self.do_query(cursor)
            result = await self.do_query_with_email_and_social_type(cursor, 'supertesteru@gmail.com', 4)
            return result
        except Exception as cause:
            if isinstance(cause, IeCustomException):
                raise cause
            else:
                raise IeCustomException(database_error_common, 'error on SsoUserQueryTask#execute_asyncio_task()', cause)
        finally:
            await database_client.close_connection()

    async def do_query(self, cursor: psy_cursor) -> IeApiResponse[List[SsoAuthUser]]:
        query = 'select * from auth_user'
        sso_user_list: List[SsoAuthUser] = []
        try:
            cursor.execute(query)
            auth_users: List[Any] = cursor.fetchmany(5)
            # print('SsoUserQueryTask#do_query - type of public.auth_users: {}'.format(type(auth_users)))
            for row in auth_users:
                # print('SsoUserQueryTask#do_query - type of row: {}, row: {}'.format(type(row), row))
                # tuple:
                # id, password, last_login, is_superuser, social_id, social_type, username, email, picture_url, slot_capacity, slot_usage, date_joined, is_staff, is_active
                # (2,
                #  'pbkdf2_sha256$150000$HQ6ASpEgD5TJ$l21fcL0LQb8Xne+QmCdY5wAmk9W3AigtWbkJDlcYcGU=',
                #  None,
                #  False,
                #  'GYatWfH1iVaUgdTuVxm5TGQRNDm2',
                #  2,
                #  'tom wu',
                #  'tom_wu@goglobal.com.tw',
                #  'https://lh6.googleusercontent.com/-dFa4xPECUJ0/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rf95rv97XCoHVqZHOsC-nniLqPQrQ/s96-c/photo.jpg',
                #  10,
                #  0,
                #  datetime.datetime(2019, 5, 31, 15, 16, 27, 586078,
                #                    tzinfo=psycopg2.tz.FixedOffsetTimezone(offset=0, name=None)),
                #  False,
                #  True)
                # print("User Id: {}, type: {}".format(row[0], type(row[0])))
                # print("Social Id: {}, type: {}".format(row[4], type(row[4])))
                # print("Social Type: {}, type: {}".format(row[5], type(row[5])))
                # print("Name: {}, type: {}".format(row[6], type(row[6])))
                # print("Email: {},type: {}".format(row[7], type(row[7])))
                sso_user = SsoAuthUser(
                    user_id=row[0], user_name=row[6], social_id=row[4], social_type=row[5], email=row[7])
                sso_user_list.append(sso_user)
                # print(sso_user)
            return IeApiResponse(result=sso_user_list)
        except (Exception, psy_error) as cause:
            print("Error fetching data from PostgreSQL table", cause)
            error = IeCustomException(database_error_query, 'error on [select * from auth_user]', cause)
            return IeApiResponse(error=error)

    async def do_query_with_email_and_social_type(
            self, cursor: psy_cursor, email: str, social_type: int) -> IeApiResponse[List[SsoAuthUser]]:
        postgresql_select_query = "select auth_user.id, auth_user.social_id, auth_user.social_type, " \
                                  "auth_user.username, auth_user.email from auth_user " \
                                  "where email = '{}' AND social_type = '{}'".format(email, social_type)
        print('SsoUserQueryTask#postgresql_select_query: {}'.format(postgresql_select_query))
        sso_user_list: List[SsoAuthUser] = []
        try:
            cursor.execute(postgresql_select_query, (email, social_type, ))
            auth_users: List[Any] = cursor.fetchall()
            # print('SsoUserQueryTask#do_query - type of public.auth_users: {}'.format(type(auth_users)))
            for row in auth_users:
                sso_user = SsoAuthUser(
                    user_id=row[0], user_name=row[3], social_id=row[1], social_type=row[2], email=row[4])
                sso_user_list.append(sso_user)
                print(sso_user)
            return IeApiResponse(result=sso_user_list)
        except (Exception, psy_error) as cause:
            print("Error fetching data from PostgreSQL table", cause)
            error = IeCustomException(database_error_query, 'error on [{}]'.format(postgresql_select_query), cause)
            return IeApiResponse(error=error)


class SsoUserQueryCallback(IeApiResult):
    def on_result_available(self, success: IeApiResponse[List[SsoAuthUser]], error: IeCustomException):
        if success:
            if success.result:
                print('SsoUserQueryCallback#IeApiResponse.result.size: {}'.format(len(success.result)))
                for sso_user in success.result:
                    print('SsoUserQueryCallback: [{}]'.format(sso_user))
            elif success.error:
                print('SsoUserQueryCallback#IeApiResponse.error: {}'.format(success.error.message))
        elif error:
            print('SsoUserQueryCallback#error!', traceback.print_tb(error.__traceback__))

###


class SsoBackupBinFilesQueryTask(IeAsyncioTask):

    def __init__(self, backup_id: int):
        self.backup_id = backup_id

    # override
    async def execute_asyncio_task(self) -> IeApiResponse[SsoGroupProgramInfo]:
        database_client = IeDatabaseClient(sso_db_config)
        try:
            await database_client.connect_to_database()
            cursor = database_client.get_cursor_from_db_connection()
            ie_api_response: IeApiResponse[SsoBackupBinFiles] = await self.do_query_with_backup_id(cursor)
            if ie_api_response.result:
                group_program_into: SsoGroupProgramInfo = SsoBackupBinFilesToGroupProgramInfo()\
                    .do_convert(ie_api_response.result)
                return IeApiResponse(result=group_program_into)
            elif ie_api_response.error:
                return IeApiResponse(error=ie_api_response.error)
            else:
                error = IeCustomException(unknown_error, 'Unknown error on execute_asyncio_task', None)
                return IeApiResponse(error=error)
        except Exception as cause:
            if isinstance(cause, IeCustomException):
                raise cause
            else:
                raise IeCustomException(database_error_common, 'error on SsoUserQueryTask#execute_asyncio_task()', cause)
        finally:
            await database_client.close_connection()

    async def do_query_with_backup_id(self, cursor: psy_cursor) -> IeApiResponse[SsoBackupBinFiles]:
        postgresql_select_query = \
            "select app1_profilebackup.id as backup_id, app1_profilebackup.comment as backup_comment, " \
            "app1_profilebackup.version as backup_version, " \
            "app1_groupfile.id as group_id, app1_groupfile.file as group_file, app1_groupfile.checksum as group_checksum, " \
            "app1_programblue.id as blue_id, app1_programblue.file as blue_file, app1_programblue.checksum as blue_checksum, " \
            "app1_programsun.id as sun_id, app1_programsun.file as sun_file, app1_programsun.checksum as sun_checksum, " \
            "app1_programrefugium.id as refugium_id, app1_programrefugium.file as refugium_file, app1_programrefugium.checksum as refugium_checksum, " \
            "app1_programzeroten.id as zeroten_id, app1_programzeroten.file as zeroten_file, app1_programzeroten.checksum as zeroten_checksum " \
            "FROM app1_profilebackup " \
            "LEFT OUTER JOIN app1_groupfile ON app1_profilebackup.group_file_id = app1_groupfile.id " \
            "LEFT OUTER JOIN app1_programblue ON app1_profilebackup.program_blue_id = app1_programblue.id " \
            "LEFT OUTER JOIN app1_programsun ON app1_profilebackup.program_sun_id = app1_programsun.id " \
            "LEFT OUTER JOIN app1_programrefugium ON app1_profilebackup.program_refugium_id = app1_programrefugium.id " \
            "LEFT OUTER JOIN app1_programzeroten ON app1_profilebackup.program_zero_ten_id = app1_programzeroten.id " \
            "where app1_profilebackup.id = '{}'".format(self.backup_id)
        print('SsoUserQueryTask#postgresql_select_query: {}'.format(postgresql_select_query))
        try:
            cursor.execute(postgresql_select_query, (self.backup_id, ))
            query_result: Any = cursor.fetchone()
            if query_result is None:
                error = IeCustomException(
                    database_query_no_result, 'no result for query [{}]'.format(postgresql_select_query), None)
                return IeApiResponse(error=error)

            print('SsoBackupBinFilesQueryTask#do_query - type of query_result: {}'.format(type(query_result)))
            print('SsoBackupBinFilesQueryTask#do_query - query_result: {}'.format(query_result))
            print("Backup Id: {}, type: {}".format(query_result[0], type(query_result[0])))
            print("Backup comment: {}, type: {}".format(query_result[1], type(query_result[1])))
            print("Backup version: {}, type: {}".format(query_result[2], type(query_result[2])))

            group_file = None
            if query_result[3] and query_result[4] and query_result[5]:
                print("Group Id: {}, type: {}".format(query_result[3], type(query_result[3])))
                print("Group File: {}, type: {}".format(query_result[4], type(query_result[4])))
                # Group File: <memory at 0x7f7fe3710880>, type: <class 'memoryview'>
                group_byte_array: bytes = bytes(query_result[4])
                md5 = hashlib.md5(group_byte_array).hexdigest()
                print("Group File md5: {}".format(md5))
                print("Group checksum: {},type: {}".format(query_result[5], type(query_result[5])))
                group_file = SsoBackupBinFile(
                    identifier=query_result[3], file=query_result[4], checksum=query_result[5], description='group')

            blue_file = None
            if query_result[6] and query_result[7] and query_result[8]:
                blue_byte_array: bytes = bytes(query_result[7])
                print("blue_byte_array.size: {}".format(len(blue_byte_array)))
                blue_file = SsoBackupBinFile(
                    identifier=query_result[6], file=query_result[7], checksum=query_result[8], description='prog_blue')

            sun_file = None
            if query_result[9] and query_result[10] and query_result[11]:
                sun_byte_array: bytes = bytes(query_result[10])
                print("sun_byte_array.size: {}".format(len(sun_byte_array)))
                sun_file = SsoBackupBinFile(
                    identifier=query_result[9], file=query_result[10], checksum=query_result[11], description='prog_sun')

            refugium_file = None
            if query_result[12] and query_result[13] and query_result[14]:
                refugium_byte_array: bytes = bytes(query_result[13])
                print("refugium_byte_array.size: {}".format(len(refugium_byte_array)))
                refugium_file = SsoBackupBinFile(
                    identifier=query_result[12], file=query_result[13], checksum=query_result[14], description='prog_rf')

            zero_ten_file = None
            if query_result[15] and query_result[16] and query_result[17]:
                zero_ten_byte_array: bytes = bytes(query_result[16])
                print("zero_ten_byte_array.size: {}".format(len(zero_ten_byte_array)))
                zero_ten_file = SsoBackupBinFile(
                    identifier=query_result[15], file=query_result[16], checksum=query_result[17], description='prog_0-10v')

            backup_bin_files = SsoBackupBinFiles(
                identifier=query_result[0], comment=query_result[1], version=query_result[2],
                group=group_file, blue=blue_file, sun=sun_file, refugium=refugium_file, zero_ten=zero_ten_file)
            return IeApiResponse(result=backup_bin_files)
        except (Exception, psy_error) as cause:
            print("Error fetching data from PostgreSQL tables", cause)
            error = IeCustomException(database_error_query, 'error on [{}]'.format(postgresql_select_query), cause)
            return IeApiResponse(error=error)


class SsoBackupBinFilesQueryCallback(IeApiResult):
    def on_result_available(self, success: IeApiResponse[SsoGroupProgramInfo], error: IeCustomException):
        if success:
            if success.result:
                print('SsoBackupBinFilesQueryCallback#IeApiResponse.result:: {}'.format(success.result))
            elif success.error:
                print('SsoBackupBinFilesQueryCallback#IeApiResponse.error: {}'.format(success.error.message))
        elif error:
            print('SsoBackupBinFilesQueryCallback#error!', traceback.print_tb(error.__traceback__))


if __name__ == '__main__':
    # SimpleDbConnectionTask().execute_task(SimpleDbConnectionCallback())
    # SsoUserQueryTask().execute_task(SsoUserQueryCallback())
    # version 3
    SsoBackupBinFilesQueryTask(586).execute_task(SsoBackupBinFilesQueryCallback())
    # version 2
    # SsoBackupBinFilesQueryTask(98).execute_task(SsoBackupBinFilesQueryCallback())
    # version 1
    # SsoBackupBinFilesQueryTask(120).execute_task(SsoBackupBinFilesQueryCallback())

