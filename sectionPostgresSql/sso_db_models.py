
from typing import List
from sectionJson.exceptions import IeCustomException, internal_conversion_error, illegal_argument_error
from sectionJson.chain_operations import IeAbsConverter
from sectionStructs.dongle_binary_file import DongleGroup, DongleProgram, BinBytesToGroupList, BinBytesToProgramList


class IeDatabaseConfig(object):

    def __init__(self, user: str, password: str, host: str, port: str, database: str, tag: str):
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self.database = database
        self.tag = tag
        # end of __init__

    def __str__(self):
        return 'IeDatabaseConfig[ user: {}, password: {}, host: {}, port: {}, database: {}, tag: {} ]'\
            .format(self.user, self.password, self.host, self.port, self.database, self.tag)
        # end of __str__


class SsoAuthUser(object):

    def __init__(self, user_id: int, user_name: str, social_id: str, social_type: int, email: str):
        self.user_id = user_id
        self.user_name = user_name
        self.social_id = social_id
        self.social_type = social_type
        self.email = email

    # override
    def __str__(self):
        return 'SsoAuthUser { user_id: ' + str(self.user_id) + ', user_name: ' + self.user_name + ', social_id: ' + \
               self.social_id + ', social_type: ' + str(self.social_type) + ', email: ' + self.email + ' }'


class SsoBackupBinFile(object):
    def __init__(self, identifier: int, file, checksum: str, description: str):
        self.identifier = identifier
        self.file = file
        self.checksum = checksum
        self.description = description

    # override
    def __str__(self):
        return self.description + ' File { id: ' + str(self.identifier) + ', checksum: ' + self.checksum + ' }'


class SsoBackupBinFiles(object):

    def __init__(self,
                 identifier: int,
                 comment: str,
                 version: int,
                 group: SsoBackupBinFile = None,
                 blue: SsoBackupBinFile = None,
                 sun: SsoBackupBinFile = None,
                 refugium: SsoBackupBinFile = None,
                 zero_ten: SsoBackupBinFile = None):
        self.identifier = identifier
        self.comment = comment
        self.version = version
        self.group = group
        self.blue = blue
        self.sun = sun
        self.refugium = refugium
        self.zero_ten = zero_ten

    # override
    def __str__(self):
        group_str = 'None'
        if self.group:
            group_str = self.group.__str__()

        blue_str = 'None'
        if self.blue:
            blue_str = self.blue.__str__()

        sun_str = 'None'
        if self.sun:
            sun_str = self.sun.__str__()

        refugium_str = 'None'
        if self.refugium:
            refugium_str = self.refugium.__str__()

        zero_ten_str = 'None'
        if self.zero_ten:
            zero_ten_str = self.zero_ten.__str__()

        return 'SsoBackupBinFiles { identifier: ' + str(self.identifier) + ', comment: ' + self.comment + \
               ', version: ' + str(self.version) + ', [' + group_str + '], [' + blue_str + '], [' + sun_str + '], [' + \
               refugium_str + '], [' + zero_ten_str + ']}'


class SsoGroupProgramInfo(object):

    def __init__(self,
                 identifier: int,
                 comment: str,
                 version: int,
                 groups: List[DongleGroup] = None,
                 blues: List[DongleProgram] = None,
                 suns: List[DongleProgram] = None,
                 refugiums: List[DongleProgram] = None,
                 zero_tens: List[DongleProgram] = None):
        self.identifier = identifier
        self.comment = comment
        self.version = version
        self.groups = groups
        self.blues = blues
        self.suns = suns
        self.refugiums = refugiums
        self.zero_tens = zero_tens

    # override
    def __str__(self):
        group_str = '0'
        if self.groups:
            group_str = str(len(self.groups))

        blue_str = ''
        if self.blues:
            blue_str = str(len(self.blues))

        sun_str = ''
        if self.suns:
            sun_str = str(len(self.suns))

        refugium_str = ''
        if self.refugiums:
            refugium_str = str(len(self.refugiums))

        zero_ten_str = ''
        if self.zero_tens:
            zero_ten_str = str(len(self.zero_tens))

        return 'SsoGroupProgramInfo { identifier: ' + str(self.identifier) + ', comment: ' + self.comment + \
               ', version: ' + str(self.version) + ', [' + group_str + '], [' + blue_str + '], [' + \
               sun_str + '], [' +  refugium_str + '], [' + zero_ten_str + ']}'


class SsoBackupBinFilesToGroupProgramInfo(IeAbsConverter):
    def execute_conversion(self, item: SsoBackupBinFiles) -> SsoGroupProgramInfo:
        try:
            program_converter = BinBytesToProgramList()
            group_converter = BinBytesToGroupList()

            if item.group:
                the_group_list: List[DongleGroup] = group_converter.do_convert(item.group.file)
                print("the_group_list.size: {}".format(len(the_group_list)))
                for group_item in the_group_list:
                    print('[group_item to json]: {}'.format(group_item.to_json()))
            else:
                the_group_list: List[DongleGroup] = []

            if item.blue:
                program_blue_list: List[DongleProgram] = program_converter.do_convert(item.blue.file)
                print("program_blue_list.size: {}".format(len(program_blue_list)))
                for program_blue_item in program_blue_list:
                    print('[program_blue_item to json]: {}'.format(program_blue_item.to_json()))
            else:
                program_blue_list: List[DongleProgram] = []

            if item.sun:
                program_sun_list: List[DongleProgram] = program_converter.do_convert(item.sun.file)
                print("program_sun_list.size: {}".format(len(program_sun_list)))
                for program_sun_item in program_sun_list:
                    print('[program_sun_item to json]: {}'.format(program_sun_item.to_json()))
            else:
                program_sun_list: List[DongleProgram] = []

            if item.refugium:
                program_refugium_list: List[DongleProgram] = program_converter.do_convert(item.refugium.file)
                print("program_refugium_list.size: {}".format(len(program_refugium_list)))
                for program_refugium_item in program_refugium_list:
                    print('[program_refugium_item to json]: {}'.format(program_refugium_item.to_json()))
            else:
                program_refugium_list: List[DongleProgram] = []

            if item.zero_ten:
                program_zero_ten_list: List[DongleProgram] = program_converter.do_convert(item.zero_ten.file)
                print("program_zero_ten_list.size: {}".format(len(program_zero_ten_list)))
                for program_zero_ten_item in program_zero_ten_list:
                    print('[program_zero_ten_item to json]: {}'.format(program_zero_ten_item.to_json()))
            else:
                program_zero_ten_list: List[DongleProgram] = []

            return SsoGroupProgramInfo(
                identifier=item.identifier,
                comment=item.comment,
                version=item.version,
                groups=the_group_list,
                blues=program_blue_list,
                suns=program_sun_list,
                refugiums=program_refugium_list,
                zero_tens=program_zero_ten_list)
        except Exception as cause:
            print('error on SsoBackupBinFilesToGroupProgramInfo#execute_conversion')
            raise IeCustomException(internal_conversion_error,
                                    'error on SsoBackupBinFilesToGroupProgramInfo#execute_conversion',
                                    cause)

