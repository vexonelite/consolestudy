import os
import pandas
from pandas import DataFrame

from sectionJson.chain_operations import IeAbsCallable, IeAbsConverter, IeAbsFilter, IeApiResponse
from sectionJson.exceptions import IeCustomException, internal_generation_error, internal_process_error


# get the current path
# [start] constants definition:
root_folder_path = '/home/elite_lin/Dongle_dapp/'
current_path: str = os.getcwd()


class TestCreateDataFrame1(IeAbsCallable):
    # override IeAbsCallable
    def execute_generation(self) -> DataFrame:
        try:
            return DataFrame(
                {
                    "Name": [
                        "Braund, Mr. Owen Harris",
                        "Allen, Mr. William Henry",
                        "Bonnell, Miss. Elizabeth",
                    ],
                    "Age": [22, 35, 58],
                    "Sex": ["male", "male", "female"],
                }
            )
        except Exception as cause:
            print('error on generating pandas DataFrame: {}'.format(cause))
            raise IeCustomException(internal_generation_error, 'error on generating pandas DataFrame', cause)


class DataFrameToCsvCallable(IeAbsConverter):

    def __init__(self, file_path: str):
        self.file_path = file_path

    # override IeAbsConverter
    def execute_conversion(self, data_frame: DataFrame) -> IeApiResponse[bool]:
        try:
            print('DataFrameToCsvCallable - data_frame: {}'.format(data_frame))
            date_format_string: str = pandas.to_datetime('today').strftime('%Y_%m_%d_%f')
            print('DataFrameToCsvCallable - date_format_string: {}'.format(date_format_string))
            output_file_path: str = self.file_path + 'Result_' + date_format_string + '.csv'
            data_frame.to_csv(path_or_buf=output_file_path, index=False)
            return IeApiResponse(result=True)
        except Exception as cause:
            print('error on pandas DataFrame#to_csv: {}'.format(cause))
            error = IeCustomException(internal_process_error, 'error on pandas DataFrame#to_csv', cause)
            return IeApiResponse(error=error)


if __name__ == '__main__':
    r_data_frame: DataFrame = TestCreateDataFrame1().call()
    r_response: IeApiResponse = DataFrameToCsvCallable(root_folder_path).do_convert(r_data_frame)
    if r_response.result:
        print('pandas DataFrame#to_csv: success')
    elif r_response.error:
        print('pandas DataFrame#to_csv: failure')
