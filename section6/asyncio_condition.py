import asyncio
from asyncio import Condition

# # Conditions
# A ``Condition`` works similarly to an ``Event`` except that
# rather than notifying all waiting coroutines
# the number of waiters awakened is controlled with an argument to ``notify()``.

# This example starts five consumers of the ``Condition``.
# Each uses the ``wait()`` method to wait for a notification that they can proceed.
# ``manipulate_condition()`` notifies one consumer,
# then two consumers, then all of the remaining consumers.


async def consumer(condition: Condition, n: int):
    """
    a Coroutine stands for consumer
    """
    async with condition:
        print('consumer {} is waiting'.format(n))
        await condition.wait()
        print('consumer {} triggered'.format(n))
    print('ending consumer {}'.format(n))


async def manipulate_condition(condition: Condition):
    """
    a Coroutine wrapped in a task
    """
    print('starting manipulate_condition')

    # pause to let consumers start
    await asyncio.sleep(1)

    for i in range(1, 3):
        async with condition:
            print('notifying {} consumers'.format(i))
            condition.notify(n=i)
        await asyncio.sleep(1)

    async with condition:
        print('notifying remaining consumers')
        condition.notify_all()

    print('ending manipulate_condition')


async def main(loop):
    # Create a condition
    condition = asyncio.Condition()

    # Set up tasks watching the condition
    consumers = [
        consumer(condition, i)
        for i in range(5)
    ]

    # Schedule a task to manipulate the condition variable
    loop.create_task(manipulate_condition(condition))

    # Wait for the consumers to be done
    await asyncio.wait(consumers)


event_loop = asyncio.get_event_loop()
try:
    print('entering event loop')
    result = event_loop.run_until_complete(main(event_loop))
finally:
    event_loop.close()
    print('closing event loop')
