import asyncio
from asyncio import Event
import functools

# # Events
# An ``asyncio.Event`` is based on ``threading.Event``,
# and is used to allow __multiple consumers__
# to wait for something to happen without looking for a specific value
# to be associated with the notification.

# As with the ``Lock``, both ``coro1()`` and ``coro2()``
# wait for the event to be set. __The difference is that
# both can start as soon as the event state changes,
# and they do not need to acquire a unique hold on the event object__.


def set_event(event: Event):
    print('setting event in callback')
    event.set()


async def coro1(event: Event):
    print('coroutine1 waiting for event')
    await event.wait()
    print('coroutine1 triggered')


async def coro2(event: Event):
    print('coroutine2 waiting for event')
    await event.wait()
    print('coroutine2 triggered')


async def main(loop):
    # Create a shared event
    event = asyncio.Event()
    print('event start state: {}'.format(event.is_set()))

    loop.call_later(1, functools.partial(set_event, event))

    await asyncio.wait([coro1(event), coro2(event)])
    print('event end state: {}'.format(event.is_set()))


event_loop = asyncio.get_event_loop()
try:
    print('entering event loop')
    event_loop.run_until_complete(main(event_loop))
finally:
    event_loop.close()
    print('closing event loop')
