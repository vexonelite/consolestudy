# https://www.bestprog.net/en/2020/05/03/python-module-struct-working-with-binary-files/

# Binary files. Module struct.
# Example of writing/reading a tuple of strings.

# 1. Include the struct module
import struct

# 2. The specified tuple of rows to be written to the file.
T = ('abc', 'abcd', 'def', 'ghi jkl')

# 3. Writing to a file tuple T
# 3.1. Open file for writing in binary mode
f = open('myfile10.bin', 'wb')

# 3.2. Write the number of elements in a tuple
count = len(T)
header = struct.pack('>i', count)  # get the packed data
print('header: ', header)
print('header.size: ', len(header))
f.write(header)  # write the packed data

# 3.3. Write each row of the tuple in a loop.
#     Since each line has a different length,
#     this length must also be written to the file.
for item in T:  # the loop bypassing of elements of the tuple
    # get the length of item
    length = len(item)

    # pack the length of item
    header = struct.pack('>i', length)
    print('[loop] length: ', length)
    print('[loop] header: ', header)
    print('[loop] header.size: ', len(header))
    # write to the file
    f.write(header)

    bt_item = item.encode()  # convert str => bytes
    print('[loop] bt_item: ', bt_item)
    # '>ns' - means char [n]
    format_string = '>' + str(length) + 's'
    content = struct.pack(format_string, bt_item)
    print('[loop] content: ', content)
    print('[loop] content.size: ', len(content))

    # write to the file
    f.write(content)

# 3.4. Close file
f.close()

# ------------------------------------------------------
# 4. Reading a recorded tuple from a file
# 4.1. Open file for reading in binary mode
f = open('myfile10.bin', 'rb')

# 4.2. Count the number of elements (lines) in a file
header = f.read(4) # Read the first 4 bytes - size of type int, d - packed data
count = struct.unpack('>i', header)[0]  # count - number of elements

# 4.3.Generate an empty tuple
T2 = ()

# 4.4. Lines reading cycle
i = 0
while i < count:
    # get the length of the line
    header = f.read(4)
    length = struct.unpack('>i', header)[0]

    # create a format string
    format_string = '>' + str(length) + 's'

    # read length bytes from file to object d
    content = f.read(length)

    # unpack the string according to the sf line
    bt_item = struct.unpack(format_string, content)[0]  # sb - string of type bytes

    # convert bytes=>str
    str_item = bt_item.decode()

    # Add string to the tuple
    T2 = T2 + (str_item,)

    i = i+1

# 4.5. Print the tuple T2
print("T2: ", T2)

# 4.6. Close file
f.close()
