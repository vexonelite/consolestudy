# https://www.bestprog.net/en/2020/05/03/python-module-struct-working-with-binary-files/

# Binary files. Writing/reading a list of different data.
# Using the capabilities of the struct module.

# 1. Include the struct module
import struct

# 2. The specified list of different types of data:
#   - 1.5 - type float, in struct denoted by 'f'
#   - True - type bool, in struct denoted by '?'
#   - 'abc def' - type char[], in struct denoted 's'
L = [1.5, True, 'abc def']

# 3. define the format string: byte order: big-endian, 1 float, 1 bool, and one char[] with size 7
#     Decoding the string '>f?7s':
#     - '>' - reverse byte order (high bytes follow last);
#     - 'f' - type float;
#     - '?' - type bool;
#     - '7s' - the type char[] of 7 character size.
format_string = '>f?7s'

# 4. To pack data, use the pack() method
d = struct.pack(format_string, L[0], L[1], L[2].encode())

# 5. Write list L to the file 'myfile4.bin'
# 5.1. Open file for writing
f = open('myfile4.bin', 'wb')

# 5.2. Write pack data d in file
f.write(d)

# 5.3. Close file
f.close()

# 6. Read the list from the binary file 'myfile4.bin'
# 6.1. Open file for reading
f = open('myfile4.bin', 'rb')
print('type of f: {}'.format(type(f)))

# 6.2. Read data from file
d = f.read()

# 6.3. Close the file
f.close()

# 6.4. Unpack data using the unpack() method.
#      Data is unpacked as a tuple.
T = struct.unpack(format_string, d)

# 6.5. Convert tuple T to list L2
L2 = list(T)

# 6.6. Convert string L2[2] to str type
L2[2] = L2[2].decode()

# 6.7. Print the list
# L2 =   [1.5, True, 'abc def']
print("L2 = ", L2)
