##  https://www.bestprog.net/en/2020/05/08/python-module-struct-packing-unpacking-data-basic-methods/

# Module struct. Methods pack(), unpack()
# Pack/unpack list of numbers

# 1. Include module struct
import struct

# 2. Specified list of numbers
number_list = [2.88, 3.9, -10.5]

# 3. define the format string: byte order: big-endian, 3 float numbers
format_string = '>3f'

# 4. Pack list of numbers. Method pack()
pack_obj = struct.pack(format_string, number_list[0], number_list[1], number_list[2])
# pack_obj = struct.pack(format_string, 2.88, 3.9, -10.5)

# 5. Display the object pack_obj
# type: bytes
print('type_of_pack_obj: {}'.format(type(pack_obj)))
# b'@8Q\xec@y\x99\x9a\xc1(\x00\x00'
print('pack_obj: ', pack_obj)
pack_obj_size = struct.calcsize('>3f')
# pack_obj_size: 12
print('pack_obj_size: ', pack_obj_size)

# 6. Unpack list of numbers. Method unpack().
# The result is a tuple unpacked_obj
unpacked_obj = struct.unpack(format_string, pack_obj)

# 7. Print the unpacked object unpacked_obj
# type: tuple
print('type_of_pack_obj: {}'.format(type(unpacked_obj)))
# (2.880000114440918, 3.9000000953674316, -10.5)
print('unpacked_obj: ', unpacked_obj)

# 8. Convert tuple unpacked_obj to list number_list2
number_list2 = list(unpacked_obj)

# 9. Display the list LS2
# [2.880000114440918, 3.9000000953674316, -10.5]
print('number_list2: ', number_list2)

print('=========================================================')

# 3. Determine the size of a packed tuple of strings
# 3.1. The specified tuple of two strings
string_tuple = ('Hello', 'abcd')

# 3.2. define the format string: byte order: little-endian, two char[] their sizes are 5 and 4, respectively
format_string2 = '<5s4s'

# 3.3. Pack the string_tuple
pack_obj2 = struct.pack(format_string2, string_tuple[0].encode(), string_tuple[1].encode())

# 3.3. Display the packed object
# b'Helloabcd'
print('pack_obj2: ', pack_obj2)

# 3.4. Display the size of packed tuple
format_string2_size = struct.calcsize(format_string2)
# format_string2: 9
print('format_string2_size: ', format_string2_size)
