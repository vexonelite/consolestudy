# https://www.bestprog.net/en/2020/05/03/python-module-struct-working-with-binary-files/

# Binary files. Module struct.
# Example of writing/reading dictionary

# 1. Include the struct module
import struct

# 2. Specified dictionary
D = {1: 'Sunday', 2: 'Monday', 3: 'Tuesday', 4: 'Wednesday',
     5: 'Thursday', 6: 'Friday', 7: 'Saturday'}

# 3. Write tuple T to the file
# 3.1. Open file for writing in binary mode
f = open('myfile11.bin', 'wb')

# 3.2. Write the number of items in the dictionary
count = len(D)
header = struct.pack('>i', count)  # get packed data
print('header: ', header)
print('header.size: ', len(header))
f.write(header)  # write packed data

# 3.3. Write each line of the dictionary in a loop.
#     Since each line can have a different length,
#     this length must also be written to a file.
for key in D:  # dictionary traversal cycle
    # write key - int number
    key_bytes = struct.pack('>i', key)
    print('key_bytes: ', key_bytes)
    print('key_bytes.size: ', len(key_bytes))
    f.write(key_bytes)

    # write line by key
    # get the length of line item
    value_length = len(D[key])
    print('value_length: ', value_length)

    # pack the length of the string
    value_length_bytes = struct.pack('>i', value_length)
    print('value_length_bytes: ', value_length_bytes)
    print('key_bytes.value_length_bytes: ', len(value_length_bytes))

    # write length to the file
    f.write(value_length_bytes)

    # pack string D[key]: '>ns' - means char[n]
    value_bytes = D[key].encode()  # convert str=>bytes
    print('value_bytes: ', value_bytes)
    print('key_bytes.value_bytes: ', len(value_bytes))
    format_string = '>' + str(value_length) + 's'
    packed_value_bytes = struct.pack(format_string, value_bytes)
    print('packed_value_bytes: ', packed_value_bytes)
    print('key_bytes.packed_value_bytes: ', len(packed_value_bytes))
    print('=========================================================')
    # write to file
    f.write(packed_value_bytes)

# 3.4. Close file
f.close()

# ------------------------------------------------------
# 4. Reading a recorded tuple from a file
# 4.1. Open file for reading in binary mode
f = open('myfile11.bin', 'rb')

# 4.2. Count the number of elements (lines) in a file
header = f.read(4)  # Read the first 4 bytes - size of type int, d - packed data
print('2- header: ', header)
print('2- header.size: ', len(header))
count = struct.unpack('>i', header)[0]  # count - number of items in the dictionary

# 4.3.Form an empty dictionary
D2 = dict()

# 4.4. Lines reading loop
i = 0
while i < count:
    # 4.4.1. Read key - an integer of 4 bytes
    key_bytes = f.read(4)
    print('2- key_bytes: ', key_bytes)
    print('2- key_bytes.size: ', len(key_bytes))

    key = struct.unpack('>i', key_bytes)[0]  # unpack data
    print('2- key: ', key)

    # 4.4.2. Read the number of characters per line
    value_length_bytes = f.read(4)  # 4 - the number of type int
    print('2- value_length_bytes: ', value_length_bytes)
    print('2- value_length_bytes.size: ', len(value_length_bytes))

    value_length = struct.unpack('>i', value_length_bytes)[0]
    print('2- value_length: ', value_length)

    # 4.4.3. Read the line, first you need to form
    #        the sf format line
    format_string = '>' + str(value_length) + 's'
    value_bytes = f.read(value_length)  # read length bytes from file
    print('2- value_bytes: ', value_bytes)
    print('2- value_bytes.size: ', len(value_bytes))

    unpacked_value_bytes = struct.unpack(format_string, value_bytes)[0]  # unpack string
    print('2- unpacked_value_bytes: ', unpacked_value_bytes)
    print('2- unpacked_value_bytes: ', len(unpacked_value_bytes))

    value = unpacked_value_bytes.decode()
    print('2- value: ', value)

    # 4.4.4. Add pair key:value to the dictionary D2
    D2[key] = value

    i = i+1

# 4.5. Display the dictionary D2
print("D2 = ", D2)

# 4.6. Close file
f.close()
