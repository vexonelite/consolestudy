# https://www.bestprog.net/en/2020/05/03/python-module-struct-working-with-binary-files/

# Binary files. Module struct.
# Example of writing/reading a list of integers

# 1. Include the struct module
import struct

# 2. Specified list
L = [2, 4, 6, 8, 10]

# 3. Write list to the file

# 3.1. Open file for writing
f = open('myfile9.bin', 'wb')

# 3.2. To pack data, use the pack() method
header = struct.pack('>i', len(L))
# type: bytes
print('type of header: {}'.format(type(header)))
# b'\x00\x00\x00\x05'
print('header: ', header)

# 3.3. Write object d to the file
f.write(header)

# 3.4. Write elements one at a time - this is also possible
for item in L:
    content = struct.pack('>i', item)
    # type: bytes
    print('type of content: {}'.format(type(content)))
    # b'\x00\x00\x00\x02'
    # b'\x00\x00\x00\x04'
    # b'\x00\x00\x00\x06'
    # b'\x00\x00\x00\x08'
    # b'\x00\x00\x00\n'
    print('content: ', content)
    f.write(content)

# 3.5. Close file
f.close()

# ---------------------------------------------
# 4. Reading a list from a file
# 4.1. Open file for reading
f = open('myfile9.bin', 'rb')

# 4.2. Get the number of list items - the first number in the file,
#     first 4 bytes are read - int type size
header = f.read(4)
# type: bytes
print('2 - type of header: {}'.format(type(header)))
# b'\x00\x00\x00\x05'
print('2 - header: ', header)
print('2 - header.size: ', len(header))

# count = struct.unpack('>i', header)[0]
temp = struct.unpack('>i', header)
# type: tuple
print('type of temp: {}'.format(type(temp)))
print('temp: ', temp)
count = temp[0]

# 4.3. Read the entire list into a binary object d
# reading occurs from the current position to the end of the file
content = f.read()
# type: bytes
print('2 - type of content: {}'.format(type(content)))
# b'\x00\x00\x00\x02\x00\x00\x00\x04\x00\x00\x00\x06\x00\x00\x00\x08\x00\x00\x00\n'
print('2 - content: ', content)
print('2 - content.size: ', len(header))

# 4.4. Generate a format string to read all numbers at a time
#      (numbers can also be read one at a time in a loop)
s = '>' + str(count) + 'i'

# 4.5. Get a tuple of numbers based on a format string
T = struct.unpack(s, content)

# 4.6. Convert tuple to a list
L2 = list(T)

# 4.7. Print the list
print("L2 = ", L2)

# 4.8. Close file
f.close()
