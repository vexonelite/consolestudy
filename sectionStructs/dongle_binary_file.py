
import json
import re
import struct
import traceback

from collections import namedtuple
from io import BufferedReader
from typing import List
from sectionJson.json_utils import IeObjectToJsonMixin
from sectionJson.exceptions import IeCustomException, internal_conversion_error, illegal_argument_error
from sectionJson.chain_operations import IeAbsConverter


# [start] constants definition:
root_folder_path = '/home/elite_lin/Dongle_dapp/'
local_temp_folder = 'temp_bins/'

folder_LS4SGS0004 = 'Dongle_LS4SGS0004/'


pem_file = 'Dongle.pem'
remote_bin_data_folder = 'bin/Data/'
command_scp = 'scp'
param_using_pem_file = ' -i'
local_temp_folder = root_folder_path + 'temp_bins'
remote_source_prefix = 'root@'
remote_source_suffix = ':/'

all_bin_files = '*.bin'
bin_file_tuna_blue = 'prog_blue.bin'
bin_file_tuna_sun = 'prog_sun.bin'
bin_file_refugium = 'prog_rf.bin'
bin_file_zero_ten = 'prog_zero_ten.bin'
bin_file_group = 'group.bin'

number_of_groups = 41
number_of_programs = 13
number_of_program_points_per_program = 20

# [little-endian] int, int, char[40], int, int
program_format = '2i40s2i'

# [little-endian] int, int, int, int, int, int, int, int, int
program_point_format = '9i'

# [little-endian] char[40], int, int
group_prefix_format = '40s2i'
# [little-endian] long long, long long, int, int, int, int, int, int, int, int, int, int, int, int,
group_suffix_format = '2q12i'

# [little-endian] int, int, int, long long, long long
acclimation_format = '3i2q'

# [little-endian] int, int, int, int, int, int, long long, long long
lunar_cycle_format = '6i2q'

re_pattern = re.compile(r'\s+')
# [end] constants definition:

# [start] tuples definition:
ProgramNameTuple = namedtuple('Program', 'pid order name exist type')

ProgramPointNameTuple = namedtuple('Program_Point', 'minute ui_0 ui_1 ui_2 ui_3 ui_4 eid duration frequency')

AcclimationNameTuple = namedtuple('Acclimation', 'enabled start period startDate stopDate')

LunarCycleNameTuple = namedtuple('LunarCycle', 'enabled color end start high phase startDate stopDate')

GroupPrefixNameTuple = namedtuple('GroupPrefix', 'name type hasLamps')
GroupSuffixNameTuple = namedtuple(
    'GroupSuffix', 'startDate stopDate pid eid eff_interval eff_times eff_valley eff_base '
                   'pause ui_0 ui_1 ui_2 ui_3 ui_4')
# [end] tuples definition:


class DongleProgramPoint:

    def __init__(self, p_name_tuple: ProgramPointNameTuple):
        p_ui: list = [p_name_tuple.ui_0, p_name_tuple.ui_1, p_name_tuple.ui_2, p_name_tuple.ui_3, p_name_tuple.ui_4]
        self.minutes = p_name_tuple.minute
        self.UI = p_ui
        self.eid = p_name_tuple.eid
        self.duration = p_name_tuple.duration
        self.freq = p_name_tuple.frequency

    # override
    def __str__(self):
        # return '{} { EID: {}, Minute: {}, Duration: {}, Frequency: {}, UI: {} }' \
        #     .format('DongleProgramPoint', self.p_eid, self.p_minute, self.p_duration, self.p_freq, self.p_ui)
        return 'DongleProgramPoint { EID: ' + str(self.eid) + ', Minute: ' + str(self.minutes) + ', Duration: ' + \
               str(self.duration) + ', Frequency: ' + str(self.freq) + ', UI: ' + str(self.UI) + ' }'


# [start] classes definition:
class DongleProgram(IeObjectToJsonMixin):

    def __init__(self, p_name_tuple: ProgramNameTuple, p_point_list: List[DongleProgramPoint]):
        self.PID = p_name_tuple.pid
        self.order = p_name_tuple.order

        raw_p_name: str = p_name_tuple.name.decode()
        print('raw_p_name: {}, length: {}'.format(raw_p_name, len(raw_p_name)))
        re_p_name: str = re.sub(re_pattern, ' ', raw_p_name)
        print('re_p_name: {}, length: {}'.format(re_p_name, len(re_p_name)))
        self.name = re_p_name

        self.type = p_name_tuple.type

        self.points = p_point_list

    # override
    def __str__(self):
        # return 'DongleProgram { PID: {}, Name: {}, Type: {}, Order: {}, Points.Size: {} }'\
        #     .format('DongleProgram', self.p_id, self.p_name, self.p_type, self.p_order, len(self.p_points))
        return 'DongleProgram { PID: ' + str(self.PID) + ', Name: ' + self.name + ', Type: ' + str(
            self.type) + ', Order: ' + str(self.order) + ', Points.Size: ' + str(len(self.points)) + ' }'


class DongleAcclimation:

    def __init__(self, a_name_tuple: AcclimationNameTuple):
        self.enabled = a_name_tuple.enabled
        self.start = a_name_tuple.start
        self.period = a_name_tuple.period
        self.startDate = a_name_tuple.startDate
        self.stopDate = a_name_tuple.stopDate

    # override
    def __str__(self):
        return 'DongleAcclimation { Enabled: ' + str(self.enabled) + ', Start: ' + str(self.start) + ', Period: ' + \
               str(self.period) + ', StartDate: ' + str(self.startDate) + ', StopDate: ' + str(self.stopDate) + ' }'
    
    
class DongleLunarCycle:

    def __init__(self, l_name_tuple: LunarCycleNameTuple):
        self.enabled = l_name_tuple.enabled
        self.color = l_name_tuple.color
        self.end = l_name_tuple.end
        self.start = l_name_tuple.start
        self.high = l_name_tuple.high
        self.phase = l_name_tuple.phase
        self.startDate = l_name_tuple.startDate
        self.stopDate = l_name_tuple.stopDate

    # override
    def __str__(self):
        return 'DongleLunarCycle { Enabled: ' + str(self.enabled) + ', Color: ' + str(self.color) + ', Start: ' + \
               str(self.start) + ', End: ' + str(self.end) + ', High: ' + str(self.high) + \
               ', Phase: ' + str(self.phase) + ', StartDate: ' + str(self.startDate) + \
               ', StopDate: ' + str(self.stopDate) + ' }'


class DongleGroup(IeObjectToJsonMixin):

    def __init__(self,
                 gid: int,
                 gp_name_tuple: GroupPrefixNameTuple,
                 gs_name_tuple: GroupSuffixNameTuple,
                 a_name_tuple: AcclimationNameTuple,
                 l_name_tuple: LunarCycleNameTuple):
        self.gid = gid

        raw_g_name: str = gp_name_tuple.name.decode()
        print('raw_g_name: {}, length: {}'.format(raw_g_name, len(raw_g_name)))
        re_g_name: str = re.sub(re_pattern, ' ', raw_g_name)
        print('re_g_name: {}, length: {}'.format(re_g_name, len(re_g_name)))
        self.name = re_g_name

        self.type = gp_name_tuple.type
        self.hasLamps = gp_name_tuple.hasLamps

        self.startDate = gs_name_tuple.startDate
        # self.stopDate = gs_name_tuple.stopDate

        self.pid = gs_name_tuple.pid
        self.eid = gs_name_tuple.eid
        self.pause = gs_name_tuple.pause

        g_ui: list = [gs_name_tuple.ui_0, gs_name_tuple.ui_1, gs_name_tuple.ui_2, gs_name_tuple.ui_3, gs_name_tuple.ui_4]
        self.UI = g_ui

        self.accl = DongleAcclimation(a_name_tuple)
        self.lunar = DongleLunarCycle(l_name_tuple)

    # override
    def __str__(self):
        return 'DongleGroup { Name: ' + self.name + ', Type: ' + str(self.type) + ', hasLamps: ' + \
               str(self.hasLamps) + ', Gid: ' + str(self.gid) + ', Pid: ' + str(self.pid) + \
               ', Eid: ' + str(self.eid) + ', Pause: ' + str(self.pause) + \
               ', StartDate: ' + str(self.startDate) + ', UI: ' + str(self.UI) + ' }'


class BinFileToProgramList(IeAbsConverter):
    def execute_conversion(self, item: str) -> List[DongleProgram]:
        try:
            return self.parse_bin_file(item)
        except Exception as cause:
            print('error on BinFileToProgramList#execute_conversion')
            raise IeCustomException(internal_conversion_error, 'error on BinFileToProgramList#execute_conversion', cause)

    def parse_bin_file(self, filename: str) -> List[DongleProgram]:
        file_stream = open(filename, 'rb')
        # print('type of file_stream: {}'.format(type(file_stream)))
        # _io.BufferedReader

        program_format_size = struct.calcsize(program_format)
        # program_format_size: 56
        print('program_format_size: ', program_format_size)

        program_point_format_size = struct.calcsize(program_point_format)
        # program_point_format_size: 36
        print('program_point_format_size: ', program_point_format_size)

        expected_program_byte = number_of_programs * (
                program_format_size + (number_of_program_points_per_program * program_point_format_size))
        sum_up_bytes = 0

        program_list: List[DongleProgram] = []
        for i in range(number_of_programs):
            program_bytes: bytes = file_stream.read(program_format_size)
            sum_up_bytes = sum_up_bytes + len(program_bytes)
            program_tuple: tuple = struct.unpack(program_format, program_bytes)
            # print('program_tuple: {}'.format(program_tuple))
            program_name_tuple = ProgramNameTuple._make(program_tuple)
            # print('program_name_tuple: {}'.format(program_name_tuple))

            p_points: List[DongleProgramPoint] = []
            for j in range(number_of_program_points_per_program):
                program_point_bytes: bytes = file_stream.read(program_point_format_size)
                sum_up_bytes = sum_up_bytes + len(program_point_bytes)
                program_point_tuple: tuple = struct.unpack(program_point_format, program_point_bytes)
                # print('program_point_tuple: {}'.format(program_point_tuple))
                program_point_name_tuple = ProgramPointNameTuple._make(program_point_tuple)
                
                if program_point_name_tuple.minute >= 0:
                    the_program_point = DongleProgramPoint(program_point_name_tuple)
                    print('the_program_point: {}'.format(the_program_point))
                    p_points.append(the_program_point)
                else:
                    print('program_point_name_tuple: {}'.format(program_point_name_tuple))

            if program_name_tuple.pid >= 0:
                the_program = DongleProgram(program_name_tuple, p_points)
                print('the_program: {}'.format(the_program))
                program_list.append(the_program)
            else:
                print('program_name_tuple: {}'.format(program_name_tuple))

        file_stream.close()

        print('expected_program_byte: {}, sum_up_bytes: {}'.format(expected_program_byte, sum_up_bytes))

        return program_list

###


class BinBytesToProgramList(IeAbsConverter):
    def execute_conversion(self, item: bytes) -> List[DongleProgram]:
        try:
            return self.parse_bin_bytes(item)
        except Exception as cause:
            print('error on BinBytesToProgramList#execute_conversion')
            raise IeCustomException(internal_conversion_error,
                                    'error on BinBytesToProgramList#execute_conversion',
                                    cause)

    def parse_bin_bytes(self, bin_bytes: bytes) -> List[DongleProgram]:
        program_list: List[DongleProgram] = []
        if bin_bytes is None:
            print('Warning on BinBytesToProgramList#parse_bin_bytes - bin_bytes is None!!')
            return program_list

        program_format_size = struct.calcsize(program_format)
        # program_format_size: 56
        print('program_format_size: ', program_format_size)

        program_point_format_size = struct.calcsize(program_point_format)
        # program_point_format_size: 36
        print('program_point_format_size: ', program_point_format_size)

        expected_program_bytes = number_of_programs * (
                program_format_size + (number_of_program_points_per_program * program_point_format_size))
        bin_bytes_length = len(bin_bytes)
        print('expected_program_bytes: {}, bin_bytes_length: {}'.format(expected_program_bytes, bin_bytes_length))
        if bin_bytes_length != expected_program_bytes:
            message = 'expected_program_bytes: {} != bin_bytes_length: {}'.format(expected_program_bytes, bin_bytes_length)
            raise IeCustomException(illegal_argument_error, message, None)

        single_program_bytes = program_format_size + (number_of_program_points_per_program * program_point_format_size)
        splited_bytes = [bin_bytes[i:i + single_program_bytes] for i in range(0, len(bin_bytes), single_program_bytes)]
        # print('splited_bytes length: {}, single_program_bytes: {}'.format(len(splited_bytes), single_program_bytes))

        for program_bytes in splited_bytes:
            # print('program_bytes length: {}'.format(len(program_bytes)))
            header_bytes = program_bytes[0:program_format_size]
            program_tuple: tuple = struct.unpack(program_format, header_bytes)
            # print('program_tuple: {}'.format(program_tuple))
            program_name_tuple = ProgramNameTuple._make(program_tuple)
            # print('program_name_tuple: {}'.format(program_name_tuple))

            point_splited_bytes = [program_bytes[i:i + program_point_format_size]
                                   for i in range(program_format_size, len(program_bytes), program_point_format_size)]
            # print('header_bytes length: {}'.format(len(header_bytes)))

            p_points: List[DongleProgramPoint] = []
            for program_point_bytes in point_splited_bytes:
                # print('point_bytes length: {}'.format(len(point_bytes)))

                program_point_tuple: tuple = struct.unpack(program_point_format, program_point_bytes)
                # print('program_point_tuple: {}'.format(program_point_tuple))
                program_point_name_tuple = ProgramPointNameTuple._make(program_point_tuple)

                if program_point_name_tuple.minute >= 0:
                    the_program_point = DongleProgramPoint(program_point_name_tuple)
                    print('the_program_point: {}'.format(the_program_point))
                    p_points.append(the_program_point)
                else:
                    print('program_point_name_tuple: {}'.format(program_point_name_tuple))

            if program_name_tuple.pid >= 0:
                the_program = DongleProgram(program_name_tuple, p_points)
                print('the_program: {}'.format(the_program))
                program_list.append(the_program)
            else:
                print('program_name_tuple: {}'.format(program_name_tuple))

        return program_list

###


class BinFileToGroupList(IeAbsConverter):
    def execute_conversion(self, item: str) -> List[DongleGroup]:
        try:
            return self.parse_bin_file(item)
        except Exception as cause:
            print('error on BinFileToGroupList#execute_conversion')
            raise IeCustomException(internal_conversion_error, 'error on BinFileToGroupList#execute_conversion',
                                    cause)

    @staticmethod
    def parse_bin_file(filename: str) -> List[DongleGroup]:
        file_stream = open(filename, 'rb')

        group_prefix_format_size = struct.calcsize(group_prefix_format)
        # group_prefix_format_size:48
        print('group_prefix_format_size: ', group_prefix_format_size)

        group_suffix_format_size = struct.calcsize(group_suffix_format)
        # group_suffix_format_size: 64
        print('group_suffix_format_size: ', group_suffix_format_size)

        acclimation_format_size = struct.calcsize(acclimation_format)
        # acclimation_format_size: 28
        print('acclimation_format_size: ', acclimation_format_size)

        lunar_cycle_format_size = struct.calcsize(lunar_cycle_format)
        # lunar_cycle_format_size: 40
        print('lunar_cycle_format_size: ', lunar_cycle_format_size)

        the_group_list: List[DongleGroup] = []
        for i in range(number_of_groups):
            group_prefix_bytes: bytes = file_stream.read(group_prefix_format_size)
            group_prefix_tuple: tuple = struct.unpack(group_prefix_format, group_prefix_bytes)
            # print('group_prefix_tuple: {}'.format(group_prefix_tuple))
            group_prefix_name_tuple = GroupPrefixNameTuple._make(group_prefix_tuple)
            # print('group_prefix_name_tuple: {}'.format(group_prefix_name_tuple))

            acclimation_bytes: bytes = file_stream.read(acclimation_format_size)
            acclimation_tuple: tuple = struct.unpack(acclimation_format, acclimation_bytes)
            # print('acclimation_tuple: {}'.format(acclimation_tuple))
            acclimation_name_tuple = AcclimationNameTuple._make(acclimation_tuple)
            # print('acclimation_name_tuple: {}'.format(acclimation_name_tuple))

            lunar_cycle_bytes: bytes = file_stream.read(lunar_cycle_format_size)
            lunar_cycle_tuple: tuple = struct.unpack(lunar_cycle_format, lunar_cycle_bytes)
            # print('lunar_cycle_tuple: {}'.format(lunar_cycle_tuple))
            lunar_cycle_name_tuple = LunarCycleNameTuple._make(lunar_cycle_tuple)
            # print('lunar_cycle_name_tuple: {}'.format(lunar_cycle_name_tuple))

            group_suffix_bytes: bytes = file_stream.read(group_suffix_format_size)
            group_suffix_tuple: tuple = struct.unpack(group_suffix_format, group_suffix_bytes)
            # print('group_suffix_tuple: {}'.format(group_suffix_tuple))
            group_suffix_name_tuple = GroupSuffixNameTuple._make(group_suffix_tuple)
            # print('group_suffix_name_tuple: {}'.format(group_suffix_name_tuple))

            print('group_prefix_name_tuple: {}'.format(group_prefix_name_tuple))
            print('group_suffix_name_tuple: {}'.format(group_suffix_name_tuple))
            if i > 0 and group_prefix_name_tuple.type >= 0:
                the_group = DongleGroup(i, group_prefix_name_tuple, group_suffix_name_tuple,
                                        acclimation_name_tuple, lunar_cycle_name_tuple)
                # print('group_suffix_name_tuple: {}'.format(group_suffix_name_tuple))
                print('the_group: {}'.format(the_group))
                print('the_acclimation: {}'.format(the_group.accl))
                print('the_lunar_cycle: {}'.format(the_group.lunar))
                the_group_list.append(the_group)
            else:
                # print('group_prefix_name_tuple: {}'.format(group_prefix_name_tuple))
                # print('group_suffix_name_tuple: {}'.format(group_suffix_name_tuple))
                print('acclimation_name_tuple: {}'.format(acclimation_name_tuple))
                print('lunar_cycle_name_tuple: {}'.format(lunar_cycle_name_tuple))

        file_stream.close()

        return the_group_list

###


class BinBytesToGroupList(IeAbsConverter):
    def execute_conversion(self, item: bytes) -> List[DongleGroup]:
        try:
            return self.parse_bin_bytes(item)
        except Exception as cause:
            print('error on BinBytesToGroupList#execute_conversion')
            raise IeCustomException(internal_conversion_error,
                                    'error on BinBytesToGroupList#execute_conversion',
                                    cause)

    def parse_bin_bytes(self, bin_bytes: bytes) -> List[DongleGroup]:
        the_group_list: List[DongleGroup] = []
        if bin_bytes is None:
            print('Warning on BinBytesToGroupList#parse_bin_bytes - bin_bytes is None!!')
            return the_group_list

        group_prefix_format_size = struct.calcsize(group_prefix_format)
        # group_prefix_format_size:48
        print('group_prefix_format_size: ', group_prefix_format_size)

        group_suffix_format_size = struct.calcsize(group_suffix_format)
        # group_suffix_format_size: 64
        print('group_suffix_format_size: ', group_suffix_format_size)

        acclimation_format_size = struct.calcsize(acclimation_format)
        # acclimation_format_size: 28
        print('acclimation_format_size: ', acclimation_format_size)

        lunar_cycle_format_size = struct.calcsize(lunar_cycle_format)
        # lunar_cycle_format_size: 40
        print('lunar_cycle_format_size: ', lunar_cycle_format_size)

        single_group_bytes_length = group_prefix_format_size + group_suffix_format_size \
                                    + acclimation_format_size + lunar_cycle_format_size
        expected_group_bytes = number_of_groups * single_group_bytes_length
        bin_bytes_length = len(bin_bytes)
        print('expected_group_bytes: {}, bin_bytes_length: {}'.format(expected_group_bytes, bin_bytes_length))
        if bin_bytes_length != expected_group_bytes:
            message = 'expected_group_bytes: {} != bin_bytes_length: {}'.format(expected_group_bytes, bin_bytes_length)
            raise IeCustomException(illegal_argument_error, message, None)

        splited_bytes = [bin_bytes[i:i + single_group_bytes_length]
                         for i in range(0, len(bin_bytes), single_group_bytes_length)]
        print('splited_bytes length: {}'.format(len(splited_bytes)))

        for i in range(len(splited_bytes)):
            single_group_bytes = splited_bytes[i]
            print('single_group_bytes length: {}'.format(len(single_group_bytes)))

            start_index = 0
            end_index = start_index + group_prefix_format_size
            group_prefix_bytes = single_group_bytes[start_index:end_index]
            group_prefix_tuple: tuple = struct.unpack(group_prefix_format, group_prefix_bytes)
            # print('group_prefix_tuple: {}'.format(group_prefix_tuple))
            group_prefix_name_tuple = GroupPrefixNameTuple._make(group_prefix_tuple)
            # print('group_prefix_name_tuple: {}'.format(group_prefix_name_tuple))

            start_index = end_index
            end_index = start_index + acclimation_format_size
            acclimation_bytes = single_group_bytes[start_index:end_index]
            acclimation_tuple: tuple = struct.unpack(acclimation_format, acclimation_bytes)
            # print('acclimation_tuple: {}'.format(acclimation_tuple))
            acclimation_name_tuple = AcclimationNameTuple._make(acclimation_tuple)
            # print('acclimation_name_tuple: {}'.format(acclimation_name_tuple))

            start_index = end_index
            end_index = start_index + lunar_cycle_format_size
            lunar_cycle_bytes = single_group_bytes[start_index:end_index]
            lunar_cycle_tuple: tuple = struct.unpack(lunar_cycle_format, lunar_cycle_bytes)
            # print('lunar_cycle_tuple: {}'.format(lunar_cycle_tuple))
            lunar_cycle_name_tuple = LunarCycleNameTuple._make(lunar_cycle_tuple)
            # print('lunar_cycle_name_tuple: {}'.format(lunar_cycle_name_tuple))

            start_index = end_index
            end_index = start_index + group_suffix_format_size
            group_suffix_bytes = single_group_bytes[start_index:end_index]
            group_suffix_tuple: tuple = struct.unpack(group_suffix_format, group_suffix_bytes)
            # print('group_suffix_tuple: {}'.format(group_suffix_tuple))
            group_suffix_name_tuple = GroupSuffixNameTuple._make(group_suffix_tuple)
            # print('group_suffix_name_tuple: {}'.format(group_suffix_name_tuple))

            print('group_prefix_name_tuple: {}'.format(group_prefix_name_tuple))
            print('group_suffix_name_tuple: {}'.format(group_suffix_name_tuple))
            if i > 0 and group_prefix_name_tuple.type >= 0:
                the_group = DongleGroup(i, group_prefix_name_tuple, group_suffix_name_tuple,
                                        acclimation_name_tuple, lunar_cycle_name_tuple)
                # print('group_suffix_name_tuple: {}'.format(group_suffix_name_tuple))
                print('the_group: {}'.format(the_group))
                print('the_acclimation: {}'.format(the_group.accl))
                print('the_lunar_cycle: {}'.format(the_group.lunar))
                the_group_list.append(the_group)
            else:
                # print('group_prefix_name_tuple: {}'.format(group_prefix_name_tuple))
                # print('group_suffix_name_tuple: {}'.format(group_suffix_name_tuple))
                print('acclimation_name_tuple: {}'.format(acclimation_name_tuple))
                print('lunar_cycle_name_tuple: {}'.format(lunar_cycle_name_tuple))

        return the_group_list

# [end] classes definition:


# [start] test statement:
def test_decode_bin_files():
    tuna_blue1 = root_folder_path + folder_LS4SGS0004 + bin_file_tuna_blue
    tuna_sun1 = root_folder_path + folder_LS4SGS0004 + bin_file_tuna_sun
    refugium1 = root_folder_path + folder_LS4SGS0004 + bin_file_refugium
    zero_ten1 = root_folder_path + folder_LS4SGS0004 + bin_file_zero_ten
    group1 = root_folder_path + folder_LS4SGS0004 + bin_file_group

    try:
        IeObjectToJsonMixin().to_json()
        output_program_list: list = BinFileToProgramList().do_convert(tuna_blue1)
        for program_item in output_program_list:
            print('[program]: {}'.format(program_item))
            print('[program to json]: {}'.format(program_item.to_json()))

        # output_group_list: list = BinFileToGroupList().do_convert(group1)
        # for group_item in output_group_list:
        #     print('[group]: {}'.format(group_item))
        #     print('[group to json]: {}'.format(group_item.to_json()))
    except Exception as error1:
        print('error on parsing bin file', traceback.print_tb(error1.__traceback__))
# [end] test statement:


if __name__ == '__main__':
    test_decode_bin_files()
