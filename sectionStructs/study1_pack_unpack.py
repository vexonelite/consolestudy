
# https://docs.python.org/3/library/struct.html

# 1. Include module struct
import struct

from collections import namedtuple

# 2. define the format string
format_string = 'hhl'

# 3. Pack list of numbers. Method pack()
pack_obj = struct.pack(format_string, 1, 2, 3)  # type: bytes

# 4. Display the object pack_obj
# type: bytes
print('type of pack_obj: {}'.format(type(pack_obj)))
# b'\x01\x00\x02\x00\x00\x00\x00\x00\x03\x00\x00\x00\x00\x00\x00\x00'
print('pack_obj: ', pack_obj)

unpacked_obj = struct.unpack(format_string, pack_obj)
# unpacked_obj = struct.unpack(format_string, b'\x00\x01\x00\x02\x00\x00\x00\x03')
print('type of unpacked_obj: {}'.format(type(unpacked_obj)))
print('unpacked_obj: ', unpacked_obj)

print('size of format_string: ', struct.calcsize(format_string))

print('=========================================================')
###

# 2. define the format string
# byte order little-endian, char[] with size 10, 2 unsigned shorts and 1 signed char
format_string2 = '<10sHHb'


record = b'raymond   \x32\x12\x08\x01\x08'
print('type of record: {}'.format(type(record)))
# b'\x01\x00\x02\x00\x00\x00\x00\x00\x03\x00\x00\x00\x00\x00\x00\x00'
print('record: ', record)
print('size of record: ', len(record))

name, serialnum, school, gradelevel = struct.unpack(format_string2, record)
print('name: {}, serialnum: {}, school: {}, gradelevel: {}'.format(name, serialnum, school, gradelevel))

# define a namedtuple Student
Student = namedtuple('Student', 'name serialnum school gradelevel')
student = Student._make(struct.unpack(format_string2, record))
# Student(name=b'raymond   ', serialnum=4658, school=264, gradelevel=8)
print('student: ', student)
