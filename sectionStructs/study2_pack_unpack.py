##  https://www.bestprog.net/en/2020/05/08/python-module-struct-packing-unpacking-data-basic-methods/

# Module struct. Methods pack(), unpack()
# Pack/unpack list of numbers

# 1. Include module struct
import struct

# 2. Specified list of numbers
number_list = [1, 3, 9, 12]

# 3. define the format string: byte order: big-endian, 4 numbers of type int
# UnicodeEncodeError: 'ascii' codec can't encode character '\u0456' in position 2 : ordinal not in range(128)
format_string = '<4i'
# 4. Pack list of numbers. Method pack()
pack_obj = struct.pack(format_string, number_list[0], number_list[1], number_list[2], number_list[3])
# pack_obj = struct.pack(format_string, 1, 1, 1, 1)

# 5. Display the object pack_obj
print('type_of_pack_obj: {}'.format(type(pack_obj)))
print('pack_obj: ', pack_obj)

# 6. Unpack list of numbers. Method unpack().
# The result is a tuple unpacked_obj
unpacked_obj = struct.unpack(format_string, pack_obj)  # unpacked_obj = (1, 3, 9, 12)

# 7. Print the unpacked object unpacked_obj
print('type_of_pack_obj: {}'.format(type(unpacked_obj)))
print('unpacked_obj: ', unpacked_obj)

# 8. Convert tuple unpacked_obj to list number_list2
number_list2 = list(unpacked_obj)  # number_list2 = [1, 3, 9, 12]

# 9. Display the list LS2
print('number_list2: ', number_list2)
