import asyncio
import datetime

# # Handling Background Operations as They Finish

# ``as_completed()`` is a generator that
# manages the execution of a list of coroutines given to it
# and produces their results one at a time as they finish running.
# As with ``wait()``, __order is not guaranteed by__ ``as_completed()``,
# but it is not necessary to wait for all of the background operations
# to complete before taking other action.

# This example starts several background phases that finish in the reverse order
# from which they start. As the generator is consumed,
# the loop waits for the result of the coroutine using ``await``.


async def phase(i: int):
    """
    the sub-Coroutine that runs in parallel way with other sub-Coroutines
    """
    print('in phase {}, {}'.format(i, datetime.datetime.now()))
    await asyncio.sleep(6 - i)
    print('done with phase {}, {}'.format(i, datetime.datetime.now()))
    return 'phase {} result'.format(i)


async def main(num_phases: int):
    """
    the main Coroutine that waits the result of task to be produced
    """
    print('starting main')
    phases = [
        phase(i)
        for i in range(num_phases)
    ]
    print('waiting for phases to complete')
    results = []
    for next_to_complete in asyncio.as_completed(phases):
        answer = await next_to_complete
        print('received answer {!r}, {}'.format(answer, datetime.datetime.now()))
        results.append(answer)
    print('results: {!r}, {}'.format(results, datetime.datetime.now()))
    return results


event_loop = asyncio.get_event_loop()
try:
    print('entering event loop')
    event_loop.run_until_complete(main(3))
finally:
    event_loop.close()
    print('closing event loop')
