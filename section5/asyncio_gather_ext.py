import asyncio

# # Gathering Results from Coroutines

# If the background phases are well-defined, and only the results of those phases matter,
# then ``gather()`` may be more useful for waiting for multiple operations.

# The tasks created by ``gather()`` __are not exposed, so they cannot be cancelled__!!
# The return value is __a list of results in the same order__
# as the arguments passed to ``gather()``,
# regardless of the order the background operations actually completed.


async def phase(i: int):
    """
    the sub-Coroutine that runs in parallel way with other sub-Coroutines
    """
    print('in phase {}'.format(i))
    try:
        await asyncio.sleep(1 * i)
    except asyncio.CancelledError:
        print('phase {} canceled'.format(i))
        raise
    else:
        print('done with phase {}'.format(i))
        return 'phase {} result'.format(i)


async def main(num_phases: int):
    """
    the main Coroutine that waits the result of task to be produced
    """
    print('starting main')
    phases = [
        phase(i)
        for i in range(num_phases)
    ]
    print('waiting for phases to complete')
    # results = await asyncio.gather(phases)
    for task in phases:
        results = await asyncio.gather(task)
    print('results: {!r}'.format(results))


event_loop = asyncio.get_event_loop()
try:
    print('entering event loop')
    event_loop.run_until_complete(main(3))
finally:
    event_loop.close()
    print('closing event loop')
