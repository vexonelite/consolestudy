import asyncio

# # Starting a Coroutine

# There are a few different ways to have the ``asyncio`` event loop start a coroutine.
# The simplest is to use ``run_until_complete()``, passing the coroutine to it directly.

# The first step is to obtain a reference to the ``event loop``.
# The default loop type can be used, or a specific loop class can be instantiated.
# In this example, the default loop is used.
# The ``run_until_complete()`` method starts the loop with the coroutine object
# and stops the loop when the coroutine exits by returning.


async def coroutine():
    print('in coroutine')


event_loop = asyncio.get_event_loop()
try:
    print('starting coroutine')
    coro = coroutine()
    print('entering event loop')
    event_loop.run_until_complete(coro)
finally:
    event_loop.close()
    print('closing event loop')