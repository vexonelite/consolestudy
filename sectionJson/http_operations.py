from abc import ABC #, ABCMeta
import json
import requests as py_http_requests
from requests import Response

from .chain_operations import IeAbsCallable, IeAbsConverter, IeAbsFilter
from .exceptions import IeCustomException, http_request_error, http_illegal_argument_error, \
    http_wrong_status_code, internal_conversion_error


class AbsIeHttpOperation(ABC, IeAbsCallable):
    def __init__(self, url: str, headers: dict = None, timeout: float = 60.0):
        self.url = url
        self.headers = headers
        self.timeout = timeout


class IeHttpPost(AbsIeHttpOperation):

    def __init__(self,
                 url: str,
                 headers: dict = None,
                 timeout: float = 60.0,
                 data_payload: dict = None,
                 json_payload: str = None):
        super().__init__(url=url, headers=headers, timeout=timeout)
        self.data_payload = data_payload
        self.json_payload = json_payload

    # override IeCallableDelegate
    def execute_generation(self) -> Response:
        try:
            if self.headers:
                print('headers: {}'.format(self.headers))
                if self.data_payload:
                    print('data_payload: {}'.format(self.data_payload))
                    http_response = py_http_requests.post(
                        self.url, data=self.data_payload, headers=self.headers, timeout=self.timeout) #, verify=False
                elif self.json_payload:
                    print('json_payload: {}'.format(self.json_payload))
                    http_response = py_http_requests.post(
                        self.url, json=self.json_payload, headers=self.headers, timeout=self.timeout) #, verify=False
                else:
                    http_response = None
            else:
                if self.data_payload:
                    print('data_payload: {}'.format(self.data_payload))
                    http_response = py_http_requests.post(self.url, data=self.data_payload, timeout=self.timeout) # verify=False
                elif self.json_payload:
                    print('json_payload: {}'.format(self.json_payload))
                    http_response = py_http_requests.post(self.url, json=self.json_payload, timeout=self.timeout) # verify=False
                else:
                    http_response = None
            if http_response is None:
                raise IeCustomException(http_illegal_argument_error, 'either data_payload or json_payload is None')
            else:
                print('type of response: {}'.format(type(http_response)))
                # requests.models.Response
                return http_response
        except Exception as cause:
            print('error on send HTTP POST request: {}'.format(cause))
            raise IeCustomException(http_request_error, 'error on send_command', cause)


class IeHttpGet(AbsIeHttpOperation):

    def __init__(self, url: str, headers: dict = None, timeout: float = 60.0):
        super().__init__(url=url, headers=headers, timeout=timeout)

    # override IeCallableDelegate
    def execute_generation(self) -> Response:
        try:
            if self.headers:
                print('headers: {}'.format(self.headers))
                # http_response = py_http_requests.get(self.url, headers=self.header, verify=False)
                http_response = py_http_requests.get(self.url, headers=self.headers, timeout=self.timeout)
            else:
                # http_response = py_http_requests.get(self.url, verify=False)
                http_response = py_http_requests.get(self.url, timeout=self.timeout)
            print('type of response: {}'.format(type(http_response)))
            # requests.models.Response
            return http_response
        except Exception as cause:
            print('error on send HTTP GET request: {}'.format(cause))
            raise IeCustomException(http_request_error, 'error on send_command', cause)


class ResponseVerifier(IeAbsConverter):

    def __init__(self, expected_status_code: int):
        self.expected_status_code = expected_status_code

    # override IeAbsConverter
    def execute_conversion(self, item: Response) -> Response:
        # input_data.encoding: str
        print('type of input_data.encoding : {}, encoding: {}'.format(type(item.encoding), item.encoding))
        # input_data.encoding: int
        print('type of input_data.status_code : {}, status_code: {}'
              .format(type(item.status_code), item.status_code))
        # input_data.headers: requests.structures.CaseInsensitiveDict
        # print('type of input_data.headers : {}, headers: {}'.format(type(input_data.headers), input_data.headers))
        # input_data.content: bytes
        print('type of input_data.content : {}, content: {}'.format(type(item.content), item.content))
        if self.expected_status_code == item.status_code:
            return item
        else:
            raise IeCustomException(http_wrong_status_code, 'wrong http code: {}'.format(item.status_code))


class ResponseToJson(IeAbsConverter):

    def __init__(self):
        pass

    # override IeAbsConverter
    def execute_conversion(self, item: Response) -> dict:
        try:
            json_response = item.json()
            print('type of json_response : {}, json_response: {}'.format(type(json_response), json_response))
            return json_response
        except Exception as cause:
            print('error on ResponseToJson!!')
            raise IeCustomException(internal_conversion_error, 'Fail to involve input_data.json()!', cause)


class SaBeeFilter(IeAbsFilter):

    def __init__(self):
        pass

    # override IeFilterDelegate
    def execute_judgement(self, item: str) -> bool:
        return 'SaBee' in item


class SsoFacebookLoginParser(IeAbsConverter):
    def execute_conversion(self, item: Response) -> dict:
        pass


response_verifier = ResponseVerifier(200)
response_to_json_converter = ResponseToJson()
# i_url1: str = 'https://api.github.com/users/vexonelite'
# i_url2: str = 'http://127.0.0.1:8000/sso/plainyogurt/'
# i_header1 = {
#     "Authorization": "Token d5135d0ce91a839dc4a4cf4274f409c6c5e15104",
#     "content-type": "application/json"
# }
# i_header2 = {
#     "Authorization": "Token cebcaa45817a13600021c62305fc1854837dedf6",
#     "content-type": "application/json"
# }
# result_http_response1 = IeHttpGet(url=i_url1).call()
# print('result_http_response1: {}'.format(result_http_response1))
# temp_result1 = response_verifier.do_convert(result_http_response1)
# json_result1 = response_to_json_converter.do_convert(result_http_response1)
# print('json_result1: {}'.format(json_result1))
#
# print('==========================================')
# result_http_response2 = IeHttpGet(url=i_url2, headers=i_header2).call()
# print('result_http_response2: {}'.format(result_http_response2))
# temp_result2 = response_verifier.do_convert(result_http_response2)
# json_result2 = response_to_json_converter.do_convert(result_http_response2)
# print('json_result2: {}'.format(json_result2))
#
# print('==========================================')
# result_http_response3 = IeHttpGet(url=i_url2, headers=i_header1).call()
# print('result_http_response3: {}'.format(result_http_response3))
# temp_result3 = response_verifier.do_convert(result_http_response3)
# json_result3 = response_to_json_converter.do_convert(result_http_response3)
# print('json_result3: {}'.format(json_result3))


#i_url: str = 'http://api.kessil.com:8000/sso/login/facebook/'
i_url3: str = 'http://127.0.0.1:8000/sso/login/facebook/'
i_data_payload1 = {
  "email": "elet960112@hotmail.com",
  "name": "Rebecca-Tesora Lin",
  "picture_url": "https://graph.facebook.com/2300295153519132/picture?type=large&access_token=EAAgWWpEAPAABAMRpMaOZAatzp3ZAp00CMWG566k5AJnYudkCGvEd9DJeXEcABAuFqFBBuNiDcZAUb8qPY6YzK8rGPyYKFpEE40pZA6o4njj72whVxRwCu6ZBqmQipdvHs3NmnIdNZCOC8fT6bmkynx59XCTVOcZCD9yQuVDoDdHbsFGyuDTjK8uDfNC9ILU75NGpm9KmqrNBfKZC0asDeA0Drdny1KFNN42HwWMNCr6ZAtYIa6rjgNkJZB",
  "userId": "2300295153519132",
  "userToken": "EAAgWWpEAPAABAMRpMaOZAatzp3ZAp00CMWG566k5AJnYudkCGvEd9DJeXEcABAuFqFBBuNiDcZAUb8qPY6YzK8rGPyYKFpEE40pZA6o4njj72whVxRwCu6ZBqmQipdvHs3NmnIdNZCOC8fT6bmkynx59XCTVOcZCD9yQuVDoDdHbsFGyuDTjK8uDfNC9ILU75NGpm9KmqrNBfKZC0asDeA0Drdny1KFNN42HwWMNCr6ZAtYIa6rjgNkJZB"
}
print('type of i_data_payload1: {}'.format(type(i_data_payload1)))
i_json_payload1 = json.dumps(i_data_payload1)
print('type of i_json_payload1: {}'.format(type(i_json_payload1)))
i_header3 = {"content-type": "application/json"}
i_header4 = {"content-type": "application/json; charset=utf-8"}
print('type of i_header4: {}'.format(type(i_header4)))

print('==========================================')
result_http_response4 = IeHttpPost(url=i_url3, headers=i_header4, json_payload=i_json_payload1).call()
print('result_http_response4: {}'.format(result_http_response4))
temp_result4 = response_verifier.do_convert(result_http_response4)
json_result4 = response_to_json_converter.do_convert(result_http_response4)
print('json_result4: {}'.format(json_result4))

# cannot use data_payload when specifying "content-type": "application/json" in headers
print('==========================================')
result_http_response5 = IeHttpPost(url=i_url3, headers=i_header4, data_payload=i_data_payload1).call()
print('result_http_response5: {}'.format(result_http_response5))
temp_result5 = response_verifier.do_convert(result_http_response5)
json_result5 = response_to_json_converter.do_convert(result_http_response5)
print('json_result5: {}'.format(json_result5))


