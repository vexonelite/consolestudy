

# https://docs.python.org/3.3/tutorial/errors.html#tut-userexceptions
# https://www.programiz.com/python-programming/user-defined-exception
# https://www.codementor.io/sheena/how-to-write-python-custom-exceptions-du107ufv9
# https://franklingu.github.io/programming/2016/06/30/properly-reraise-exception-in-python/
class IeCustomException(Exception):
    def __init__(self, status_code: str, message: str, errors: Exception = None):
        self.status_code = status_code
        self.message = message
        self.errors = errors


def log_wrapper(*args, **kwargs):
    if len(kwargs) > 0:
        print(args, kwargs)
    else:
        print(args)


unknown_error = '00000'

http_execute_request_failure = '99599'
# catch the {@link java.io.IOException} in Java
http_request_error = '99598'
http_response_error = '99597'
http_response_parsing_error = '99597'
http_illegal_argument_error = '99596'
http_wrong_status_code = '99595'

ftp_connection_failure = "99399"
ftp_connection_timeout = "99398"
ftp_login_failure = "99397"
ftp_logout_failure = "99396"
ftp_change_directory_failure = "99395"
ftp_fail_to_get_file_list = "99394"
ftp_download_failure = "99393"
ftp_upload_failure = "99392"
ftp_delete_failure = "99391"
ftp_disconnect_failure = "99390"
ftp_general_operation_failure = "99379"

socket_error_common = '89999'
socket_error_connection = '89998'
socket_error_disconnection = '89997'
socket_error_write = '89996'
socket_error_read = '89995'
socket_error_selector_register_events = '89994'

database_error_common = '79999'
database_error_connection = '79998'
database_error_disconnection = '79997'
database_error_no_connection = '79996'
database_error_query = '79995'
database_query_no_result = '79994'

internal_generation_error = '99998'
internal_conversion_error = '99997'
internal_filtering_error = '99996'
internal_process_error = '99995'
illegal_argument_error = '99994'

file_read_error = '99499'
