import zipfile

from abc import ABC  # ABCMeta
from io import BytesIO, StringIO
from .exceptions import IeCustomException, log_wrapper, internal_process_error


class Point:
    """
    Point class represents and manipulates x,y coords.
    https://openbookproject.net/thinkcs/python/english3e/classes_and_objects_I.html
    """

    def __init__(self, x, y):
        """ Create a new point at the origin """
        self.x = x
        self.y = y


class BaseZipFile(ABC):

    def __init__(self, filename: str):
        self._filename = filename

    def get_filename(self) -> str:
        return self._filename


class ZipBinaryFile(BaseZipFile):

    def __init__(self, filename, byte_array):
        super().__init__(filename)
        self._byte_array = byte_array

    def get_value_from_byte_array_io(self):
        try:
            binary_byte_io = BytesIO()
            binary_byte_io.write(self._byte_array)
            return binary_byte_io.getvalue()
        except Exception as cause:
            log_wrapper('error on [ZipBinaryFile] get_value_from_byte_array_io!', cause)
            raise IeCustomException(internal_process_error,
                                    'Fail to [ZipBinaryFile] get_value_from_byte_array_io!',
                                    cause)


class ZipTextFile(BaseZipFile):

    def __init__(self, filename: str, string_content):
        super().__init__(filename)
        self._string_content = string_content

    def get_value_from_content_string_io(self):
        try:
            content_string_io = StringIO()
            content_string_io.write(self._string_content)
            return content_string_io.getvalue()
        except Exception as cause:
            log_wrapper('error on get_value_from_content_string_io!', cause)
            raise IeCustomException(internal_process_error,
                                    'Fail to get_value_from_content_string_io!',
                                    cause)


class ZipContainer:

    def __init__(self):
        self._buffer_byte_io = BytesIO()
        self._zip_archive = zipfile.ZipFile(self._buffer_byte_io, mode='w')

    def add_binary_file(self, zip_binary_file):
        self._zip_archive.writestr(zip_binary_file.get_filename(), zip_binary_file.get_value_from_byte_array_io())

    def add_text_file(self, zip_text_file):
        self._zip_archive.writestr(zip_text_file.get_filename(), zip_text_file.get_value_from_content_string_io())

    def get_value_from_byte_array_io(self):
        try:
            self._zip_archive.close()
            log_wrapper(self._zip_archive.printdir())
            return self._buffer_byte_io.getvalue()
        except Exception as cause:
            log_wrapper('error on [ZipContainer] get_value_from_byte_array_io!', cause)
            raise IeCustomException(internal_process_error,
                                    'Fail to [ZipContainer] get_value_from_byte_array_io!',
                                    cause)
