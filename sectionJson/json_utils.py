import json

from abc import ABC
from. chain_operations import T


# [Convert Python Objects to JSON](http://tdongsi.github.io/blog/2016/05/21/convert-python-objects-to-json/)
def object_to_json(given: T) -> str:
    try:
        return json.dumps(given, default=vars, indent=4)
    except Exception as cause:
        print('error on object_to_json {}'.format(cause))
        return ''


class IeObjectToJsonMixin:
    def to_json(self):
        return object_to_json(self)


# [Serialization in Python with JSON](http://thepythoncorner.com/dev/serialization-python-json/)
class IeObjectFromJsonMixin:
    @classmethod
    def from_json(cls, json_dict: dict):
        return cls(**json_dict)


class IeObjectToJsonDelegate(ABC):
    # abstract
    def to_json(self) -> str:
        """
        return output
        """
        raise NotImplementedError("do_convert() must be overridden.")


class IeObjectToJsonImpl(IeObjectToJsonDelegate):
    # override
    def to_json(self):
        return object_to_json(self)

###


def get_class_and_module(given: T) -> dict:
    object_dict = {
        "__class__": given.__class__.__name__,
        "__module__": given.__module__
    }
    return object_dict


class IeClassModuleDictMixin1:
    def get_class_and_module(self):
        return get_class_and_module(self)


class IeClassModuleDictMixin2:

    @classmethod
    def get_class_and_module(cls):
        object_dict = {
            "__class__": cls.__name__,
            "__module__": cls.__module__
        }
        return object_dict

    @classmethod
    def get_to_json_class(cls):
        return getattr(cls.__module__, cls.__name__)

    @classmethod
    def from_json(cls, json_dict: dict):
        the_class_ = getattr(cls.__module__, cls.__name__)
        return the_class_(**json_dict)


# [JSON — The Python Way](https://medium.com/python-pandemonium/json-the-python-way-91aac95d4041)
# def convert_to_dict(obj):
def convert_into_dict(given: T) -> dict:
    """
    A function takes in a custom object and returns a dictionary representation of the object.
    This dict representation includes meta data such as the object's module and class names.
    """

    #  Populate the dictionary with object meta data
    # object_dict = {
    #     "__class__": given.__class__.__name__,
    #     "__module__": given.__module__
    # }
    object_dict = get_class_and_module(given)

    #  Populate the dictionary with object properties
    object_dict.update(given.__dict__)

    return object_dict


# [JSON — The Python Way](https://medium.com/python-pandemonium/json-the-python-way-91aac95d4041)
# def dict_to_obj(our_dict):
def dict_to_object(given_dict: dict):
    """
    Function that takes in a dict and returns a custom object associated with the dict.
    This function makes use of the "__module__" and "__class__" metadata in the dictionary
    to know which object type to create.
    """
    if "__class__" in given_dict:
        # Pop ensures we remove metadata from the dict to leave only the instance arguments
        class_name = given_dict.pop("__class__")

        # Get the module name from the dict and import it
        module_name = given_dict.pop("__module__")

        # We use the built in __import__ function since the module name is not yet known at runtime
        module = __import__(module_name)

        # Get the class from the module
        class_ = getattr(module, class_name)

        # Use dictionary unpacking to initialize the object
        the_object = class_(**given_dict)
    else:
        the_object = given_dict
    return the_object


class IeObjectToDictMixin:
    def to_dict(self):
        return convert_into_dict(self)
