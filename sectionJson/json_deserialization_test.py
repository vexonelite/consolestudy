# https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
# python3.8 -m sectionJson.object_to_json_test

import json
import traceback
from .dongle_models import (
    IeSimpleGroupId, IeDongleLamp, IeSimpleProgramIdOrderWrapper, IeSystemInfoResponse, IeDongleResponseWrapper,
    IeDongleGroup, IeDongleProgram, IeSimpleDongleGroup, json_dongle_key_group, json_dongle_key_program
)
from .json_utils import convert_into_dict, dict_to_object, IeObjectFromJsonMixin
from typing import List


# [JSON — The Python Way](https://medium.com/python-pandemonium/json-the-python-way-91aac95d4041)
class User:
    """
    Custom User Class
    """
    def __init__(self, name, age, active, balance, other_names, friends, spouse):
        self.name = name
        self.age = age
        self.active = active
        self.balance = balance
        self.other_names = other_names
        self.friends = friends
        self.spouse = spouse

    def __str__(self):
        return 'User { name: ' + self.name + ', age: ' + str(self.age) + ', balance: ' + str(self.balance) + \
               ', spouse: ' + str(self.spouse) + ', friends: ' + str(self.friends) + ', other_names: ' + \
               str(self.other_names) + ' }'


def json_test1():
    print('=================================================================')
    user_data = json.loads(
        '{"__class__": "User", "__module__": "__main__", "name": "Foo Bar", "age": 78, "active": true, "balance": 345.8, "other_names": ["Doe", "Joe"], "friends": ["Jane", "John"], "spouse": null}')
    print('type of user_data: {}, content: {}'.format(type(user_data), user_data))


def json_test2():
    print('=================================================================')
    user_data = '{"__class__": "User", "__module__": "__main__", "name": "Foo Bar", "age": 78, "active": true, "balance": 345.8, "other_names": ["Doe", "Joe"], "friends": ["Jane", "John"], "spouse": null}'
    new_object = json.loads(user_data, object_hook=dict_to_object)
    print('type of new_object: {}, content: {}'.format(type(new_object), new_object))


class JsonTestObject1:
    def __init__(self, name: str, age: int, balance: float, active: bool, spouse,
                 friends: List[str], other_names: List[str]):
        self.name = name
        self.age = age
        self.balance = balance
        self.active = active
        self.spouse = spouse
        self.friends = friends
        self.other_names = other_names

    def __str__(self):
        return 'JsonTestObject1 { name: ' + self.name + ', age: ' + str(self.age) + ', balance: ' + str(self.balance) + \
                ', spouse: ' + str(self.spouse) + ', friends: ' + str(self.friends) + ', other_names: ' + \
               str(self.other_names) + ' }'

    @classmethod
    def get_class_and_module(cls):
        object_dict = {
            "__class__": cls.__name__,
            "__module__": cls.__module__
        }
        return object_dict


def json_test3():
    print('=================================================================')
    user_data = '{"name": "Foo Bar", "age": 78, "active": true, "balance": 345.8, "other_names": ["Doe", "Joe"], "friends": ["Jane", "John"], "spouse": null}'
    print('type of user_data: {}, content: {}'.format(type(user_data), user_data))
    user_dict = json.loads(user_data)
    print('type of user_dict: {}, content: {}'.format(type(user_dict), user_dict))
    object_dict = JsonTestObject1.get_class_and_module()
    print('type of object_dict: {}, content: {}'.format(type(object_dict), object_dict))
    object_dict.update(user_dict)
    print('type of object_dict: {}, content: {}'.format(type(object_dict), object_dict))

    json_str = json.dumps(object_dict)
    new_object = json.loads(json_str, object_hook=dict_to_object)
    print('type of new_object: {}, content: {}'.format(type(new_object), new_object))


class JsonWindow(IeObjectFromJsonMixin):

    def __init__(self, title: str, name: str, width: int, height: int):
        self.title = title
        self.name = name
        self.width = width
        self.height = height

    def __str__(self):
        return 'Window { title: ' + self.title + ', name: ' + self.name + ', width: ' + str(self.width) + \
               ', height: ' + str(self.height) + '}'


class JsonImage(IeObjectFromJsonMixin):

    def __init__(self, src: str, name: str, hOffset: int, vOffset: int, alignment: str):
        self.src = src
        self.name = name
        self.hOffset = hOffset
        self.vOffset = vOffset
        self.alignment = alignment

    def __str__(self):
        return 'Image { src: ' + self.src + ', name: ' + self.name + ', hOffset: ' + str(self.hOffset) + \
               ', vOffset: ' + str(self.vOffset) + ', alignment: ' + self.alignment + '}'


class JsonText(IeObjectFromJsonMixin):

    def __init__(self, data: str, size: int, style: str, name: str,
                 hOffset: int, vOffset: int, alignment: str, onMouseUp: str):
        self.data = data
        self.size = size
        self.style = style
        self.name = name
        self.hOffset = hOffset
        self.vOffset = vOffset
        self.alignment = alignment
        self.onMouseUp = onMouseUp

    def __str__(self):
        return 'Text { data: ' + self.data + ', size: ' + str(self.size) + ', style: ' + self.style + \
               ', name: ' + self.name + ', hOffset: ' + str(self.hOffset) + \
               ', vOffset: ' + str(self.vOffset) + ', alignment: ' + self.alignment + \
               ', onMouseUp: ' + self.onMouseUp + '}'


class JsonWidget:
    def __init__(self, debug: str, window: JsonWindow, image: JsonImage, text: JsonText):
        self.debug = debug
        self.window = window
        self.image = image
        self.text = text

    def __str__(self):
        return 'Widget { debug: ' + self.debug + ', ' + str(self.window) + ', ' + str(self.image) + ', ' + \
               str(self.text) + '}'

    @classmethod
    def from_json(cls, json_dict: dict):
        debug = json_dict['debug']
        window_dict = json_dict['window']
        print('type of widget_object: {}, content: {}'.format(type(window_dict), window_dict))

        image_dict = json_dict['image']
        print('type of image_dict: {}, content: {}'.format(type(image_dict), image_dict))

        text_dict = json_dict['text']
        print('type of text_dict: {}, content: {}'.format(type(text_dict), text_dict))

        window = JsonWindow.from_json(window_dict)
        image = JsonImage.from_json(image_dict)
        text = JsonText.from_json(text_dict)
        return cls(debug, window, image, text)


def json_test4():
    print('=================================================================')
    json_widget_str = '{"widget":{"debug":"on","window":{"title":"Sample Konfabulator Widget","name":"main_window","width":500,"height":500},"image":{"src":"Images/Sun.png","name":"sun1","hOffset":250,"vOffset":250,"alignment":"center"},"text":{"data":"Click Here","size":36,"style":"bold","name":"text1","hOffset":250,"vOffset":100,"alignment":"center","onMouseUp":"sun1.opacity = (sun1.opacity / 100) * 90;"}}}'
    json_widget_dict = json.loads(json_widget_str)
    widget_dict = json_widget_dict['widget']
    print('widget_dict: {}'.format(widget_dict))
    widget_object = JsonWidget.from_json(widget_dict)
    print('type of widget_object: {}, content: {}'.format(type(widget_object), widget_object))


# system_info
def json_test5():
    print('=================================================================')
    system_info_json_str = '{"code":0,"msg":"Success","dType":114,"sn":"L4JSGS0004","ver":"1.5","SSID":"IE-Google","ENC":"psk2","timezone":"Asia/Taipei","demo":0,"BAND":1,"lamps":[{"KID":19001314,"GID":2,"lGID":-1,"rGID":-1,"model":"A360X Tuna Sun","FWVer":"1.3","type":2,"temp":0,"timestamp":1601444856}],"num_prog_ids":{"TB":[{"PID":0,"order":0},{"PID":1,"order":1},{"PID":2,"order":2},{"PID":3,"order":3},{"PID":4,"order":4},{"PID":5,"order":5},{"PID":6,"order":6},{"PID":7,"order":7},{"PID":8,"order":8},{"PID":9,"order":9},{"PID":10,"order":10},{"PID":11,"order":11},{"PID":12,"order":12}],"TS":[{"PID":0,"order":0},{"PID":1,"order":1},{"PID":2,"order":2},{"PID":3,"order":3},{"PID":4,"order":4},{"PID":5,"order":5},{"PID":6,"order":6},{"PID":7,"order":7},{"PID":8,"order":8},{"PID":9,"order":9},{"PID":10,"order":10},{"PID":11,"order":11},{"PID":12,"order":12}],"RF":[{"PID":0,"order":0},{"PID":1,"order":1},{"PID":2,"order":2},{"PID":3,"order":3},{"PID":4,"order":4},{"PID":5,"order":5},{"PID":6,"order":6},{"PID":7,"order":7},{"PID":8,"order":8},{"PID":9,"order":9},{"PID":10,"order":10},{"PID":11,"order":11},{"PID":12,"order":12}],"Z2T":[{"PID":0,"order":0},{"PID":1,"order":1},{"PID":2,"order":2},{"PID":3,"order":3},{"PID":4,"order":4},{"PID":5,"order":5},{"PID":6,"order":6},{"PID":7,"order":7},{"PID":8,"order":8},{"PID":9,"order":9},{"PID":10,"order":10},{"PID":11,"order":11},{"PID":12,"order":12}]},"group_ids":[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27]}'
    system_info_dict = json.loads(system_info_json_str)

    # # type: List[int]
    # group_ids_dict = system_info_dict['group_ids']
    # print('type of group_ids_dict: {}, content: {}'.format(type(group_ids_dict), group_ids_dict))
    # group_ids_list: List[IeSimpleGroupId] = list()
    # for group_id in group_ids_dict:
    #     simple_group_id = IeSimpleGroupId(group_id)
    #     group_ids_list.append(simple_group_id)
    #     print(str(simple_group_id))
    #
    # print('*****************************************************************')
    #
    # lamps_dict = system_info_dict['lamps']
    # print('type of lamps_dict: {}, content: {}'.format(type(lamps_dict), lamps_dict))
    # lamps_list: List[IeDongleLamp] = list(map(IeDongleLamp.from_json, lamps_dict))
    # for the_lamp in lamps_list:
    #     print(str(the_lamp))
    #
    # print('*****************************************************************')
    #
    # num_prog_ids_dict = system_info_dict['num_prog_ids']
    # print('type of num_prog_ids_dict: {}, content: {}'.format(type(num_prog_ids_dict), num_prog_ids_dict))
    # num_prog_ids = IeSimpleProgramIdOrderWrapper.from_json(num_prog_ids_dict)
    # for pid_order in num_prog_ids.TB:
    #     print(str(pid_order))
    # print('*****************************************************************')
    # for pid_order in num_prog_ids.TS:
    #     print(str(pid_order))
    # print('*****************************************************************')
    # for pid_order in num_prog_ids.RF:
    #     print(str(pid_order))
    # print('*****************************************************************')
    # for pid_order in num_prog_ids.Z2T:
    #     print(str(pid_order))
    # print('*****************************************************************')
    system_info_response = IeSystemInfoResponse.from_json(system_info_dict)
    print(str(system_info_response))

    for group_id in system_info_response.group_ids:
        print(str(group_id))
    print('*****************************************************************')

    for the_lamp in system_info_response.lamps:
        print(str(the_lamp))
    print('*****************************************************************')

    for pid_order in system_info_response.num_prog_ids.TB:
        print(str(pid_order))
    print('*****************************************************************')
    for pid_order in system_info_response.num_prog_ids.TS:
        print(str(pid_order))
    print('*****************************************************************')
    for pid_order in system_info_response.num_prog_ids.RF:
        print(str(pid_order))
    print('*****************************************************************')
    for pid_order in system_info_response.num_prog_ids.Z2T:
        print(str(pid_order))
    print('*****************************************************************')


# group_info
def json_test6():
    print('=================================================================')
    group_info_json_str = '{"msg":"Success","code":0,"dType":102,"group":{"gid":26,"pid":11,"eid":-1,"type":3,"hasLamps":0,"name":"Default RF","pause":0,"startDate":0,"UI":[255,96,0,0,0],"accl":{"start":20,"period":14,"enabled":0,"startDate":0,"stopDate":0},"lunar":{"start":1200,"end":480,"high":120,"color":0,"phase":0,"enabled":0,"startDate":0,"stopDate":0}}}'
    response_wrapper = IeDongleResponseWrapper.from_json(group_info_json_str)
    print(str(response_wrapper.json_dict))
    group_dict = response_wrapper.get_dict_by_key(json_dongle_key_group)
    group = IeDongleGroup.from_json(group_dict)
    print(str(group))

    # simple_group = IeSimpleDongleGroup.from_json(group_dict)
    # print(str(simple_group))


# program_info
def json_test7():
    print('=================================================================')
    program_info_json_str = '{"msg":"Success","code":0,"program":{"PID":1,"order":1,"type":1,"name":"Preset Tuna Blue","points":[{"minutes":480,"UI":[3,0,0,0,0],"eid":-1,"duration":30,"freq":1},{"minutes":540,"UI":[51,26,0,0,0],"eid":-1,"duration":30,"freq":1},{"minutes":660,"UI":[102,102,0,0,0],"eid":-1,"duration":30,"freq":1},{"minutes":780,"UI":[153,179,0,0,0],"eid":-1,"duration":30,"freq":1},{"minutes":900,"UI":[153,179,0,0,0],"eid":-1,"duration":30,"freq":1},{"minutes":1020,"UI":[102,128,0,0,0],"eid":-1,"duration":30,"freq":1},{"minutes":1140,"UI":[51,51,0,0,0],"eid":-1,"duration":30,"freq":1},{"minutes":1200,"UI":[0,0,0,0,0],"eid":-1,"duration":30,"freq":1}]},"dType":106}'
    response_wrapper = IeDongleResponseWrapper.from_json(program_info_json_str)
    print(str(response_wrapper.json_dict))
    program_dict = response_wrapper.get_dict_by_key(json_dongle_key_program)
    program = IeDongleProgram.from_json(program_dict)
    print(str(program))
    for program_point in program.points:
        print(str(program_point))


if __name__ == '__main__':
    # json_test1()
    # json_test2()
    # json_test3()
    # json_test4()

    # json_test5()

    json_test6()
    json_test7()




