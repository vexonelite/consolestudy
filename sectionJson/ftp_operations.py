#{"dtype":205, "data":{"enabled":1,"host":"/tmp/Data"}}

# [Python FTP Client Tutorial](https://www.devdungeon.com/content/python-ftp-client-tutorial)
# [ftplib — FTP protocol client](https://docs.python.org/3/library/ftplib.html)
# [ftpretty](https://github.com/codebynumbers/ftpretty)

import traceback

from abc import ABC #, ABCMeta
from ftplib import FTP
from ftpretty import ftpretty

from concurrent.futures import ThreadPoolExecutor, as_completed
from .exceptions import IeCustomException, ftp_connection_failure, ftp_disconnect_failure,\
    ftp_login_failure, ftp_change_directory_failure, ftp_fail_to_get_file_list, \
    ftp_download_failure, ftp_upload_failure


ANONYMOUS_USER = 'anonymous'
ANONYMOUS_PASSWORD = 'anonymous@'


class IeFtpConfiguration:
    def __init__(self,
                 host_name: str,
                 port_number: int = 21,
                 connection_timeout: int = 10,  # in seconds
                 user_name: str = '',
                 password: str = '',
                 remote_path: str = '/',
                 passive: bool = False):
        self.host_name = host_name
        self.port_number = port_number
        self.connection_timeout = connection_timeout
        self.user_name = user_name
        self.password = password
        self.remote_path = remote_path
        self.passive = passive


class IeFtpOperationDelegate(ABC):

    def connect_to_remote(self):
        raise NotImplementedError("connect_to_remote() must be overridden.")

    def disconnect(self):
        raise NotImplementedError("disconnect() must be overridden.")

    def login(self):
        raise NotImplementedError("login() must be overridden.")

    def change_working_directory(self):
        raise NotImplementedError("change_working_directory() must be overridden.")

    def get_file_list(self) -> list:
        raise NotImplementedError("get_file_list() must be overridden.")

    # def download_single_text_file(self, local_filename: str, remote_filename: str):
    #     raise NotImplementedError("download_single_text_file() must be overridden.")

    def download_single_binary_file(self, local_filename: str, remote_filename: str):
        raise NotImplementedError("download_single_binary_file() must be overridden.")

    # def upload_single_text_file(self, local_filename: str, remote_filename: str):
    #     raise NotImplementedError("upload_single_text_file() must be overridden.")

    def upload_single_binary_file(self, local_filename: str, remote_filename: str):
        raise NotImplementedError("upload_single_binary_file() must be overridden.")


class IeFtpLibClient(IeFtpOperationDelegate):

    def __init__(self, configuration: IeFtpConfiguration):
        #self.ftp_client = FTP()
        self.configuration = configuration
        self.ftp_client = FTP(
            host=self.configuration.host_name,
            user=self.configuration.user_name,
            passwd=self.configuration.password,
            timeout=self.configuration.connection_timeout)

    def connect_to_remote(self):
        try:
            self.ftp_client.connect(
                host=self.configuration.host_name,
                port=self.configuration.port_number,
                timeout=self.configuration.connection_timeout)
            self.ftp_client.set_pasv(self.configuration.passive)
            print('connect to {}, with {}'.format(self.configuration.host_name, self.ftp_client.getwelcome()))
        except Exception as cause:
            print('error on ftp connect', traceback.print_tb(cause.__traceback__))
            raise IeCustomException(ftp_connection_failure, 'error on ftp connect', cause)

    def disconnect(self):
        try:
            self.ftp_client.quit()
            print('disconnect via ftp_client.quit')
        except Exception as cause:
            print('error on ftp quit', traceback.print_tb(cause.__traceback__))
            raise IeCustomException(ftp_disconnect_failure, 'error on ftp quit', cause)

    def login(self):
        try:
            # if len(self.configuration.user_name) == 0 and len(self.configuration.password) == 0:
            #     print('login anonymously ')
            #     self.ftp_client.login()
            # else:
            #     print('login by using customized user_name: {}, password: {}'.
            #           format(self.configuration.user_name, self.configuration.password))
            #     self.ftp_client.login(user=self.configuration.user_name,
            #                           passwd=self.configuration.password)
            self.ftp_client.login(user=self.configuration.user_name,
                                  passwd=self.configuration.password)
            print('login successfully with {}'.format(self.ftp_client.getwelcome()))
        except Exception as cause:
            print('error on ftp login', traceback.print_tb(cause.__traceback__))
            raise IeCustomException(ftp_login_failure, 'error on ftp login', cause)

    def change_working_directory(self):
        try:
            result = self.ftp_client.cwd(self.configuration.remote_path)
            print('result of change working directory: {}'.format(result))
        except Exception as cause:
            print('error on ftp change working directory', traceback.print_tb(cause.__traceback__))
            raise IeCustomException(ftp_change_directory_failure, 'error on ftp change working directory', cause)

    def get_file_list(self) -> list:
        try:
            file_list = []
            self.ftp_client.dir(file_list.append)  # Takes a callback for each file
            for file in file_list:
                # type of file is 'str', the content is like:
                # '-rw-rw-r--    1 0        3003       267407 Sep 02 15:20 find.txt.gz'
                print('type of file: {}, file: {}'.format(type(file), file))
            return file_list
        except Exception as cause:
            print('error on ftp get file list', traceback.print_tb(cause.__traceback__))
            raise IeCustomException(ftp_fail_to_get_file_list, 'error on ftp get file list', cause)

    def download_single_text_file(self, local_filename: str, remote_filename: str):
        try:
            with open(local_filename, 'w') as local_file:
                # Download `test.txt` from server and write to `local_file`
                # Pass absolute or relative path
                response = self.ftp_client.retrlines('RETR ' + remote_filename, local_file.write)

                # Check the response code
                # https://en.wikipedia.org/wiki/List_of_FTP_server_return_codes
                if response.startswith('226'):  # Transfer complete
                    print('Transfer complete')
                else:
                    print('Error transferring. Local file may be incomplete or corrupt.')
        except Exception as cause:
            print('error on ftp download single text file', traceback.print_tb(cause.__traceback__))
            raise IeCustomException(ftp_download_failure, 'error on ftp download single text file', cause)

    def download_single_binary_file(self, local_filename: str, remote_filename: str):
        try:
            with open(local_filename, 'wb') as local_file:
                print('type of local_file: {}, content: {}'.format(type(local_file), local_file))
                response = self.ftp_client.retrbinary('RETR ' + remote_filename, local_file.write)
                if response.startswith('226'):  # Transfer complete
                    print('Transfer complete')
                else:
                    print('Error transferring. Local file may be incomplete or corrupt.')

                print('is file closed? {}'.format(local_file.closed))
                if not local_file.closed:
                    local_file.close()
        except Exception as cause:
            print('error on ftp download single binary file', traceback.print_tb(cause.__traceback__))
            raise IeCustomException(ftp_download_failure, 'error on ftp download single binary file', cause)

    def upload_single_text_file(self, local_filename: str, remote_filename: str):
        try:
            with open(local_filename, 'rb') as local_file:
                response = self.ftp_client.storlines('STOR ' + remote_filename, local_file)
                print('upload response: {}'.format(response))
        except Exception as cause:
            print('error on ftp upload single text file', traceback.print_tb(cause.__traceback__))
            raise IeCustomException(ftp_upload_failure, 'error on ftp upload single text file', cause)

    def upload_single_binary_file(self, local_filename: str, remote_filename: str):
        try:
            with open(local_filename, 'rb') as local_file:
                response = self.ftp_client.storbinary('STOR ' + remote_filename, local_file)
                print('upload response: {}'.format(response))
        except Exception as cause:
            print('error on ftp upload single binary file', traceback.print_tb(cause.__traceback__))
            raise IeCustomException(ftp_upload_failure, 'error on ftp upload single binary file', cause)


class IeFtpPrettyClient(IeFtpOperationDelegate):

    def __init__(self, configuration: IeFtpConfiguration):
        self.ftp_client = None
        self.configuration = configuration

    def connect_to_remote(self):
        try:
            # Advanced
            # kwargs are passed to underlying FTP or FTP_TLS connection
            # secure=True argument switches to an FTP_TLS connection default is False
            # passive=False disable passive connection, True is the default
            # Note: port is not supported as an argument in underlying FTP or FTP_TLS but support for
            # handling port has been internally added in ftpretty by setting FTP.port or FTP_TLS.port
            # f = ftpretty(host, user, password, secure = True, passive = False, timeout = 10, port = 2121)
            ftp_client = ftpretty(
                host=self.configuration.host_name,
                user=self.configuration.user_name,
                password=self.configuration.password,
                timeout=self.configuration.connection_timeout)
            if ftp_client:
                self.ftp_client = ftp_client
                print('ftpretty connect to {}, with {}'.format(self.configuration.host_name, self.ftp_client.getwelcome()))
            else:
                print('ftp_client is null!!')
                raise IeCustomException(ftp_connection_failure, 'ftp_client is null', None)
        except Exception as cause:
            print('error on ftpretty connect', traceback.print_tb(cause.__traceback__))
            raise IeCustomException(ftp_connection_failure, 'error on ftp connect', cause)

    def disconnect(self):
        if self.ftp_client:
            try:
                self.ftp_client.close()
                print('disconnect via ftpretty.close')
            except Exception as cause:
                print('error on ftpretty.close', traceback.print_tb(cause.__traceback__))
                raise IeCustomException(ftp_disconnect_failure, 'error on ftpretty.close', cause)
        else:
            print('disconnect - ftpretty is None!!')

    def login(self):
        pass

    def change_working_directory(self):
        if self.ftp_client:
            try:
                result = self.ftp_client.cd(self.configuration.remote_path)
                print('result of ftpretty change working directory: {}'.format(result))
            except Exception as cause:
                print('error on ftpretty ftp change working directory', traceback.print_tb(cause.__traceback__))
                raise IeCustomException(ftp_change_directory_failure,
                                        'error on ftpretty ftp change working directory', cause)
        else:
            print('change_working_directory - ftpretty is None!!')

    def get_file_list(self) -> list:
        if self.ftp_client:
            try:
                file_list = []
                self.ftp_client.dir(file_list.append)
                for file in file_list:
                    # type of file is 'str', the content is like: 'find.txt.gz'
                    print('type of file: {}, file: {}'.format(type(file), file))
                return file_list

                # file_list = self.ftp_client.list(self.configuration.remote_path)  # Takes a callback for each file
                # if file_list:
                #     for file in file_list:
                #         # type of file is 'str', the content is like: 'find.txt.gz'
                #         print('type of file: {}, file: {}'.format(type(file), file))
                #     return file_list
                # else:
                #     print('ftpretty.list() returns None!!')
                #     return []
            except Exception as cause:
                print('error on ftpretty get file list', traceback.print_tb(cause.__traceback__))
                raise IeCustomException(ftp_fail_to_get_file_list, 'error on ftpretty get file list', cause)
        else:
            print('get_file_list - ftpretty is None!!')

    def download_single_binary_file(self, local_filename: str, remote_filename: str):
        if self.ftp_client:
            try:
                with open(local_filename, 'wb') as local_file:
                    # type of local_file is 'io.BufferedWriter'
                    print('type of local_file: {}, content: {}'.format(type(local_file), local_file))
                    self.ftp_client.get(remote_filename, local_file)

                    print('is file closed? {}'.format(local_file.closed))
                    if not local_file.closed:
                        local_file.close()
            except Exception as cause:
                print('error on ftpretty download single binary file', traceback.print_tb(cause.__traceback__))
                raise IeCustomException(ftp_download_failure, 'error on ftpretty download single binary file', cause)
        else:
            print('download_single_binary_file - ftpretty is None!!')

    def upload_single_binary_file(self, local_filename: str, remote_filename: str):
        if self.ftp_client:
            try:
                with open(local_filename, 'rb') as local_file:
                    response = self.ftp_client.storbinary('STOR ' + remote_filename, local_file)
                    print('upload response: {}'.format(response))
            except Exception as cause:
                print('error on ftp upload single binary file', traceback.print_tb(cause.__traceback__))
                raise IeCustomException(ftp_upload_failure, 'error on ftp upload single binary file', cause)
        else:
            print('upload_single_binary_file - ftpretty is None!!')


theFtpConfiguration1 = IeFtpConfiguration(host_name='192.168.86.115', port_number=21)
theFtpConfiguration3 = IeFtpConfiguration(host_name='192.168.86.129', port_number=21)
theFtpConfiguration2 = IeFtpConfiguration(host_name='ftp.gnu.org', user_name=ANONYMOUS_USER, password=ANONYMOUS_PASSWORD)


def test1():
    print('==========================================')
    the_ftp_client = IeFtpLibClient(theFtpConfiguration2)
    try:
        the_ftp_client.connect_to_remote()
        the_ftp_client.login()
        # the_ftp_client.change_working_directory()
        the_ftp_client.get_file_list()
        # the_ftp_client.download_single_text_file('README.txt', 'README')
        the_ftp_client.download_single_binary_file('README_221.txt', 'README')
        # the_ftp_client.upload_single_binary_file('README', 'README_dummy')
        the_ftp_client.disconnect()
    except Exception as error:
        print('error on ftp operations: {}'.format(error))


def test2():
    print('==========================================')
    the_ftp_client = IeFtpPrettyClient(theFtpConfiguration2)
    try:
        the_ftp_client.connect_to_remote()
        # the_ftp_client.login()
        # the_ftp_client.change_working_directory()
        the_ftp_client.get_file_list()
        the_ftp_client.download_single_binary_file('README_223.txt', 'README')
        the_ftp_client.disconnect()
    except Exception as error:
        print('error on ftp operations: {}'.format(error))


def test3():
    print('==========================================')
    the_ftp_client = IeFtpLibClient(theFtpConfiguration3)
    try:
        # the_ftp_client.connect_to_remote()
        # the_ftp_client.login()
        print(the_ftp_client.ftp_client.getwelcome())
        # the_ftp_client.change_working_directory()
        # the_ftp_client.get_file_list()
        the_ftp_client.upload_single_binary_file('README_221.txt', 'README_dummy')
        the_ftp_client.disconnect()
    except Exception as error:
        print('error on ftp operations: {}'.format(error))


def test4():
    print('==========================================')
    the_ftp_client = IeFtpPrettyClient(theFtpConfiguration3)
    try:
        the_ftp_client.connect_to_remote()
        # the_ftp_client.login()
        # the_ftp_client.change_working_directory()
        the_ftp_client.get_file_list()
        # the_ftp_client.download_single_binary_file('README_223.txt', 'README')
        the_ftp_client.disconnect()
    except Exception as error:
        print('error on ftp operations: {}'.format(error))


def upload_to_dongle():
    print('==========================================')

    ftp_client = None
    try:
        ftp_client = ftpretty('192.168.86.225', '', '')
        # Put FW File to Dongle
        myfile = open('README_221.txt', 'rb')
        filesize = ftp_client.put(myfile, 'README_221.txt')
        print(filesize)
        # list current Folder
        print(ftp_client.list('.'))
        # close
        return 'Success'
    except Exception as error:
        print('error on ftp operations', traceback.print_tb(error.__traceback__))
        return 'Failure'
    finally:
        if ftp_client:
            ftp_client.close()


def ftp_dir():
    print('==========================================')
    ftp_client = None
    try:
        ftp_client = ftpretty('192.168.86.225', '', '')
        # Put FW File to Dongle
        ftp_client.list('', extra=True)
        return True
    except Exception as error:
        print('error on ftp operations', traceback.print_tb(error.__traceback__))
        return 'Failure'
    finally:
        if ftp_client:
            ftp_client.close()


def test_ftp():
    with ThreadPoolExecutor(max_workers=1) as executor:
        # future_object = executor.submit(upload_to_dongle)
        future_object = executor.submit(ftp_dir)
        print('get future_object')
        print('get result from future_object: {}'.format(future_object.result()))


if __name__ == '__main__':
    test_ftp()
