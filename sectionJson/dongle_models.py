
import json
from .json_utils import IeObjectFromJsonMixin, IeObjectToJsonMixin
from typing import List, Optional


json_dongle_key_code = 'code'
json_dongle_key_message = 'msg'

json_dongle_key_group = 'group'
json_dongle_key_group_id = 'gid'
json_dongle_key_program_id_1 = 'pid'
json_dongle_key_program_id_2 = 'PID'
json_dongle_key_effect_id = 'eid'
json_dongle_key_type = 'type'
json_dongle_key_has_lamps = 'hasLamps'
json_dongle_key_name = 'name'
json_dongle_key_pause = 'pause'
json_dongle_key_start_date = 'startDate'
json_dongle_key_stop_date = 'stopDate'

json_dongle_key_program = 'program'
json_dongle_key_order = 'order'
json_dongle_key_points = 'points'

json_dongle_key_start = 'start'
json_dongle_key_end = 'end'
json_dongle_key_high = 'high'
json_dongle_key_color = 'color'
json_dongle_key_phase = 'phase'
json_dongle_key_period = 'period'
json_dongle_key_enabled = 'enabled'
json_dongle_key_ui = 'UI'

json_dongle_key_acclimation = 'accl'
json_dongle_key_lunar_cycle = 'lunar'


# abstract
class BaseDongleResponse:

    def __init__(self, code: int, msg: str, dType: int):
        self.code = code
        self.msg = msg
        self.dType = dType


# "lamps": [
#     {
#       "KID": 19001314,
#       "GID": 2,
#       "lGID": -1,
#       "rGID": -1,
#       "model": "A360X Tuna Sun",
#       "FWVer": "1.3",
#       "type": 2,
#       "temp": 0,
#       "timestamp": 1601338320
#     }
class IeDongleLamp(IeObjectFromJsonMixin):
    def __init__(self, KID: int, GID: int, lGID: int, rGID: int, model: str, FWVer: str, type: int, temp: int,
                 timestamp: int):
        self.KID = KID
        self.GID = GID
        self.lGID = lGID
        self.rGID = rGID
        self.model = model
        self.FWVer = FWVer
        self.type = type
        self.temp = temp
        self.timestamp = timestamp

    def __str__(self):
        return 'Lamp { KID: ' + str(self.KID) + ', GID: ' + str(self.GID) + ', lGID: ' + str(self.lGID) + \
               ', rGID: ' + str(self.rGID) + ', model: ' + self.model + ', FWVer: ' + self.FWVer + \
               ', type: ' + str(self.type) + ', temp: ' + str(self.temp) + ', timestamp: ' + str(self.timestamp) + ' }'


class IeSimpleProgramIdOrder(IeObjectFromJsonMixin, IeObjectToJsonMixin):
    def __init__(self, PID: int, order: int):
        self.PID = PID
        self.order = order

    def __str__(self):
        return 'ProgramIdOrder { PID: ' + str(self.PID) + ', order: ' + str(self.order) + '}'


class IeSimpleGroupId(IeObjectToJsonMixin):
    def __init__(self, gid: int):
        self.GID = gid

    # override
    def __str__(self):
        return 'IeSimpleGroupId {' + str(self.GID) + '}'


# key 'num_prog_ids'
class IeSimpleProgramIdOrderWrapper:
    def __init__(self,
                 TB: List[IeSimpleProgramIdOrder],
                 TS: List[IeSimpleProgramIdOrder],
                 RF: List[IeSimpleProgramIdOrder],
                 Z2T: List[IeSimpleProgramIdOrder]):
        self.TB = TB
        self.TS = TS
        self.RF = RF
        self.Z2T = Z2T

    @classmethod
    def from_json(cls, json_dict: dict):
        tuna_blue_dict = json_dict['TB']
        # print('type of tuna_blue_dict: {}, content: {}'.format(type(tuna_blue_dict), tuna_blue_dict))
        tuna_sun_dict = json_dict['TS']
        refugium_dict = json_dict['RF']
        zero_ten_dict = json_dict['Z2T']

        tuna_blue_list = list(map(IeSimpleProgramIdOrder.from_json, tuna_blue_dict))
        # print('type of tuna_blue_list: {}, content: {}'.format(type(tuna_blue_list), tuna_blue_list))
        tuna_sun_list = list(map(IeSimpleProgramIdOrder.from_json, tuna_sun_dict))
        refugium_list = list(map(IeSimpleProgramIdOrder.from_json, refugium_dict))
        zero_ten_list = list(map(IeSimpleProgramIdOrder.from_json, zero_ten_dict))
        return cls(tuna_blue_list, tuna_sun_list, refugium_list, zero_ten_list)


class IeSystemInfoResponse(BaseDongleResponse):
    def __init__(self, code: int, msg: str, dType: int, sn: str, ver: str, SSID: str, ENC: str, timezone: str,
                 demo: int, BAND: int, lamps: List[IeDongleLamp], num_prog_ids: IeSimpleProgramIdOrderWrapper,
                 group_ids: List[IeSimpleGroupId]):
        super().__init__(code, msg, dType)
        self.sn = sn
        self.ver = ver
        self.SSID = SSID
        self.ENC = ENC
        self.timezone = timezone
        self.demo = demo
        self.BAND = BAND
        self.lamps = lamps
        self.num_prog_ids = num_prog_ids
        self.group_ids = group_ids

    # override
    def __str__(self):
        return 'IeSystemInfoResponse { code: ' + str(self.code) + ', msg: ' + self.msg + ', dType: ' + str(self.dType) \
               + ', sn: ' + self.sn + ', ver: ' + self.ver + ', SSID: ' + self.SSID + ', ENC: ' + self.ENC + \
               ', timezone: ' + self.timezone + ', demo: ' + str(self.demo) + ', BAND: ' + str(self.BAND) + \
               ', lamps: ' + str(len(self.lamps)) + ', group_ids: ' + str(len(self.group_ids)) + '}'

    @classmethod
    def from_json(cls, json_dict: dict):
        code = json_dict['code']
        msg = json_dict['msg']
        dType = json_dict['dType']
        sn = json_dict['sn']
        ver = json_dict['ver']
        SSID = json_dict['SSID']
        ENC = json_dict['ENC']
        timezone = json_dict['timezone']
        demo = json_dict['demo']
        BAND = json_dict['BAND']

        # type: List[int]
        group_ids_dict = json_dict['group_ids']
        # print('type of group_ids_dict: {}, content: {}'.format(type(group_ids_dict), group_ids_dict))
        group_ids: List[IeSimpleGroupId] = list()
        for group_id in group_ids_dict:
            simple_group_id = IeSimpleGroupId(group_id)
            group_ids.append(simple_group_id)
            # print(str(simple_group_id))
        # print('*****************************************************************')

        lamps_dict = json_dict['lamps']
        # print('type of lamps_dict: {}, content: {}'.format(type(lamps_dict), lamps_dict))
        lamps: List[IeDongleLamp] = list(map(IeDongleLamp.from_json, lamps_dict))
        # for the_lamp in lamps:
        #     print(str(the_lamp))
        # print('*****************************************************************')

        num_prog_ids_dict = json_dict['num_prog_ids']
        # print('type of num_prog_ids_dict: {}, content: {}'.format(type(num_prog_ids_dict), num_prog_ids_dict))
        num_prog_ids = IeSimpleProgramIdOrderWrapper.from_json(num_prog_ids_dict)
        # for pid_order in num_prog_ids.TB:
        #     print(str(pid_order))
        # print('*****************************************************************')
        # for pid_order in num_prog_ids.TS:
        #     print(str(pid_order))
        # print('*****************************************************************')
        # for pid_order in num_prog_ids.RF:
        #     print(str(pid_order))
        # print('*****************************************************************')
        # for pid_order in num_prog_ids.Z2T:
        #     print(str(pid_order))
        # print('*****************************************************************')

        return cls(code, msg, dType, sn, ver, SSID, ENC, timezone, demo, BAND, lamps,num_prog_ids, group_ids)


class IeDongleProgramPoint(IeObjectFromJsonMixin):

    def __init__(self, minutes: int, UI: List[int], eid: int, duration: int, freq: int):
        self.minutes = minutes
        self.UI = UI
        self.eid = eid
        self.duration = duration
        self.freq = freq

    # override
    def __str__(self):
        return 'DongleProgramPoint { EID: ' + str(self.eid) + ', Minute: ' + str(self.minutes) + ', Duration: ' + \
               str(self.duration) + ', Frequency: ' + str(self.freq) + ', UI: ' + str(self.UI) + ' }'


class IeDongleProgram(IeObjectToJsonMixin):

    def __init__(self, PID: int, order: int, type: int, name: str, points: List[IeDongleProgramPoint]):
        self.PID = PID
        self.order = order
        self.type = type
        self.name = name
        self.points = points

    # override
    def __str__(self):
        return 'DongleProgram { PID: ' + str(self.PID) + ', Name: ' + self.name + ', Type: ' + str(
            self.type) + ', Order: ' + str(self.order) + ', Points.Size: ' + str(len(self.points)) + ' }'

    @classmethod
    def from_json(cls, json_dict: dict):
        PID = json_dict[json_dongle_key_program_id_2]
        order = json_dict[json_dongle_key_order]
        the_type = json_dict[json_dongle_key_type]
        name = json_dict[json_dongle_key_name]
        points_dict = json_dict[json_dongle_key_points]
        print('type of points_dict: {}, content: {}'.format(type(points_dict), points_dict))
        points: List[IeDongleProgramPoint] = list(map(IeDongleProgramPoint.from_json, points_dict))
        return cls(PID, order, the_type, name, points)


class IeDongleAcclimation(IeObjectFromJsonMixin):
    def __init__(self, start: int, period: int, enabled: int, startDate: int, stopDate: int):
        self.start = start
        self.period = period
        self.enabled = enabled
        self.startDate = startDate
        self.stopDate = stopDate

    # override
    def __str__(self):
        return 'DongleAcclimation { start: ' + str(self.start) + ', period: ' + str(self.period) + ', enabled: ' \
               + str(self.enabled) + ', startDate: ' + str(self.startDate) + ', stopDate: ' + str(self.stopDate) + ' }'


class IeDongleLunarCycle(IeObjectFromJsonMixin):

    def __init__(self, start: int, end: int, high: int, color: int, phase: int, enabled: int,
                 startDate: int, stopDate: int):
        self.start = start
        self.end = end
        self.high = high
        self.color = color
        self.phase = phase
        self.enabled = enabled
        self.startDate = startDate
        self.stopDate = stopDate

    # override
    def __str__(self):
        return 'DongleLunarCycle { start: ' + str(self.start) + ', end: ' + str(self.end) + ', high: ' + \
               str(self.high) + ', color: ' + str(self.color) + ', phase: ' + str(self.phase) + ', enabled: ' + \
               str(self.enabled) + ', startDate: ' + str(self.startDate) + ', stopDate: ' + str(self.stopDate) + ' }'


class IeDongleGroup(IeObjectToJsonMixin):

    def __init__(self, gid: int, pid: int, eid: int, type: int, hasLamps: int, name: str, pause: int,
                 startDate: int, UI: List[int], accl: IeDongleAcclimation, lunar: IeDongleLunarCycle):
        self.gid = gid
        self.pid = pid
        self.eid = eid
        self.type = type
        self.hasLamps = hasLamps
        self.name = name
        self.pause = pause
        self.startDate = startDate
        # self.stopDate = stopDate
        self.UI = UI
        self.accl = accl
        self.lunar = lunar

    # override
    def __str__(self):
        return 'DongleGroup { Name: ' + self.name + ', Gid: ' + str(self.gid) + ', Type: ' + str(self.type) + \
               ', hasLamps: ' + str(self.hasLamps) + ', Pid: ' + str(self.pid) + ', Eid: ' + str(self.eid) + \
               ', Pause: ' + str(self.pause) + ', StartDate: ' + str(self.startDate) + ', UI: ' + str(self.UI) + ' }'

    @classmethod
    def from_json(cls, json_dict: dict):
        gid = json_dict[json_dongle_key_group_id]
        pid = json_dict[json_dongle_key_program_id_1]
        eid = json_dict[json_dongle_key_effect_id]
        the_type = json_dict[json_dongle_key_type]
        has_lamps = json_dict[json_dongle_key_has_lamps]
        name = json_dict[json_dongle_key_name]
        pause = json_dict[json_dongle_key_pause]
        start_date = json_dict[json_dongle_key_start_date]

        ui = json_dict[json_dongle_key_ui]

        acclimation_dict = json_dict[json_dongle_key_acclimation]
        print('acclimation_dict: {}'.format(acclimation_dict))
        acclimation = IeDongleAcclimation.from_json(acclimation_dict)
        print(acclimation)

        lunar_cycle_dict = json_dict[json_dongle_key_lunar_cycle]
        print('lunar_cycle_dict: {}'.format(lunar_cycle_dict))
        lunar_cycle = IeDongleLunarCycle.from_json(lunar_cycle_dict)
        print(lunar_cycle)

        return cls(gid, pid, eid, the_type, has_lamps, name, pause, start_date, ui, acclimation, lunar_cycle)


class IeSimpleDongleGroup(IeObjectToJsonMixin, IeObjectFromJsonMixin):

    def __init__(self, gid: int, pid: int, eid: int, type: int, hasLamps: int, name: str, pause: int,
                 startDate: int, UI: List[int]):
        self.gid = gid
        self.pid = pid
        self.eid = eid
        self.type = type
        self.hasLamps = hasLamps
        self.name = name
        self.pause = pause
        self.startDate = startDate
        # self.stopDate = stopDate
        self.UI = UI

    # override
    def __str__(self):
        return 'SimpleDongleGroup { Name: ' + self.name + ', Gid: ' + str(self.gid) + ', Type: ' + str(self.type) + \
               ', hasLamps: ' + str(self.hasLamps) + ', Pid: ' + str(self.pid) + ', Eid: ' + str(self.eid) + \
               ', Pause: ' + str(self.pause) + ', StartDate: ' + str(self.startDate) + ', UI: ' + str(self.UI) + ' }'


class IeDongleResponseWrapper:

    def __init__(self, json_dict: dict):
        self.json_dict = json_dict

    def is_successful(self) -> bool:
        code = self.json_dict[json_dongle_key_code]
        if code and code == 0:
            return True
        else:
            return False

    def get_dict_by_key(self, json_key: str) -> Optional[dict]:
        if json_key in self.json_dict:
            return self.json_dict[json_key]
        else:
            return None

    @classmethod
    def from_json(cls, json_str: str):
        try:
            json_dict = json.loads(json_str)
        except Exception as cause:
            json_dict = {}
        return cls(json_dict)

