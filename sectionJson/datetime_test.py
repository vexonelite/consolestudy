from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta

now: datetime = datetime.now() # current date and time
# print('type of now: {}'.format(type(now))) # type of now: <class 'datetime.datetime'>

print('current time: {}'.format(now))  # current time: 2022-03-25 08:57:25.752288
print('current timestamp: {}'.format(now.timestamp()))  # current timestamp: 1648169845.752288
timestamp: int = int(now.timestamp())
print('current timestamp [int]: {}'.format(timestamp))  # current timestamp: 1648169845

year: str = now.strftime("%Y")
print('year: {}'.format(year)) # year: 2022

month: str = now.strftime("%m")
print('month: {}'.format(month)) # month: 03

day: str = now.strftime("%d")
print('day: {}'.format(day)) # day: 25

time: str = now.strftime("%H:%M:%S")
print('time: {}'.format(time)) # time: 08:57:25

date_time: str = now.strftime("%m/%d/%Y, %H:%M:%S")
print('date_time: {}'.format(date_time)) # date_time: 03/25/2022, 08:57:25

###

date_time2: datetime = datetime.fromtimestamp(timestamp)
print('type of date_time2: {}'.format(type(date_time2))) # type of date_time2: <class 'datetime.datetime'>
print('Date time object: {}'.format(date_time2)) # Date time object: 2022-03-25 09:05:28

d: str = date_time2.strftime("%m/%d/%Y, %H:%M:%S")
print('Output 2: {}'.format(d)) # Output 2: 03/25/2022, 09:06:25

d = date_time2.strftime("%d %b, %Y")
print('Output 3: {}'.format(d)) # Output 3: 25 Mar, 2022

d = date_time2.strftime("%d %B, %Y")
print('Output 4: {}'.format(d)) # Output 4: 25 March, 2022

d = date_time2.strftime("%I%p")
print('Output 5: {}'.format(d)) # Output 5: 09AM

###

# Calculating future dates
# for two years
future_date_after_2years: datetime = now + timedelta(days = 730)
future_date_after_2days: datetime  = now + timedelta(days = 2)
past_date_before_2years: datetime  = now - timedelta(days = 730)
past_date_before_2hours: datetime  = now - timedelta(hours = 2)
print('future_date_after_2years: {}'.format(future_date_after_2years)) #
print('future_date_after_2days: {}'.format(future_date_after_2days)) #
print('past_date_before_2years: {}'.format(past_date_before_2years)) #
print('past_date_before_2hours: {}'.format(past_date_before_2hours)) #

future_date_after_3months: datetime  = now + timedelta(days = 90)
past_date_before_3months: datetime  = now - timedelta(days = 90)
print('future_date_after_3months: {}'.format(future_date_after_3months)) #
print('past_date_before_3months: {}'.format(past_date_before_3months)) #

future_date_after_3months: datetime = now + relativedelta(months=3)
past_date_before_3months: datetime = now - relativedelta(months=3)
print('future_date_after_3months [r]: {}'.format(future_date_after_3months)) #
print('past_date_before_3months [r]: {}'.format(past_date_before_3months)) #

