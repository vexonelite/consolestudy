from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest
import os
from random import randint
from appium import webdriver
from time import sleep


class SimpleIOSTests(unittest.TestCase):

    def setUp(self):
        # set up appium
        app = os.path.abspath('../../apps/TestApp/build/release-iphonesimulator/TestApp.app')
        self.driver = webdriver.Remote(
            command_executor='http://127.0.0.1:4723/wd/hub',
            desired_capabilities={
                'app': app,
                'platformName': 'iOS',
                'platformVersion': '10.1.1',#Modify to the current version number
                'deviceName': 'iPhone 6s',#Modified to the current model
                'uuid': '42b9d3af431ac1d5af95018f84cdf525e97089f7'  #uuid
            })

    def _populate(self):
        # populate text fields with two random numbers
        self.driver.find_element_by_id()
        els = [self.driver.find_element_by_accessibility_id('TextField1'),
               self.driver.find_element_by_accessibility_id('TextField2')]

        self._sum = 0
        for i in range(2):
            rnd = randint(0, 10)
            els[i].send_keys(rnd)
            self._sum += rnd
