# https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
# python3.8 -m sectionJson.object_to_json_test

import json
import traceback
from .dongle_commands import (
    IeDongleCommand, IeEmptyDongleCommandData, IeSiblingDongleCommandData, IeProgramInfoDongleCommandData
)
from .dongle_models import IeSimpleGroupId
from .json_utils import object_to_json, convert_into_dict


def json_test1():
    print('=================================================================')
    system_info = IeDongleCommand(114, IeEmptyDongleCommandData())
    siblings = IeDongleCommand(116, IeSiblingDongleCommandData('DongleB999'))
    group_info = IeDongleCommand(102, IeSimpleGroupId(26))
    program_info = IeDongleCommand(106, IeProgramInfoDongleCommandData(1, 1))

    print(system_info.to_json())
    print(siblings.to_json())
    print(group_info.to_json())
    print(program_info.to_json())


# [Serialization in Python with JSON](http://thepythoncorner.com/dev/serialization-python-json/)
def json_test2():
    print('=================================================================')
    my_list = ["this", "is", "a", "simple", "list", 35]
    my_json_object = json.dumps(my_list)
    print(my_json_object)

    my_second_list = json.loads(my_json_object)
    print(my_second_list)


def json_test3():
    print('=================================================================')
    json_src = {
        "name": "Foo Bar",
        "age": 78,
        "friends": ["Jane", "John"],
        "balance": 345.80,
        "other_names": ("Doe", "Joe"),
        "active": True,
        "spouse": None
    }
    json_str: str = json.dumps(json_src, sort_keys=True, indent=4)
    print('type of json_str: {}, content: {}'.format(type(json_str), json_str))
    json_object = json.loads(json_str)
    print('type of json_object: {}, content: {}'.format(type(json_object), json_object))


# [JSON — The Python Way](https://medium.com/python-pandemonium/json-the-python-way-91aac95d4041)
class User:
    """
    Custom User Class
    """
    def __init__(self, name, age, active, balance, other_names, friends, spouse):
        self.name = name
        self.age = age
        self.active = active
        self.balance = balance
        self.other_names = other_names
        self.friends = friends
        self.spouse = spouse

    def __str__(self):
        return 'User { name: ' + self.name + ', age: ' + str(self.age) + ', balance: ' + str(self.balance) + \
               ', spouse: ' + str(self.spouse) + ', friends: ' + str(self.friends) + ', other_names: ' + \
               str(self.other_names) + ' }'


def json_test4():
    print('=================================================================')
    new_user = User(
        name="Foo Bar",
        age=78,
        friends=["Jane", "John"],
        balance=345.80,
        other_names=("Doe", "Joe"),
        active=True,
        spouse=None)
    try:
        json_str = json.dumps(new_user)
        # TypeError: Object of type User is not JSON serializable
        print('json_str: {}'.format(json_str))
    except Exception as error:
        print('error on json.dump()', traceback.print_tb(error.__traceback__))


def json_test5():
    print('=================================================================')
    new_user = User(
        name="Foo Bar",
        age=78,
        friends=["Jane", "John"],
        balance=345.80,
        other_names=("Doe", "Joe"),
        active=True,
        spouse=None)
    json_str = json.dumps(new_user, default=convert_into_dict, sort_keys=True, indent=4)
    print('json_str: {}'.format(json_str))


if __name__ == '__main__':
    json_test1()
    json_test2()
    json_test3()
    json_test4()
    json_test5()




