from .chain_operations import IeChain, IeConversionDelegate


class TestConverter1(IeConversionDelegate):
    def do_convert(self, input_data: str) -> int:
        return len(input_data)


class TestConverter2(IeConversionDelegate):
    def do_convert(self, input_data: int) -> str:
        return 'result: ' + str(input_data)


result = IeChain('Hello World')\
    .map(TestConverter1())\
    .map(TestConverter2())\
    .on_complete()
print(result)
