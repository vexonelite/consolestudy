
import asyncio
import traceback

from abc import ABC  # ,ABCMeta
from typing import Generic, Optional, TypeVar

from .exceptions import (
    IeCustomException, unknown_error, internal_generation_error, internal_conversion_error,
    internal_filtering_error
)


T = TypeVar("T")
R = TypeVar("R")


class IeConversionDelegate(ABC):
    # abstract
    def do_convert(self, input_data: T) -> T:
        """
        return output
        """
        raise NotImplementedError("do_convert() must be overridden.")


class IeAbsConverter(Generic[T, R], IeConversionDelegate, ABC):
    """
    added in 2020/08/05
    """
    def do_convert(self, item: T) -> R:
        try:
            return self.execute_conversion(item)
        except Exception as cause:
            if isinstance(cause, IeCustomException):
                raise cause
            else:
                raise IeCustomException(internal_conversion_error, 'Error on involve execute_conversion()!', cause)

    def execute_conversion(self, item: T) -> R:
        """
        return output
        """
        raise NotImplementedError("execute_conversion() must be overridden.")


class IeCallableDelegate(ABC):
    """
    added in 2020/08/05
    """
    # abstract
    def call(self) -> T:
        """
        return output
        """
        raise NotImplementedError("call() must be overridden.")


class IeAbsCallable(Generic[T], IeCallableDelegate, ABC):
    """
    added in 2020/08/05
    """
    def call(self) -> T:
        try:
            return self.execute_generation()
        except Exception as cause:
            if isinstance(cause, IeCustomException):
                raise cause
            else:
                raise IeCustomException(internal_generation_error, 'Error on involve execute_generation()!', cause)

    def execute_generation(self) -> T:
        """
        return output
        """
        raise NotImplementedError("execute_generation() must be overridden.")


class IeRunnableDelegate(ABC):
    """
    added in 2020/11/17
    """
    # abstract
    def run(self):
        """
        """
        raise NotImplementedError("run() must be overridden.")


class IeFilterDelegate(ABC):
    """
    added in 2020/08/05
    """
    # abstract
    def predicate(self, item: T) -> bool:
        """
        return output
        """
        raise NotImplementedError("predicate() must be overridden.")


class IeFilterDelegate2(ABC):
    """
    added in 2020/09/23
    """
    # abstract
    def predicate(self, input1: T, input2: R) -> bool:
        """
        return output
        """
        raise NotImplementedError("predicate() must be overridden.")


class IeAbsFilter(Generic[T], IeFilterDelegate, ABC):
    """
    added in 2020/08/05
    """
    def predicate(self, item: T) -> bool:
        try:
            return self.execute_judgement(item)
        except Exception as cause:
            if isinstance(cause, IeCustomException):
                raise cause
            else:
                raise IeCustomException(internal_filtering_error, 'Error on involve execute_judgement()!', cause)

    def execute_judgement(self, item: T) -> bool:
        """
        return output
        """
        raise NotImplementedError("execute_judgement() must be overridden.")


class IeDataHolder(Generic[T]):
    def __init__(self, kept_data: T):
        self.kept_data = kept_data


class IeChain(Generic[T]):
    def __init__(self, data: T):
        self.ie_data_holder = IeDataHolder(data)

    def map(self, conversion: IeConversionDelegate):
        """
        return IeChain
        """
        input_data = self.ie_data_holder.kept_data
        # print('type of input_data: {}'.format(type(input_data)))
        output_data = conversion.do_convert(input_data)
        # print('type of output_data: {}'.format(type(output_data)))
        return IeChain(output_data)

    def on_complete(self) -> T:
        return self.ie_data_holder.kept_data


class IeApiResponse(Generic[T]):
    def __init__(self, result: T = None, error: IeCustomException = None):
        self.result = result
        self.error = error

    # override
    def __str__(self):
        result_str: str = ''
        if self.result:
            result_str += str(self.result)
        error_str: str = ''
        if self.error:
            error_str += self.error.message
        return 'IeApiResponse { result: ' + result_str + ', error: ' + error_str + ' }'


class IeApiResult(Generic[T]):
    def on_result_available(self, success: T, error: IeCustomException):
        raise NotImplementedError("on_result_available() must be overridden.")


class IeAsyncioTask(Generic[T], ABC):

    async def execute_asyncio_task(self) -> T:
        raise NotImplementedError("execute_task() must be overridden.")

    async def run_and_catch(self) -> tuple:
        try:
            task_result = await self.execute_asyncio_task()
            print('IeAsyncioTask#execute_asyncio_task - done!!')
            return task_result, None
        except Exception as caught_error:
            print('error on IeAsyncioTask#execute_asyncio_task')
            return None, caught_error

    def execute_task(self, api_result: IeApiResult = None):
        event_loop = asyncio.get_event_loop()
        try:
            result_tuple = event_loop.run_until_complete(self.run_and_catch())
            print('type of result_tuple: {}'.format(type(result_tuple)))
            print('result_tuple: {}'.format(result_tuple))
            if api_result:
                api_result.on_result_available(success=result_tuple[0], error=result_tuple[1])
            else:
                print('IeAsyncioTask#execute_task - api_result is null!')
                if result_tuple[0]:
                    print('IeAsyncioTask#execute_task - task_result: {}'.format(result_tuple[0]))
                elif result_tuple[1]:
                    print('error on IeAsyncioTask#execute_task', traceback.print_tb(result_tuple[1].__traceback__))
        finally:
            event_loop.close()
            print('closing event loop')
