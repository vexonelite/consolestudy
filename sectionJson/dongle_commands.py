
from .chain_operations import T
from .json_utils import IeObjectToJsonMixin


class IeDongleCommand(IeObjectToJsonMixin):
    def __init__(self, dtype: int, data: T):
        self.dtype = dtype
        self.data = data


class IeEmptyDongleCommandData:
    pass
    # def __init__(self):


class IeSiblingDongleCommandData:
    def __init__(self, siblings: str):
        self.siblings = siblings


class IeProgramInfoDongleCommandData:
    def __init__(self, PID: int, type: int):
        self.PID = PID
        self.type = type
